/**
 * @author Thomas Altenbach
 */
module photomanager {
    requires java.sql;
    requires java.prefs;
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.swing;
    requires org.apache.commons.imaging;
    requires org.apache.commons.io;
    requires commons.math3;
    requires com.google.common;
    requires gson;
    requires org.jetbrains.annotations;
    requires org.controlsfx.controls;

    opens com.arb.photomanager.controller to javafx.fxml;
    opens com.arb.photomanager.controller.editparams to javafx.fxml;
    opens com.arb.photomanager.model.db to gson;
    opens com.arb.photomanager.model.transform to gson;

    exports com.arb.photomanager;
}