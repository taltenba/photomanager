package com.arb.photomanager;

import com.arb.photomanager.controller.MainController;
import com.arb.photomanager.model.setting.Settings;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.util.Locale;

/**
 *
 * @author Aymeric Robitaille
 */
public class PhotoManagerApp extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Locale.setDefault(Locale.ENGLISH);

        Settings settings = Settings.getInstance();

        FXMLLoader loader = new FXMLLoader(PhotoManagerApp.class.getResource(Settings.MAIN_WINDOW_FILE));
        Parent root = loader.load();

        root.getStylesheets().clear();
        root.getStylesheets().add(PhotoManagerApp.class.getResource("/css/base_style.css").toString());
        root.getStylesheets().add(
                PhotoManagerApp.class.getResource(
                        "/css/Themes/" + settings.getUITheme() + ".css"
                ).toString()
        );

        primaryStage.setTitle(Settings.MAIN_WINDOW_NAME);
        primaryStage.setScene(new Scene(root));

        MainController controller = loader.getController();
        controller.init();

        primaryStage.getScene().getWindow().addEventFilter(WindowEvent.WINDOW_CLOSE_REQUEST,
                (WindowEvent event) -> Platform.exit());

        primaryStage.getIcons().add(
                new Image(PhotoManagerApp.class.getResource(Settings.ICO_FILE).toString())
        );
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
