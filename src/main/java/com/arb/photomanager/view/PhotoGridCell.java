package com.arb.photomanager.view;

import com.arb.photomanager.PhotoManagerApp;
import com.arb.photomanager.model.db.Photo;
import com.arb.photomanager.model.setting.Settings;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import org.controlsfx.control.GridCell;

import java.lang.ref.WeakReference;
import java.util.Objects;
import java.util.function.BiConsumer;

/**
 * Photo grid cell class
 * @author Aymeric Robitaille
 */
public class PhotoGridCell extends GridCell<Photo> {

    /*  Private attributes  */

    static final private int INTERNAL_BORDER = 30;
    static final private int INTERNAL_ZOOMED_BORDER = INTERNAL_BORDER / 4;

    private final ImageView mImageView;

    private boolean mIsZoomed;
    private boolean mIsSelected;
    private BiConsumer<Image, Throwable> mCurrentThumbnailCallback;
    private WeakReference<Photo> mLastPhoto;
    private WeakReference<PhotoGridView> mGridView;

    /**
     * Creates a default PhotoGridCell instance, which will preserve image properties
     */
    PhotoGridCell(PhotoGridView gridView) {
        Objects.requireNonNull(gridView);

        mGridView = new WeakReference<>(gridView);

        mIsZoomed = false;
        mIsSelected = false;

        getStyleClass().add("photo-cell");
        setAlignment(Pos.CENTER);

        mLastPhoto = new WeakReference<>(null);

        mImageView = new ImageView();
        mImageView.setPreserveRatio(true);
        mImageView.setSmooth(true);
        mImageView.setVisible(false);

        setGraphic(mImageView);

        addEventFilter(MouseEvent.MOUSE_ENTERED, (me) -> {
            mIsZoomed = true;
            adaptView();
        });
        addEventFilter(MouseEvent.MOUSE_EXITED, (me) -> {
            mIsZoomed = false;
            adaptView();
        });
//        addEventFilter(MouseEvent.MOUSE_CLICKED, (me) -> select());
    }

    private void adaptView() {
        if (mIsZoomed || mIsSelected) {
            mImageView.setFitHeight(getHeight() - INTERNAL_ZOOMED_BORDER);
            mImageView.setFitWidth(getWidth() - INTERNAL_ZOOMED_BORDER);
        } else {
            mImageView.setFitHeight(getHeight() - INTERNAL_BORDER);
            mImageView.setFitWidth(getWidth() - INTERNAL_BORDER);
        }

        if (mIsSelected && !getStyleClass().contains("photo-selected"))
            getStyleClass().add("photo-selected");
        else if (!mIsSelected)
            getStyleClass().removeAll("photo-selected");
    }

    /**
     * Updates the PhotoGridCell content
     */
    @Override
    protected void updateItem(Photo item, boolean empty) {
        super.updateItem(item, empty);

        mIsSelected = mGridView.get() != null && mGridView.get().isSelected(getIndex());

        if (empty) {
            mImageView.setVisible(false);
            return;
        }

        if (mLastPhoto.get() != item) {
            mImageView.setImage(
                    new Image(PhotoManagerApp.class.getResource(Settings.PLACEHOLDER_FILE).toString())
            );

            mCurrentThumbnailCallback = new BiConsumer<>() {
                @Override
                public void accept(Image image, Throwable throwable) {
                    if (this != mCurrentThumbnailCallback)
                        return;

                    if (throwable != null) {
                        mImageView.setImage(
                                new Image(PhotoManagerApp.class.getResource("/img/file_error.png").toString())
                        );
//                        throwable.printStackTrace();
                    } else {
                        mImageView.setImage(image);
                    }
                }
            };

            //mImageView.setImage(item);
            item.getThumbnail(mCurrentThumbnailCallback);

            mLastPhoto = new WeakReference<>(item);
        }

        if (getWidth() != 0) {
            adaptView();
            mImageView.setVisible(true);
        } else {
            mImageView.setVisible(false);
        }
    }

    /**
     * Selects the cell
     */
    void select() {
        mIsSelected = true;
        adaptView();
    }

    /**
     * Selects the cell
     */
    void unSelect() {
        mIsSelected = false;
        adaptView();
    }

    /**
     * True if the cell is selected
     * @return true if the cell is selected
     */
    public boolean isCellSelected() {
        return mIsSelected;
    }

    /**
     * Gives the contained photo image
     * @return the contained photo image
     */
    public Photo getPhoto() {
        return getItem();
    }
}
