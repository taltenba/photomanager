package com.arb.photomanager.view;

import javafx.scene.control.*;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Objects;

/**
 * @author Aymeric Robitaille
 */
public class AlbumTreeSelectorDialog extends Dialog<String> {
    private TreeView<String> mTreeView;

    public AlbumTreeSelectorDialog(TreeItem<String> rootTreeItem, Stage ownerStage) {
        super();

        Objects.requireNonNull(rootTreeItem);
        Objects.requireNonNull(ownerStage);

        mTreeView = new TreeView<>();
        mTreeView.setRoot(rootTreeItem);
        mTreeView.getSelectionModel().selectFirst();

        setTitle("Album selection");
        setHeaderText("Please choose an album");
        getDialogPane().setContent(mTreeView);
        getDialogPane().getButtonTypes().add(ButtonType.CLOSE);
        getDialogPane().getButtonTypes().add(ButtonType.OK);
        setResultConverter(buttonType -> {
            if (buttonType.getButtonData() == ButtonBar.ButtonData.CANCEL_CLOSE) {
                return null;
            } else {
                StringBuilder buff = new StringBuilder();
                ArrayList<TreeItem<String>> items = new ArrayList<>();
                TreeItem<String> item = mTreeView.getSelectionModel().getSelectedItem();

                while (item != mTreeView.getRoot()) {
                    items.add(item);
                    item = item.getParent();
                }

                for (int i = items.size() - 1; i >= 0; --i) {
                    buff
                            .append(items.get(i).getValue())
                            .append("\\");
                }

                return buff.toString();
//                return mTreeView.getSelectionModel().getSelectedItem();
            }
        });

        initOwner(ownerStage);
        getDialogPane().getStylesheets().addAll(ownerStage.getScene().getRoot().getStylesheets());
    }
}
