package com.arb.photomanager.view;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Stage;

import java.util.Optional;

/**
 * Dialog class for slide show settings
 * @author Aymeric Robitaille
 */
public class SlideShowSettingsDialog {
    private TextInputDialog mTid;
    static final private int LOWER_BOUND = 0;
    static final private int UPPER_BOUND = 60;

    private boolean isInvalid(String value) {
        int i;
        boolean r = false;
        try{
            i = Integer.parseInt(value);
            if(i <= LOWER_BOUND)
                r = true;
            if(i > UPPER_BOUND)
                r = true;
        }catch(NumberFormatException e){
            r = true;
        }

        return r;
    }

    public SlideShowSettingsDialog(int defaultDelay, Stage ownerStage) {
        mTid = new CustomTextDialog(String.valueOf(defaultDelay), ownerStage);
        mTid.setTitle("Slide show settings");
        mTid.setHeaderText("Change slide every (s)");
        mTid.setContentText("Please enter a number below "+ UPPER_BOUND +" and upper than "+ LOWER_BOUND + ":");
    }

    public Optional<Integer> showAndWait() {
        Button okButton = (Button) mTid.getDialogPane().lookupButton(ButtonType.OK);
        TextField inputField = mTid.getEditor();
        BooleanBinding isInvalid = Bindings.createBooleanBinding(() -> isInvalid(inputField.getText()), inputField.textProperty());
        okButton.disableProperty().bind(isInvalid);

        Optional<String> resultString = mTid.showAndWait();

        return resultString.map(Integer::parseInt);
    }
}
