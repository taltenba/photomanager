package com.arb.photomanager.view;

import com.arb.photomanager.PhotoManagerApp;
import com.arb.photomanager.controller.NewViewerController;
import com.arb.photomanager.model.setting.Settings;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

/**
 * @author Aymeric Robitaille
 */
public class OpenNewAlbumDialog extends Dialog<String> {

    private NewViewerController mController;

    public OpenNewAlbumDialog(Stage ownerStage) {
        super();

        Objects.requireNonNull(ownerStage);
        initOwner(ownerStage);
        getDialogPane().getStylesheets().addAll(ownerStage.getScene().getRoot().getStylesheets());
        setTitle(Settings.NEW_VIEWER_WINDOW_NAME);
        try {
            FXMLLoader loader = new FXMLLoader(PhotoManagerApp.class.getResource(Settings.NEW_VIEWER_WINDOW_FILE));
            Parent root = loader.load();
            getDialogPane().setContent(root);
            mController = loader.getController();
        } catch (IOException e) {
            e.printStackTrace();
        }

        getDialogPane().getButtonTypes().add(ButtonType.CLOSE);
        getDialogPane().getButtonTypes().add(ButtonType.OK);

        Button okButton = (Button)getDialogPane().lookupButton(ButtonType.OK);
        BooleanBinding isInvalid = Bindings.createBooleanBinding(
                () -> !mController.checkPathInput(),
                mController.getTextFieldProperty()
        );
        okButton.disableProperty().bind(isInvalid);

        setResultConverter(buttonType -> {
            if (buttonType.getButtonData() == ButtonBar.ButtonData.CANCEL_CLOSE)
                return null;
            else
                return mController.getSelectedPath();
        });
    }
}
