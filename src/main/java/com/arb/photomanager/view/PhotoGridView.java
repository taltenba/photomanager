package com.arb.photomanager.view;

import com.arb.photomanager.model.db.Photo;
import com.arb.photomanager.model.setting.Settings;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import org.controlsfx.control.GridView;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

/**
 * @author Aymeric Robitaille
 */
public class PhotoGridView extends GridView<Photo> {

    /*  Private attributes  */

    static final private int VERTICAL_SPACING = 2;
    static final private int HORIZONTAL_SPACING = 2;

    //Contextual menu
    static final private String OPEN_TXT = "Open";
    static final private String SELECT_ALL_TXT = "Select All";
    static final private String REMOVE_TXT = "Remove";
    static final private String MOVE_TXT = "Move";

    final private Set<PhotoGridCell> mSelectedCells;
    final private Set<Integer> mSelectedIndexes;
    private boolean mIsMultipleSelection;
    private Consumer<Collection<Integer>> mOnSelectionChangedCallbackFct;
    private Consumer<Collection<Integer>> mRemoveFct;
    private Consumer<Collection<Integer>> mOpenFct;
    private Consumer<Collection<Integer>> mMoveFct;

    /**
     * Creates a default PhotoGridView control with the provided items collection.
     *
     * @param items The items to display inside the PhotoGridView.
     */
    public PhotoGridView(ObservableList<Photo> items) {
        super(items);
        setHorizontalCellSpacing(HORIZONTAL_SPACING);
        setVerticalCellSpacing(VERTICAL_SPACING);
        mSelectedCells = new HashSet<>();
        mSelectedIndexes = new HashSet<>();
        mIsMultipleSelection = false;
        mOnSelectionChangedCallbackFct = null;

        init();
        initContextMenu();

        addListeners();
    }

    /**
     * Initiates default values and factories
     */
    private void init() {
        Settings settings = Settings.getInstance();

        setCellHeight(settings.getThumbnailHeight());
        setCellWidth(settings.getThumbnailWidth());

        setCellFactory(arg0 ->
                    new PhotoGridCell(this)
        );
    }

    /**
     * Creates and init the contextual menu
     */
    private void initContextMenu() {
        ContextMenu contextMenu = new ContextMenu();

        MenuItem open = new MenuItem(OPEN_TXT);
        MenuItem selectAll = new MenuItem(SELECT_ALL_TXT);
        MenuItem remove = new MenuItem(REMOVE_TXT);
        MenuItem move = new MenuItem(MOVE_TXT);

        contextMenu.getItems().addAll(open, selectAll, remove, move);

        open.setOnAction(event -> open());
        selectAll.setOnAction(event -> selectAll());
        remove.setOnAction(event -> remove());
        move.setOnAction(event -> move());

        setContextMenu(contextMenu);
    }

    /**
     * Add the listeners
     */
    private void addListeners() {
        addEventFilter(KeyEvent.KEY_PRESSED, (key) -> {
            switch (key.getCode()) {
                case CONTROL:
                    mIsMultipleSelection = true;
                    break;
                case A:
                    selectAll();
                    break;
            }
        });

        addEventFilter(KeyEvent.KEY_RELEASED, (key) -> {
            if(key.getCode() == KeyCode.CONTROL)
                mIsMultipleSelection = false;
        });

        addEventFilter(MouseEvent.MOUSE_CLICKED, this::handleClick);

    }

    /**
     * On click handler
     * @param me MouseEvent
     */
    private void handleClick(MouseEvent me) {

        if(me.getClickCount() == 2){
            open();
            return;
        }

        if (me.getButton() == MouseButton.SECONDARY)
            return;

        if (me.getTarget().getClass() == PhotoGridCell.class) {
            PhotoGridCell cell = (PhotoGridCell)me.getTarget();
            ObservableList<Photo> photos = itemsProperty().get();
            int index = photos.indexOf(cell.getPhoto());

            //If the cell is already selected we de-selected it
            if(mSelectedCells.contains(cell)) {
                cell.unSelect();
                mSelectedCells.remove(cell);
                mSelectedIndexes.remove(index);

                if(!mIsMultipleSelection)
                    unSelectAll();
            } else {

                //Selects the new cell
                cell.select();

                if (!mIsMultipleSelection)
                    unSelectAll();

                mSelectedCells.add(cell);
                mSelectedIndexes.add(index);
            }

        } else {
            unSelectAll();
        }
        if (mOnSelectionChangedCallbackFct != null)
            mOnSelectionChangedCallbackFct.accept(getSelectedIndexes());
    }

    /**
     * Selects all photo grid cell
     */
    private void selectAll() {
        Set<Node> gridCells = lookupAll(".photo-cell");
        ObservableList<Photo> photos = itemsProperty().get();
        int index;

        for (Node gc : gridCells) {
            PhotoGridCell pgc = (PhotoGridCell) gc;

            index = photos.indexOf(pgc.getPhoto());

            if (index == -1)
                continue;

            pgc.select();
            mSelectedCells.add(pgc);
            mSelectedIndexes.add(index);
        }

        if (mOnSelectionChangedCallbackFct != null)
            mOnSelectionChangedCallbackFct.accept(getSelectedIndexes());
    }

    boolean isSelected(int index) {
        try {
            return mSelectedIndexes.contains(index);
        } catch (NullPointerException e) {
            return false;
        }
    }

    /**
     * Un-selects all photo grid cells
     */
    public void unSelectAll() {
        //Un-select the others
        for(PhotoGridCell c : mSelectedCells)
            c.unSelect();

        //Update the selected cells set
        mSelectedCells.clear();

        //Update the selected indexes
        mSelectedIndexes.clear();
    }

    /**
     * Calls the user defined open function
     */
    private void open() {
        if (mOpenFct == null)
            return;

        mOpenFct.accept(getSelectedIndexes());
    }

    /**
     * Calls the user defined remove function
     */
    private void remove() {
        if (mRemoveFct == null)
            return;

        mRemoveFct.accept(getSelectedIndexes());
    }

    /**
     * Calls the user defined move function
     */
    private void move() {
        if (mMoveFct == null)
            return;

        mMoveFct.accept(getSelectedIndexes());
    }

    /**
     * Gives the selected photo indexes
     * @return the selected photo indexes
     */
    private Collection<Integer> getSelectedIndexes() {
        return Collections.unmodifiableSet(mSelectedIndexes);
    }

    /**
     * Sets the function to call when the selection changes
     * @param callBack new callback function
     */
    public void setOnSelectionChangedCallbackFct(Consumer<Collection<Integer>> callBack) {
        mOnSelectionChangedCallbackFct = callBack;
    }

    /**
     * Sets the remove function to call
     * @param removeFct new remove function
     */
    public void setRemoveFct(Consumer<Collection<Integer>> removeFct) {
        this.mRemoveFct = removeFct;
    }

    /**
     * Sets the open function to call
     * @param openFct new open function
     */
    public void setOpenFct(Consumer<Collection<Integer>> openFct) {
        this.mOpenFct = openFct;
    }

    /**
     * Sets the move function to call
     * @param moveFct new open function
     */
    public void setMoveFct(Consumer<Collection<Integer>> moveFct) {
        this.mMoveFct = moveFct;
    }

    /**
     * Sets the new list of photos to display
     * @param photoList new photo list
     */
    public void setPhotos(ObservableList<Photo> photoList) {
        setItems(photoList);
    }

}
