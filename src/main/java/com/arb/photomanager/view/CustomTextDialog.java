package com.arb.photomanager.view;

import javafx.scene.control.TextInputDialog;
import javafx.stage.Stage;

import java.util.Objects;

/**
 * Creates a text input dialog with the application style
 * @author Aymeric Robitaille
 */
public class CustomTextDialog extends TextInputDialog {

    public CustomTextDialog(String def, Stage ownerStage) {
        super(def);

        Objects.requireNonNull(ownerStage);

        initOwner(ownerStage);
        getDialogPane().getStylesheets().addAll(ownerStage.getScene().getRoot().getStylesheets());
    }

    public CustomTextDialog(Stage ownerStage) {
        this("", ownerStage);
    }
}
