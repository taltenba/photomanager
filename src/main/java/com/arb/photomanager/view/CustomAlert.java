package com.arb.photomanager.view;

import javafx.beans.NamedArg;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.util.Objects;

/**
 * @author Aymeric Robitaille
 */
public class CustomAlert extends Alert {

    public CustomAlert(@NamedArg("alertType") AlertType alertType, Stage ownerStage) {
        super(alertType);

        Objects.requireNonNull(ownerStage);

        initOwner(ownerStage);
        getDialogPane().getStylesheets().addAll(ownerStage.getScene().getRoot().getStylesheets());
    }

    public void setTexts(String headerText, String contentText) {
        Label lbl = new Label(contentText);
        lbl.setWrapText(true);

        setHeaderText(headerText);
        getDialogPane().setContent(lbl);
    }
}
