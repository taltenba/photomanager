package com.arb.photomanager.view;

import com.arb.photomanager.model.db.Album;
import com.arb.photomanager.model.db.AlbumDatabase;
import com.arb.photomanager.model.db.AlbumFile;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.MenuItem;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.jetbrains.annotations.Nullable;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Consumer;

/**
 * @author Aymeric Robitaille
 */
public class AlbumTree extends TreeView<String> {
    /*  Private attributes  */

    private AlbumDatabase mAlbumDatabase;
    private Consumer<Boolean> mLoadingStateCallback;
    private Consumer<Album> mOnSelectionChangedCallback;
    private TreeItem<String> mRootItemForLoading;

    //Albums
    final private BiMap<Album, TreeItem<String>> mAlbums;
    final private Map<AlbumFile, Album> mAlbumFiles;

    //Default values
    static final private String RENAME_MENU_MSG = "Rename";
    static final private String REMOVE_MENU_MSG = "Remove";
    static final private String ADD_MENU_MSG = "Add";
    static final private String OPEN_IN_BROWSER_MSG = "Open in explorer";
    static final private String EXPAND_RETRACT_MSG = "Expand/retract";

    public AlbumTree() {
        super(new TreeItem<>("Root"));
        TreeItem<String> root = getRoot();
        mAlbums = HashBiMap.create();
        mAlbumFiles = new HashMap<>();
        mRootItemForLoading = getRoot();

        VBox.setVgrow(this, Priority.ALWAYS);
        root.setExpanded(true);

        initContextMenu();
        initListeners();
    }

    private void initContextMenu() {
        ContextMenu contextMenu = new ContextMenu();

        MenuItem remove = new MenuItem(REMOVE_MENU_MSG);
        MenuItem rename = new MenuItem(RENAME_MENU_MSG);
        MenuItem add = new MenuItem(ADD_MENU_MSG);
        MenuItem openInBrowser = new MenuItem(OPEN_IN_BROWSER_MSG);
        MenuItem expand = new MenuItem(EXPAND_RETRACT_MSG);

        remove.setAccelerator(new KeyCodeCombination(KeyCode.DELETE, KeyCombination.CONTROL_DOWN));
        rename.setAccelerator(new KeyCodeCombination(KeyCode.F2, KeyCombination.CONTROL_DOWN));
        openInBrowser.setAccelerator(new KeyCodeCombination(KeyCode.ENTER, KeyCombination.CONTROL_DOWN));

        contextMenu.getItems().addAll(
                add,
                expand,
                rename,
                remove,
                openInBrowser
        );

        remove.setOnAction(event -> removeAlbum(mAlbums.inverse().get(getSelectionModel().getSelectedItem())));
        rename.setOnAction(event -> renameAlbum(mAlbums.inverse().get(getSelectionModel().getSelectedItem())));
        add.setOnAction(event -> createSubAlbum());
        openInBrowser.setOnAction(event -> openAlbumInBrowser(mAlbums.inverse().get(getSelectionModel().getSelectedItem())));
        expand.setOnAction(event -> {
            TreeItem<String> item = getSelectionModel().getSelectedItem();
            item.setExpanded(!item.isExpanded());
        });

        setContextMenu(contextMenu);
    }

    private void initListeners() {
        getSelectionModel().selectedItemProperty().addListener(this::handleAlbumSelectionChanged);
    }

    private void handleLoadFinished(Album album, Throwable throwable) {
        if (throwable != null) {
            displayLoadingError(throwable);
        } else {
            add(mRootItemForLoading, album);
        }

        setLoading(false);
    }

    private void displayLoadingError(Throwable throwable) {
        CustomAlert al = new CustomAlert(Alert.AlertType.ERROR, (Stage)getParent().getScene().getWindow());
        al.setTexts(
                "Error occurred when loading the directory",
                throwable.getMessage()
        );
        al.showAndWait();
    }

    private void setLoading(boolean isLoading) {
        if (mLoadingStateCallback == null)
            return;

        mLoadingStateCallback.accept(isLoading);
    }

    private void loadSubDir(TreeItem<String> rootItem, Path filePath) {
        mRootItemForLoading = rootItem;
        setLoading(true);
        mAlbumDatabase.openAlbum(filePath, this::handleLoadFinished);
    }

    private void add(TreeItem<String> rootItem, Album album) {
        if (mAlbums.containsKey(album))
            return;

        int index = -1;
        boolean exists = false;

        for (TreeItem<String> item : rootItem.getChildren()) {
            ++index;

            if (item.getValue().equals(album.getName())) {
                exists = true;
                break;
            }

        }
        TreeItem<String> item;

        if (exists) {
            item = rootItem.getChildren().get(index);
            if (getSelectionModel().getSelectedItem().equals(item))
                fireOnSelectedEvent(album);
        } else {
            item = new TreeItem<>(album.getName());
            rootItem.getChildren().add(item);
        }

        mAlbums.put(album, item);
        mAlbumFiles.put(album.getFile(), album);

        album.getFile().addListener(new AlbumFile.ChangeListener() {
            @Override
            public void onAlbumMoved(AlbumFile albumFile, Path oldPath, Path newPath) {
                Album album = mAlbumFiles.get(albumFile);
                TreeItem<String> item = mAlbums.get(album);
                item.setValue(album.getName());
            }

            @Override
            public void onAlbumRemoved(AlbumFile albumFile) {
                Album album = mAlbumFiles.get(albumFile);
                TreeItem<String> item = mAlbums.get(album);
                mAlbums.remove(album);
                mAlbumFiles.remove(albumFile);
                item.getParent().getChildren().remove(item);
                albumFile.removeListener(this);
            }
        });

        for (AlbumFile albumFile : album.getSubAlbumFiles()) {
            item.getChildren().add(new TreeItem<>(albumFile.getAlbumName()));
        }
    }

    /**
     * Handler for the album selection changed (from tree view)
     */
    @SuppressWarnings("unused")
    private void handleAlbumSelectionChanged(ObservableValue obs, TreeItem<String> oldVal, TreeItem<String> newVal) {

        if (newVal.equals(getRoot()))
            fireOnSelectedEvent(null);

        // If the selected item is an un-loaded album, we load it
        if (newVal != getRoot() && !mAlbums.containsValue(newVal)) {
            TreeItem<String> parentItem = newVal.getParent();
            Album rootAlbum =  mAlbums.inverse().get(parentItem);
            Path childPath = rootAlbum.getPath();

            for (AlbumFile albumFile : rootAlbum.getSubAlbumFiles()) {
                if (albumFile.getAlbumName().equals(newVal.getValue())) {
                    childPath = albumFile.getPath();
                    break;
                }
            }

            loadSubDir(
                    parentItem,
                    childPath
            );
        }

        fireOnSelectedEvent(mAlbums.inverse().get(newVal));
    }

    private void fireOnSelectedEvent(@Nullable Album album) {
        if (mOnSelectionChangedCallback == null)
            return;

        mOnSelectionChangedCallback.accept(album);
    }

    /*public void createAlbum(Path path) {
        if (path == null)
            return;

        setLoading(true);

        mRootItemForLoading = getItemFromPath(path);
        mAlbumDatabase.openAlbum(path, (album, throwable) -> {
            handleLoadFinished(album, throwable);

            if (throwable != null)
                return;

            createAlbum(album);
        });
    }*/

    public void createSubAlbum() {
        createAlbum(mAlbums.inverse().get(getSelectionModel().getSelectedItem()));
    }

    /**
     * Creates an album in the root album
     * @param rootAlbum the root album
     */
    private void createAlbum(Album rootAlbum) {
        if (rootAlbum == null)
            return;

        CustomTextDialog txtDial = new CustomTextDialog("", (Stage)getParent().getScene().getWindow());
        txtDial.setHeaderText("New album name");
        txtDial.setContentText("Please enter the new album's name");
        Optional<String> res = txtDial.showAndWait();

        if (res.isEmpty())
            return;

        if (res.get().isBlank())
            return;

        TreeItem<String> item = new TreeItem<>(res.get());
        Path newPath = Paths.get(rootAlbum.getPath().toString(), res.get());

        mAlbums.get(rootAlbum).getChildren().add(item);

        mAlbumDatabase.createAlbum(newPath, t -> {
            CustomAlert al = new CustomAlert(Alert.AlertType.ERROR, (Stage)getParent().getScene().getWindow());
            al.setTexts(
                    "Error occurred when creating the directory",
                    t.getMessage()
            );
            al.showAndWait();
            mAlbums.get(rootAlbum).getChildren().remove(item);
        });
    }

    /**
     * Renames an album
     * @param album the album to rename
     */
    private void renameAlbum(Album album) {
        if (album == null)
            return;

        CustomTextDialog txtDial = new CustomTextDialog(album.getName(), (Stage)getParent().getScene().getWindow());
        txtDial.setHeaderText("New name");
        txtDial.setContentText("Please enter the new album name");
        Optional<String> res = txtDial.showAndWait();

        if (res.isEmpty())
            return;

        if (res.get().isBlank())
            return;

        album.getFile().rename(res.get() ,t -> {
            CustomAlert al = new CustomAlert(Alert.AlertType.ERROR, (Stage)getParent().getScene().getWindow());
            al.setTexts(
                    "Error occurred when loading the directory",
                    t.getMessage()
            );
            al.showAndWait();
        });

    }

    /**
     * Opens an album in the file browser
     * @param album the album
     */
    private void openAlbumInBrowser(Album album) {
        if (album == null)
            return;

        try {
            Desktop.getDesktop().open(new File(album.getPath().toUri()));
        } catch (IOException e) {
            CustomAlert al = new CustomAlert(Alert.AlertType.ERROR, (Stage)getParent().getScene().getWindow());
            al.setTexts(
                    "Error occurred when opening the directory",
                    e.getMessage()
            );
            al.showAndWait();
        }
    }

    /**
     * Removes an album and its photo
     * @param album the album
     */
    private void removeAlbum(Album album) {
        if (album == null)
            return;

        CustomAlert conf = new CustomAlert(Alert.AlertType.CONFIRMATION, (Stage)getParent().getScene().getWindow());
        conf.setTexts(
                "Permanent folder deletion",
                "Please confirm the deletion of the album."
        );

        Optional<ButtonType> res = conf.showAndWait();

        if (res.isEmpty())
            return;

        if (!res.get().getButtonData().equals(ButtonBar.ButtonData.OK_DONE))
            return;

        album.getFile().remove(t -> {
            CustomAlert al = new CustomAlert(Alert.AlertType.ERROR, (Stage)getParent().getScene().getWindow());
            al.setTexts(
                    "Error occurred when loading the directory",
                    t.getMessage()
            );
            al.showAndWait();
        });
    }

    public void selectRoot() {
        getSelectionModel().selectFirst();
    }

    public void setAlbumDatabase(AlbumDatabase albumDatabase) {
        this.mAlbumDatabase = albumDatabase;
    }

    public void setLoadingStateCallback(Consumer<Boolean> loadingStateCallback) {
        this.mLoadingStateCallback = loadingStateCallback;
    }

    public void open(String filePath) {
        if (filePath == null || filePath.isBlank())
            return;

        loadSubDir(getItemFromPath(Path.of(filePath)), Path.of(filePath));
    }

    private TreeItem<String> getItemFromPath(Path filePath) {
        TreeItem<String> defaultVal = getRoot();
        Collection<TreeItem<String>> roots = getRoot().getChildren();

        if (roots == null || roots.isEmpty())
            return defaultVal;

        TreeItem<String> root = null;

        for (TreeItem<String> item : roots) {
            if (filePath.startsWith(mAlbums.inverse().get(item).getPath())) {
                root = item;
                break;
            }
        }

        if (root == null)
            return defaultVal;

        String remaining = filePath.toString().substring(mAlbums.inverse().get(root).getPath().toString().length());

        while (remaining.startsWith("\\")) {
            remaining = remaining.substring(1);
            String actual = remaining.split("\\\\")[0];
            TreeItem<String> newRoot = null;
            for (TreeItem<String> item : root.getChildren()) {
                if (item.getValue().equals(actual)) {
                    newRoot = item;
                    break;
                }
            }

            if (newRoot == null)
                break;

            root = newRoot;

        }

        return root.getParent();
    }

    public void setOnSelectionChangedCallback(Consumer<Album> onSelectionChangedCallback) {
        this.mOnSelectionChangedCallback = onSelectionChangedCallback;
    }

    public TreeItem<String> getRootItem() {
        return getRoot();
    }

    public Collection<Path> getRootFolderPaths() {
        Collection<Path> stringPaths = new HashSet<>();

        for (TreeItem<String> item : getRoot().getChildren())
            stringPaths.add(
                    mAlbums.inverse().get(item).getPath()
            );

        return stringPaths;
    }

    public Album getAlbum(TreeItem<String> item) {
        return mAlbums.inverse().get(item);
    }

    public Path getFullPath(String pathStr) {
        Collection<Path> rootPaths = getRootFolderPaths();
        String root = pathStr.split("\\\\")[0];
        Path foundPath = null;

        for (Path p : rootPaths) {
            if (root.equals(p.getName(p.getNameCount() - 1).toString())) {
                foundPath = p;
                break;
            }
        }

        if (foundPath == null)
            return null;

        return Path.of(
                foundPath.getRoot().toString() +
                        foundPath.subpath(0, foundPath.getNameCount() - 1).toString() +
                        "\\" + pathStr
        );
    }

}
