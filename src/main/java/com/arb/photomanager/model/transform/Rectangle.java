package com.arb.photomanager.model.transform;

import java.util.Objects;

/**
 * A 2D rectangle with integer coordinates and size.
 *
 * @author Thomas Altenbach
 */
public final class Rectangle {
    private final int mX;
    private final int mY;
    private final int mWidth;
    private final int mHeight;

    /**
     * Creates a new rectangle
     * @param x the x-coordinate of the upper-left corner of the rectangle
     * @param y the y-coordinate of the upper-left corner of the rectangle
     * @param width the width of the rectangle
     * @param height the height of the rectangle
     */
    public Rectangle(int x, int y, int width, int height) {
        if (width < 0 || height < 0)
            throw new IllegalArgumentException("Width and height of the rectangle must be positive or zero integers.");

        mX = x;
        mY = y;
        mWidth = width;
        mHeight = height;
    }

    /**
     * Returns the x-coordinate of the upper-left corner of this rectangle.
     * @return the x-coordinate of the upper-left corner of this rectangle
     */
    public int getX() {
        return mX;
    }

    /**
     * Returns the y-coordinate of the upper-left corner of this rectangle.
     * @return the y-coordinate of the upper-left corner of this rectangle
     */
    public int getY() {
        return mY;
    }

    /**
     * Returns the width of this rectangle.
     * @return the width of this rectangle
     */
    public int getWidth() {
        return mWidth;
    }

    /**
     * Returns the height of this rectangle.
     * @return the height of this rectangle
     */
    public int getHeight() {
        return mHeight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (!(o instanceof Rectangle))
            return false;

        Rectangle rectangle = (Rectangle) o;

        return mX == rectangle.mX &&
                mY == rectangle.mY &&
                mWidth == rectangle.mWidth &&
                mHeight == rectangle.mHeight;
    }

    @Override
    public int hashCode() {
        return Objects.hash(mX, mY, mWidth, mHeight);
    }
}
