package com.arb.photomanager.model.transform;

import com.arb.photomanager.util.BackgroundNodeSnapshooter;
import com.google.gson.Gson;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.Image;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.WritableImage;
import javafx.scene.image.WritablePixelFormat;
import javafx.scene.paint.Color;
import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.interpolation.SplineInterpolator;
import org.apache.commons.math3.analysis.polynomials.PolynomialFunction;
import org.jetbrains.annotations.Nullable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.ExecutionException;

/**
 * An image color filter.
 * @author Thomas Altenbach
 */
public final class ImageFilter {
    private static final byte[] IDENTITY_CURVE = new byte[256];

    static {
        for (int i = 0; i < 256; ++i)
            IDENTITY_CURVE[i] = (byte) i;
    }

    /**
     * Enumeration of the color channels of an image.
     */
    public enum ColorChannel {
        RED,
        GREEN,
        BLUE
    }

    //private final Map<ColorChannel, PolynomialSplineFunction> mColorCurves;
    private final ImageFilter mInputFilter;
    private final ColorAdjust mColorAdjust;
    private final Map<ColorChannel, byte[]> mColorCurves;

    /**
     * Creates a new image filter from the specified {@link ColorAdjust}.
     * @param colorAdjust the {@link ColorAdjust} using which the newly created filter must be initialized
     */
    public ImageFilter(ColorAdjust colorAdjust) {
        mInputFilter = null;
        mColorAdjust = new ColorAdjust(colorAdjust.getHue(), colorAdjust.getSaturation(),
                colorAdjust.getBrightness(), colorAdjust.getContrast());

        mColorCurves = null;
    }

    private ImageFilter(ImageFilter inputFilter, ColorAdjust colorAdjust, Map<ColorChannel, byte[]> colorCurves) {
        mInputFilter = inputFilter;
        mColorAdjust = colorAdjust;

        boolean identityCurves = true;

        for (ColorChannel c : ColorChannel.values()) {
            if (colorCurves.get(c) != IDENTITY_CURVE) {
                identityCurves = false;
                break;
            }
        }

        if (identityCurves)
            mColorCurves = null;
        else
            mColorCurves = colorCurves;
    }

    /**
     * Filters an image.
     * @param image the image to filter.
     * @return the filtered image.
     */
    public Image filter(Image image) throws ExecutionException, InterruptedException {
        int width = (int) image.getWidth();
        int height = (int) image.getHeight();

        Image baseImage = mInputFilter == null ? image : mInputFilter.filter(image);
        WritableImage outImage = applyColorAdjust(baseImage);

        if (mColorCurves != null) {
            WritablePixelFormat<ByteBuffer> pixelFormat = PixelFormat.getByteBgraInstance();

            int byteCount = 4 * width * height;
            byte[] bytes = new byte[byteCount];

            outImage.getPixelReader().getPixels(0, 0, width, height, pixelFormat, bytes, 0, 4 * width);

            byte[] blueCurve = mColorCurves.get(ColorChannel.BLUE);
            byte[] greenCurve = mColorCurves.get(ColorChannel.GREEN);
            byte[] redCurve = mColorCurves.get(ColorChannel.RED);

            for (int i = 0; i < byteCount; i += 4) {
                bytes[i] = blueCurve[bytes[i] & 0xFF];
                bytes[i + 1] = greenCurve[bytes[i + 1] & 0xFF];
                bytes[i + 2] = redCurve[bytes[i + 2] & 0xFF];
            }

            outImage.getPixelWriter().setPixels(0, 0, width, height, pixelFormat, bytes, 0, 4 * width);
        }

        return outImage;
    }

    private WritableImage applyColorAdjust(Image image) throws ExecutionException, InterruptedException {
        Canvas canvas = new Canvas(image.getWidth(), image.getHeight());
        canvas.setEffect(mColorAdjust);
        canvas.getGraphicsContext2D().drawImage(image, 0.0, 0.0);

        SnapshotParameters params = new SnapshotParameters();
        params.setFill(Color.TRANSPARENT);

        return BackgroundNodeSnapshooter.snapshot(canvas, params, null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (!(o instanceof ImageFilter))
            return false;

        ImageFilter that = (ImageFilter) o;
        return mColorAdjust.equals(that.mColorAdjust) &&
                Objects.equals(mColorCurves, that.mColorCurves);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mColorAdjust, mColorCurves);
    }

    private static ImageFilter deserialize(Gson gson, Path path) throws IOException {
        URL url = path.toUri().toURL();

        try (Reader reader = new BufferedReader(new InputStreamReader(url.openStream()))) {
            FilterInfo info = gson.fromJson(reader, FilterInfo.class);
            return new ImageFilter.Builder(info).build();
        }
    }

    /**
     * Deserializes several JSON serialized image filters.
     * @param paths the paths of the files containing the serialized image filter (one per file)
     * @return the deserialized image filters
     * @throws IOException if an I/O error occurs
     */
    public static List<ImageFilter> deserialize(Collection<Path> paths) throws IOException {
        Gson gson = new Gson();
        List<ImageFilter> imageFilters = new ArrayList<>(paths.size());

        for (Path p : paths)
            imageFilters.add(deserialize(gson, p));

        return imageFilters;
    }

    /**
     * Deserializes a JSON serialized image filter.
     * @param path the path of the file containing the serialized image filter
     * @return the deserialized image filter
     * @throws IOException if an I/O error occurs
     */
    public static ImageFilter deserialize(Path path) throws IOException {
        return deserialize(new Gson(), path);
    }

    private static final class CurveData {
        private static final CurveData IDENTITY = new CurveData();

        private double[] x;
        private double[] y;
    }

    private static final class FilterInfo {
        private final Map<ColorChannel, CurveData> mColorCurveData;
        private final double mHue;
        private final double mSaturation;
        private final double mBrightness;
        private final double mContrast;

        public FilterInfo() {
            mColorCurveData = new EnumMap<>(ColorChannel.class);

            for (ColorChannel c : ColorChannel.values())
                mColorCurveData.put(c, CurveData.IDENTITY);

            mHue = 0.0;
            mSaturation = 0.0;
            mBrightness = 0.0;
            mContrast = 0.0;
        }
    }

    /**
     * Builder that can be used to build an {@link ImageFilter}.
     */
    public static final class Builder {
        private final Map<ColorChannel, CurveData> mColorCurveData;
        private ImageFilter mInputFilter;
        private ColorAdjust mColorAdjust;

        /**
         * Creates a new builder, representing the identity image filter.
         */
        public Builder() {
            mColorCurveData = new EnumMap<>(ColorChannel.class);
            mColorAdjust = new ColorAdjust();

            for (ColorChannel c : ColorChannel.values())
                mColorCurveData.put(c, CurveData.IDENTITY);
        }

        /**
         * Creates a new builder from the specified {@link ColorAdjust}.
         *
         * <p> Note that the given {@link ColorAdjust} instance is not copied, therefore
         * changes to the latter are reflected in this builder.
         *
         * @param colorAdjust the {@link ColorAdjust} used to initialize this builder
         */
        public Builder(ColorAdjust colorAdjust) {
            this();
            mColorAdjust = colorAdjust;
        }

        private Builder(FilterInfo info) {
            mColorCurveData = info.mColorCurveData;
            mColorAdjust = new ColorAdjust(info.mHue, info.mSaturation, info.mBrightness, info.mContrast);
        }

        /**
         * Returns the input filter of this builder, if any.
         * @return the input filter of this builder, if any,
         *         {@code null} otherwise
         */
        @Nullable
        public ImageFilter getInputFilter() {
            return mInputFilter;
        }

        /**
         * Returns x coordinates of the color curve data corresponding to the given chanel
         * @return x coordinates of the color curve data corresponding to the given chanel
         */
        @Nullable
        public double[] getXColorData(ColorChannel channel) {
            CurveData data = mColorCurveData.get(channel);
            return data.x;
        }

        /**
         * Returns y coordinates of the color curve data corresponding to the given chanel
         * @return y coordinates of the color curve data corresponding to the given chanel
         */
        @Nullable
        public double[] getYColorData(ColorChannel channel) {
            CurveData data = mColorCurveData.get(channel);
            return data.y;
        }

        /**
         * Sets the input filter of this builder.
         * @param filter the input filter (can be {@code null})
         * @return this builder
         */
        public Builder setInputFilter(@Nullable ImageFilter filter) {
            mInputFilter = filter;

            return this;
        }

        /**
         * Returns the {@link ColorAdjust} instance associated with this builder.
         *
         * <p> Note that the returned {@link ColorAdjust} instance is not a copy, therefore
         * changes to the returned instance are reflected in this builder.
         *
         * @return the {@link ColorAdjust} instance associated with this builder
         */
        public ColorAdjust getColorAdjust() {
            return mColorAdjust;
        }

        /**
         * Sets the {@link ColorAdjust} associated with this builder.
         *
         * <p> Note that the given {@link ColorAdjust} instance is not copied, therefore
         * changes to the latter are reflected in this builder.
         *
         * @param adjust the {@link ColorAdjust} to associate with this builder
         * @return this builder
         */
        public Builder setColorAdjust(ColorAdjust adjust) {
            mColorAdjust = adjust;
            return this;
        }

        /**
         * Sets the curve points from which the specified color channel curve of the image filter
         * represented by this builder will be interpolated from.
         *
         * @param channel the color channel
         * @param x the x-coordinates of the points
         * @param y the y-coordinates of the points
         * @return this builder
         */
        public Builder setColorCurve(ColorChannel channel, double[] x, double[] y) {
            if (x.length != y.length)
                throw new IllegalArgumentException("x and y arrays must have the same length.");

            if (x.length < 2)
                throw new IllegalArgumentException("There must be at least two interpolation points.");

            CurveData data = mColorCurveData.get(channel);

            if (data == CurveData.IDENTITY) {
                data = new CurveData();
                mColorCurveData.put(channel, data);
            }

            data.x = x;
            data.y = y;

            return this;
        }

        /**
         * Build the image filter represented by this builder.
         * @return the image filter represented by this builder
         */
        public ImageFilter build() {
            SplineInterpolator interpolator = new SplineInterpolator();
            Map<ColorChannel, byte[]> colorCurves = new EnumMap<>(ColorChannel.class);

            for (ColorChannel c : ColorChannel.values()) {
                CurveData curveData = mColorCurveData.get(c);

                if (curveData == CurveData.IDENTITY)
                    colorCurves.put(c, IDENTITY_CURVE);
                else
                    colorCurves.put(c, computeColorCurve(mColorCurveData.get(c), interpolator));
            }

            ColorAdjust colorAdjust = new ColorAdjust(mColorAdjust.getHue(), mColorAdjust.getSaturation(),
                    mColorAdjust.getBrightness(), mColorAdjust.getContrast());

            return new ImageFilter(mInputFilter, colorAdjust, colorCurves);
        }

        private byte[] computeColorCurve(CurveData data, SplineInterpolator interpolator) {
            byte[] curve = new byte[256];
            UnivariateFunction curveFunc;

            if (data.x.length == 2) {
                double deltaX = data.x[0] - data.x[1];
                double[] c = new double[2];
                c[0] = (data.x[0] * data.y[1] - data.x[1] * data.y[0]) / deltaX;
                c[1] = (data.y[0] - data.y[1]) / deltaX;

                curveFunc = new PolynomialFunction(c);
            } else {
                curveFunc = interpolator.interpolate(data.x, data.y);
            }

            for (int i = 0; i < 256; ++i)
                curve[i] = (byte) Math.max(0, Math.min(curveFunc.value(i), 255));

            return curve;
        }
    }
}
