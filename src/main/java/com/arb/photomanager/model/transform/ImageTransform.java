package com.arb.photomanager.model.transform;

import java.util.Objects;

/**
 * A transformation that can be applied to an image. A transformation can combine cropping,
 * rotation and image filtering.
 *
 * @author Thomas Altenbach
 */
public final class ImageTransform {
    private final ImageFilter mImageFilter;
    private final Rectangle mRectangle;
    private final int mRotation;

    /**
     * Creates a new image transformation.
     * @param imageFilter the image filter
     * @param rectangle the {@link Rectangle} representing the cropped image surface
     * @param rotation the image rotation in degrees
     */
    public ImageTransform(ImageFilter imageFilter, Rectangle rectangle, int rotation) {
        if (imageFilter == null || rectangle == null)
            throw new NullPointerException("Neither filter nor insets can be null.");

        mImageFilter = imageFilter;
        mRectangle = rectangle;
        mRotation = rotation;
    }

    /**
     * Returns the image filter of this transform.
     * @return the image filter of this transform
     */
    public ImageFilter getImageFilter() {
        return mImageFilter;
    }

    /**
     * Returns the {@link Rectangle} representing the cropped image surface of this transformation.
     * @return the {@link Rectangle} representing the cropped image surface of this transformation
     */
    public Rectangle getRectangle() {
        return mRectangle;
    }

    /**
     * Returns the image rotation of this transformation.
     * @return the image rotation in degrees of this transformation
     */
    public int getRotation() {
        return mRotation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (!(o instanceof ImageTransform))
            return false;

        ImageTransform transformer = (ImageTransform) o;

        return mRotation == transformer.mRotation &&
                mImageFilter.equals(transformer.mImageFilter) &&
                mRectangle.equals(transformer.mRectangle);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mImageFilter, mRectangle, mRotation);
    }
}
