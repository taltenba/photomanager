package com.arb.photomanager.model.db;

import com.arb.photomanager.model.transform.ImageTransform;
import com.arb.photomanager.model.transform.Rectangle;
import com.arb.photomanager.util.BackgroundNodeSnapshooter;
import com.arb.photomanager.util.ImageThumbnailer;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import org.apache.commons.imaging.*;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.jpeg.exif.ExifRewriter;
import org.apache.commons.imaging.formats.tiff.TiffImageMetadata;
import org.apache.commons.imaging.formats.tiff.constants.TiffDirectoryConstants;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputDirectory;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputField;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputSet;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.Nullable;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.lang.ref.WeakReference;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletionException;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.function.BiConsumer;

/**
 * Photo file contained in an album.
 *
 * <p> Public methods of this class are only intended to be called in a single thread,
 * being the callback thread of the {@link AlbumDatabase} instance that has been used
 * to open the album from which an instance of this class originates.
 *
 * @author Thomas Altenbach
 */
public final class Photo {
    private static final Set<String> SUPPORTED_IMAGE_FORMATS = Set.of(".bmp", "jpg", "jpeg", "gif", "png");

    /**
     * Enumeration of the supported image formats of a photo.
     */
    public enum ImageFormat {
        BMP,
        JPG,
        GIF,
        PNG;

        static ImageFormat from(org.apache.commons.imaging.ImageFormat apacheFormat) {
            if (!(apacheFormat instanceof ImageFormats))
                return null;

            ImageFormats enumFormat = (ImageFormats) apacheFormat;

            switch (enumFormat) {
                case BMP:
                    return BMP;
                case JPEG:
                    return JPG;
                case GIF:
                    return GIF;
                case PNG:
                    return PNG;
                default:
                    return null;
            }
        }
    }

    private final DatabaseExecutor mExecutor;
    private final ImageThumbnailer mThumbnailer;
    private final PhotoTagDatabase mTagDatabase;
    private final ImageFormat mImageFormat;
    private final TiffImageMetadata mMetadata;
    private final AlbumFile mAlbumFile;
    private final long mThumbnailKey;
    private final long mCreationTime;

    // No need to be set volatile since visibility to callback thread is guaranteed by the Future implementation
    private String mName;
    private Path mPhotoPath;

    private WeakReference<Image> mImage;
    private ImageInfo mImageInfo;
    private long mDiskSize;
    private long mLastModifiedTime;
    
    private final List<ChangeListener> mChangeListeners;
    private final List<ChangeListener> mBackgroundChangeListeners;

    @SuppressWarnings("FieldCanBeLocal") // Cannot, used as weak listener
    private final AlbumFile.ChangeListener mAlbumFileChangeListener = new AlbumFile.ChangeListener() {
        @Override
        public void onAlbumMoved(AlbumFile albumFile, Path oldPath, Path newPath) {
            mPhotoPath = newPath.resolve(mName);
        }

        @Override
        public void onAlbumRemoved(AlbumFile albumFile) {
            onRemoved();
        }
    };

    Photo(DatabaseExecutor executor, ImageThumbnailer thumbnailer, String name, AlbumFile albumFile,
          PhotoTagDatabase tagDatabase) throws IOException, ImageReadException
    {
        mName = Objects.requireNonNull(name, "Name cannot be null");
        mAlbumFile = Objects.requireNonNull(albumFile, "Album file cannot be null");
        mPhotoPath = mAlbumFile.resolve(mName);

        if (!Files.exists(mPhotoPath))
            throw new FileNotFoundException("Photo file does not exist");

        PhotoInfo info = PhotoInfo.of(mPhotoPath);
        mImageFormat = info.mImageFormat;
        mMetadata = info.mMetadata;
        mCreationTime = info.mCreationTime;
        mImageInfo = info.mImageInfo;
        mDiskSize = info.mDiskSize;
        mLastModifiedTime = info.mLastModifiedTime;

        mExecutor = Objects.requireNonNull(executor, "Database executor cannot be null");
        mThumbnailer = Objects.requireNonNull(thumbnailer, "Thumbnailer cannot be null");
        mTagDatabase = Objects.requireNonNull(tagDatabase, "Photo tag DAO cannot be null");
        mThumbnailKey = mThumbnailer.getUniqueKey();
        mImage = new WeakReference<>(null);
        mChangeListeners = new CopyOnWriteArrayList<>();
        mBackgroundChangeListeners = new CopyOnWriteArrayList<>();

        albumFile.addWeakBackgroundListener(mAlbumFileChangeListener);
    }

    void addBackgroundChangeListener(ChangeListener listener) {
        mBackgroundChangeListeners.add(listener);
    }

    void removeBackgroundChangeListener(ChangeListener listener) {
        mBackgroundChangeListeners.remove(listener);
    }

    /**
     * Returns the name of this photo file.
     * @return the name of this photo file
     */
    public String getName() {
        return mName;
    }

    /**
     * Returns the album file in which this photo is contained.
     * @return the album file in which this photo is contained
     */
    public AlbumFile getAlbumFile() {
        return mAlbumFile;
    }

    /**
     * Returns the path of this photo file
     * @return the path of this photo file
     */
    public Path getPath() {
        return mAlbumFile.getPath().resolve(mName);
    }

    /**
     * Returns the {@link ImageInfo} associated with this photo.
     * @return the {@link ImageInfo} associated with this photo
     */
    public ImageInfo getImageInfo() {
        return mImageInfo;
    }

    /**
     * Returns the format of this photo.
     * @return the format of this photo
     */
    public ImageFormat getImageFormat() {
        return mImageFormat;
    }

    /**
     * Returns the creation time of this photo.
     * @return the creation time of this photo
     */
    public long getCreationTime() {
        return mCreationTime;
    }

    /**
     * Returns the size of this photo file.
     * @return the size of this photo file in bytes
     */
    public long getDiskSize() {
        return mDiskSize;
    }

    private Image getImageSync() throws Exception {
        Image img = mImage.get();

        if (img != null)
            return img;

        img = new Image(mPhotoPath.toUri().toString());

        if (img.getException() != null)
            throw img.getException();

        mImage = new WeakReference<>(img);

        return img;
    }

    /**
     * Loads asynchronously the image associated with this photo.
     *
     * <p> The given callback is invoked with the loaded image ({@code null} iff an error occurred)
     * and the load exception ({@code null} iff none) as arguments when the image is loaded or an error
     * occurred during the load.
     *
     * @param imageReadyCallback callback invoked when the image is loaded or an error occurred during
     *                           the load.
     */
    public void getImage(BiConsumer<Image, Throwable> imageReadyCallback) {
        mExecutor.supplyBackground(() -> {
            try {
                return getImageSync();
            } catch (Exception e) {
                throw new CompletionException(e);
            }
        }).whenCompleteAsync((r, t) -> {
            Throwable cause = t == null ? null : t.getCause();
            imageReadyCallback.accept(r, cause);
        }, mExecutor.getCallbackExecutor());
    }

    /**
     * Fills asynchronously a byte array with content of this photo file.
     *
     * <p> The given callback is invoked with the read bytes ({@code null} array iff an error occurred)
     * and the reading exception ({@code null} iff none) as arguments when the whole image file is read or
     * an error occurred during the reading.
     *
     * @param imageBytesReadyCallback callback invoked when the whole image file is read or
     *                                an error occurred during the reading.
     */
    public void getImageBytes(BiConsumer<byte[], Throwable> imageBytesReadyCallback) {
        mExecutor.supplyBackground(() -> {
            try {
                return FileUtils.readFileToByteArray(mPhotoPath.toFile());
            } catch (Exception e) {
                throw new CompletionException(e);
            }
        }).whenCompleteAsync((r, t) -> {
            Throwable cause = t == null ? null : t.getCause();
            imageBytesReadyCallback.accept(r, cause);
        }, mExecutor.getCallbackExecutor());
    }

    /**
     * Creates a thumbnail image for this photo.
     *
     * <p> The given callback is invoked with the thumbnail image ({@code null} iff an error occurred)
     * and the exception that occurred the thumbnailing process ({@code null} iff none) as arguments
     * when the thumbnail image is ready or an error occurred during the thumbnailing process.
     *
     * <p> Note that the newly created thumbnail image is cached for speeding up the next calls to
     * this method.
     *
     * @param thumbnailReadyCallback callback invoked when the thumbnail image is ready or an error
     *                               occurred during the thumbnailing process.
     */
    public void getThumbnail(BiConsumer<Image, Throwable> thumbnailReadyCallback) {
        mExecutor.supplyBackground(() -> {
            Image thumbnail;
            
            try {
                if (mImage.get() == null)
                    thumbnail = mThumbnailer.getThumbnail(mThumbnailKey, mPhotoPath);
                else
                    thumbnail = mThumbnailer.getThumbnail(mThumbnailKey, this::getImageSync);
            } catch (Exception e) {
                throw new CompletionException(e);
            }

            return thumbnail;
        }).whenCompleteAsync((r, t) -> {
            Throwable cause = t == null ? null : t.getCause();
            thumbnailReadyCallback.accept(r, cause);
        }, mExecutor.getCallbackExecutor());
    }

    /**
     * Indicates whether or not this photo file contains image metadata.
     * @return true iff this photo file contains image metadata
     */
    public boolean hasMetadata() {
        return mMetadata != null;
    }

    /**
     * Returns the EXIF metadata associated with this photo file, if any.
     * @return the EXIF metadata associated with this photo file if any,
     *         {@code null} otherwise.
     */
    @Nullable
    public TiffImageMetadata getMetadata() {
        return mMetadata;
    }

    /**
     * Returns an unmodifiable view of the tags associated with this photo.
     * @return an unmodifiable view of the tags associated with this photo
     */
    public Set<String> getTags() {
        return mTagDatabase.getTags(mName);
    }

    /**
     * Indicates whether or not this photo is tagged with the specified tag.
     * @param tag The tag
     * @return true iff this photo is tagged with the specified tag
     */
    public boolean hasTag(String tag) {
        return mTagDatabase.contains(mName, tag);
    }

    /**
     * Renames asynchronously this photo file.
     * @param newName the new name of the file
     * @param overwrite if set to true, in case a file with the specified name already
     *                  exists then the latter is overwritten with this photo file content
     * @param errorCallback optional callback invoked in case of failure
     */
    public void rename(String newName, boolean overwrite, @Nullable IOErrorCallback errorCallback) {
        Objects.requireNonNull(newName);

        mExecutor.supplyIOBackground(
                () -> {
                    String oldName = mName;

                    if (oldName.equals(newName))
                        return null;

                    Path path = mPhotoPath;

                    try {
                        if (overwrite)
                            Files.move(path, path.resolveSibling(newName), StandardCopyOption.REPLACE_EXISTING);
                        else
                            Files.move(path, path.resolveSibling(newName));
                    } catch (IOException e) {
                        throw new CompletionException(e);
                    }

                    mTagDatabase.renamePhoto(oldName, newName);

                    mName = newName;
                    mPhotoPath = mAlbumFile.resolve(newName);

                    //updateLastModifiedTime();

                    for (ChangeListener l : mBackgroundChangeListeners)
                        l.onNameChanged(this, oldName, newName);

                    return oldName;
                }, errorCallback)
                .thenAcceptAsync((oldName) -> {
                    if (oldName == null)
                        return;

                    for (ChangeListener l : mChangeListeners)
                        l.onNameChanged(this, oldName, newName);
                }, mExecutor.getCallbackExecutor());
    }

    /**
     * Copies asynchronously this photo file to the specified location.
     *
     * @param path the path to the target file
     * @param overwrite if set to true, if the target file already exist, then the latter
     *                  is overwritten with this photo file content
     * @param errorCallback optional callback invoked in case of failure
     */
    // TODO: handle format change ?
    // TODO: copy tags with photo
    public void copyTo(Path path, boolean overwrite, @Nullable IOErrorCallback errorCallback) {
        Objects.requireNonNull(path);

        mExecutor.runIOBackground(() -> {
            Path currentPath = mPhotoPath;

            if (currentPath.equals(path))
                return;

            try {
                if (overwrite)
                    Files.copy(currentPath, path, StandardCopyOption.REPLACE_EXISTING);
                else
                    Files.copy(currentPath, path);
            } catch (IOException e) {
                throw new CompletionException(e);
            }
        }, errorCallback);
    }

    /**
     * Copies asynchronously this photo file to within the specified album file, with the
     * specified target name.
     *
     * @param albumFile the album file in which to copy this album
     * @param outName the target photo name
     * @param overwrite if set to true, if the target file already exist, then the latter
     *                  is overwritten with this photo file content
     * @param errorCallback optional callback invoked in case of failure
     */
    public void copyTo(AlbumFile albumFile, String outName, boolean overwrite, @Nullable IOErrorCallback errorCallback) {
        copyTo(albumFile.resolve(outName), overwrite, errorCallback);
    }

    /**
     * Copies asynchronously this photo file to within the specified album file, with the
     * current photo name.
     *
     * @param albumFile the album file in which to copy this album
     * @param overwrite if set to true, if the target file already exist, then the latter
     *                  is overwritten with this photo file content
     * @param errorCallback optional callback invoked in case of failure
     */
    public void copyTo(AlbumFile albumFile, boolean overwrite, @Nullable IOErrorCallback errorCallback) {
        copyTo(albumFile.resolve(mName), overwrite, errorCallback);
    }

    /*public void moveTo(Path path, boolean overwrite, @Nullable IOErrorCallback errorCallback) {
        Objects.requireNonNull(path);

        if (getAlbumPath().getPath().equals(path.getParent())) {
            rename(path.getFileName().toString(), overwrite, errorCallback);
            return;
        }

        mExecutor.supplyIOBackground(() -> {
            Path currentPath = getPhotoPath();

            if (currentPath.equals(path))
                return false;

            // TODO
        })

    }

    public void moveTo(AlbumPath albumPath, String name) {
        // TODO
    }*/

    /**
     * Removes this photo file.
     * @param errorCallback optional callback invoked in case of failure
     */
    public void remove(@Nullable IOErrorCallback errorCallback) {
        mExecutor.runIOBackground(() -> {
            if (!Files.exists(mPhotoPath))
                return;

            try {
                Files.delete(mPhotoPath);
            } catch (IOException e) {
                throw new CompletionException(e);
            }

            onRemoved();
        }, errorCallback);
    }

    // Must be called in database background thread
    private void onRemoved() {
        mAlbumFile.removeBackgroundListener(mAlbumFileChangeListener);
        mThumbnailer.invalidateThumbnail(mThumbnailKey);

        for (ChangeListener l : mBackgroundChangeListeners)
            l.onRemoved(this);

        mExecutor.getCallbackExecutor().execute(() -> {
            for (ChangeListener l : mChangeListeners)
                l.onRemoved(this);
        });

        mTagDatabase.removeAllTags(mName);
    }

    /**
     * Transforms asynchronously the image associated with this photo file and overwrite the content
     * of the latter with the resulting image.
     *
     * @param transform the image transform
     * @param errorCallback optional callback invoked in case of failure
     */
    public void transformOverwrite(ImageTransform transform, @Nullable IOErrorCallback errorCallback) {
        // TODO: need to check if mPhotoPath can change before IO task ?
        transform(transform, mPhotoPath, true, errorCallback);
    }

    /**
     * Transforms asynchronously the image associated with this photo file and save the resulting
     * image at the specified location.
     *
     * @param transform the image transform
     * @param outPath the path to the target file
     * @param overwrite if set to true, if the target file already exist, then the latter
     *                  is overwritten with the resulting image
     * @param errorCallback optional callback invoked in case of failure
     */
    public void transform(ImageTransform transform, Path outPath, boolean overwrite, @Nullable IOErrorCallback errorCallback) {
        Objects.requireNonNull(transform);
        Objects.requireNonNull(outPath);

        final Path fullOutPath = addPathExtension(outPath, mImageFormat);

        mExecutor.supplyIOBackground(() -> {
            if (!overwrite && Files.exists(fullOutPath))
                throw new CompletionException(new FileAlreadyExistsException(fullOutPath.toString()));

            Rectangle rectangle = transform.getRectangle();
            int w = rectangle.getWidth();
            int h = rectangle.getHeight();

            Image image;

            try {
                image = getImageSync();
            } catch (Exception e) {
                throw new CompletionException(e);
            }

            Canvas canvas = new Canvas(w, h);
            canvas.setRotate(transform.getRotation());
            GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
            graphicsContext.drawImage(image, rectangle.getX(), rectangle.getY(), w, h, 0, 0, w, h);
            graphicsContext.rotate(transform.getRotation());

            SnapshotParameters params = new SnapshotParameters();
            params.setFill(Color.TRANSPARENT);

            try {
                image = BackgroundNodeSnapshooter.snapshot(canvas, params, null);
                image = transform.getImageFilter().filter(image);
            } catch (ExecutionException | InterruptedException e) {
                throw new CompletionException(e);
            }

            byte[] newPhotoBytes;

            try {
                newPhotoBytes = writePhoto(image, mImageFormat, fullOutPath);
            } catch (IOException | ImageWriteException | ImageReadException e) {
                throw new CompletionException(e);
            }

            if (fullOutPath.equals(mPhotoPath)) {
                mImage = new WeakReference<>(image);
                mDiskSize = newPhotoBytes.length;
                mThumbnailer.invalidateThumbnail(mThumbnailKey);

                try {
                    mImageInfo = Imaging.getImageInfo(newPhotoBytes);
                } catch (ImageReadException | IOException e) {
                    // Cannot throw, bytes are properly formatted
                    e.printStackTrace();
                }

                updateLastModifiedTime();

                for (ChangeListener l : mBackgroundChangeListeners)
                    l.onImageChanged(this);

                return true;
            }

            return false;
        }, errorCallback)
        .thenAcceptAsync((result) -> {
            if (!result)
                return;

            for (ChangeListener l : mChangeListeners)
                l.onImageChanged(this);
        }, mExecutor.getCallbackExecutor());
    }

    /**
     * Transforms asynchronously the image associated with this photo file and save the resulting image
     * to within the specified album file, with the specified target name.
     *
     * @param transform the image transform
     * @param outAlbumFile the album file in which to save the resulting image
     * @param outName the target image name
     * @param overwrite if set to true, if the target file already exist, then the latter
     *                  is overwritten with the resulting image
     * @param errorCallback optional callback invoked in case of failure
     */
    public void transform(ImageTransform transform, AlbumFile outAlbumFile, String outName, boolean overwrite, @Nullable IOErrorCallback errorCallback) {
        transform(transform, outAlbumFile.resolve(outName), overwrite, errorCallback);
    }

    /**
     * Tags this photo with the specified tag.
     * @param tag the tag
     */
    public void addTag(String tag) {
        if (!mTagDatabase.addTag(mName, tag))
            return;

        mExecutor.runBackground(() -> {
            for (ChangeListener l : mBackgroundChangeListeners)
                l.onTagAdded(this, tag);
        });

        for (ChangeListener l : mChangeListeners)
            l.onTagAdded(this, tag);
    }

    /**
     * Removes the specified tag from the tag list of this photo
     * @param tag the tag
     */
    public void removeTag(String tag) {
        if (!mTagDatabase.removeTag(mName, tag))
            return;

        mExecutor.runBackground(() -> {
            for (ChangeListener l : mBackgroundChangeListeners)
                l.onTagRemoved(this, tag);
        });

        for (ChangeListener l : mChangeListeners)
            l.onTagRemoved(this, tag);
    }

    /**
     * Adds a change listener to this photo.
     * @param listener The listener
     */
    public void addListener(ChangeListener listener) {
        mChangeListeners.add(listener);
    }

    /**
     * Removes a listener from this photo.
     * If the listener is not attached to this photo, nothing happens.
     *
     * @param listener The listener
     */
    public void removeListener(ChangeListener listener) {
        mChangeListeners.remove(listener);
    }

    // Must be called from database background thread
    /*void refresh() throws IOException {
        if (mLastModifiedTime >= Files.getLastModifiedTime(mPhotoPath).toMillis())
            return;

        mImage = new WeakReference<>(null);
        mExecutor.getThumbnailCache().remove(mThumbnailKey);

        // TODO
        // Update last modified time
    }*/

    private Path addPathExtension(Path path, ImageFormat format) {
        String formatExt = format.name().toLowerCase();

        if (path.endsWith(path))
            return path;

        return Path.of(path.toString().concat(formatExt));
    }

    private void writeMetadata(byte[] imageBytes, ByteArrayOutputStream outStream) throws ImageWriteException, ImageReadException, IOException {
        TiffOutputSet outset = mMetadata.getOutputSet();

        // Remove thumbnails contained in the metadata since the image has changed
        TiffOutputDirectory thumbnailDirectory = outset.findDirectory(TiffDirectoryConstants.DIRECTORY_TYPE_DIR_1);
        thumbnailDirectory.setTiffImageData(null);
        thumbnailDirectory.setJpegImageData(null);

        for (TiffOutputField field : thumbnailDirectory.getFields())
            thumbnailDirectory.removeField(field.tagInfo);

        new ExifRewriter().updateExifMetadataLossless(imageBytes, outStream, outset);
    }

    private byte[] writePhoto(Image image, ImageFormat imageFormat, Path outPath) throws IOException, ImageWriteException, ImageReadException {
        try (FileOutputStream out = new FileOutputStream(outPath.toFile())) {
            int w = (int) image.getWidth();
            int h = (int) image.getHeight();

            BufferedImage bufferedImage = SwingFXUtils.fromFXImage(image, null);

            if (mImageFormat == ImageFormat.JPG || mImageFormat == ImageFormat.BMP) {
                BufferedImage rgb = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
                rgb.getGraphics().drawImage(bufferedImage, 0, 0, null);
                bufferedImage = rgb;
            }

            ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();

            if (!ImageIO.write(bufferedImage, imageFormat.name(), outByteStream))
                throw new IOException("No writer found for the specified image format: " + imageFormat.name());

            byte[] outBytes = outByteStream.toByteArray();
            outByteStream.reset();

            if (mMetadata != null && imageFormat == ImageFormat.JPG) {
                writeMetadata(outBytes, outByteStream);
                outBytes = outByteStream.toByteArray();
            }

            out.write(outBytes);
            return outBytes;
        }
    }

    private void updateLastModifiedTime() {
        try {
            mLastModifiedTime = Files.getLastModifiedTime(mPhotoPath).toMillis();
        } catch (IOException e) {
            mLastModifiedTime = Calendar.getInstance().getTimeInMillis();
        }
    }

    boolean isUpToDate() {
        long lastModifiedTime = -1;

        try {
            lastModifiedTime = Files.getLastModifiedTime(mPhotoPath).toMillis();
        } catch (IOException e) {
            // Empty
        }

        return lastModifiedTime == mLastModifiedTime;
    }

    // There is no duplicate photo instance: Object.equals() is enough
    /*@Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (!(o instanceof Photo))
            return false;

        Photo photo = (Photo) o;

        return mName.equals(photo.mName) && mAlbumPath.getValue().equals(photo.mAlbumPath.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(mName, mAlbumPath.getValue());
    }*/

    /**
     * Indicates whether or not the specified image file extension corresponds to
     * an image format supported by the {@link Photo} class.
     *
     * @param imageExt the extension of the image file
     * @return true iff the specified image file extension corresponds to
     *         an image format supported by the {@link Photo} class
     */
    public static boolean isSupportedImageFormat(String imageExt) {
        return SUPPORTED_IMAGE_FORMATS.contains(imageExt.toLowerCase());
    }

    /**
     * Interface that receives notifications of changes to a photo.
     */
    public interface ChangeListener {
        /**
         * Called after the name of a photo file has changed.
         * @param photo the renamed photo
         * @param oldName the old photo name
         * @param newName the new photo name
         */
        void onNameChanged(Photo photo, String oldName, String newName);

        /**
         * Called after the image associated with a photo has changed.
         * @param photo the photo whose the associated image has changed
         */
        void onImageChanged(Photo photo);

        /**
         * Called after a new tag has been added to the tag list of a photo.
         * @param photo the photo
         * @param tag the new tag
         */
        void onTagAdded(Photo photo, String tag);

        /**
         * Called after a tag has been removed from the tag list of a photo.
         * @param photo the photo
         * @param tag the removed tag
         */
        void onTagRemoved(Photo photo, String tag);

        /**
         * Called after a photo file has been removed.
         * @param photo the removed photo
         */
        void onRemoved(Photo photo);
    }

    private static final class PhotoInfo {
        private final ImageInfo mImageInfo;
        private final ImageFormat mImageFormat;
        private final TiffImageMetadata mMetadata;
        private final long mCreationTime;
        private final long mLastModifiedTime;
        private final long mDiskSize;

        PhotoInfo(ImageInfo imageInfo, ImageFormat imageFormat, TiffImageMetadata metadata,
                  long creationTime, long lastModifiedTime, long diskSize)
        {
            mImageInfo = imageInfo;
            mImageFormat = imageFormat;
            mMetadata = metadata;
            mCreationTime = creationTime;
            mLastModifiedTime = lastModifiedTime;
            mDiskSize = diskSize;
        }

        static PhotoInfo of(Path path) throws IOException, ImageReadException {
            File file = path.toFile();

            ImageInfo imageInfo = Imaging.getImageInfo(file);
            ImageFormat format = ImageFormat.from(imageInfo.getFormat());
            TiffImageMetadata metadata;

            if (format == ImageFormat.JPG) {
                ImageMetadata m = Imaging.getMetadata(file);

                if (m != null)
                    metadata = ((JpegImageMetadata) Imaging.getMetadata(file)).getExif();
                else
                    metadata = null;
            } else {
                metadata = null;
            }

            BasicFileAttributes fileAttr = Files.readAttributes(path, BasicFileAttributes.class);
            long creationTime = fileAttr.creationTime().toMillis();
            long lastModifiedTime = fileAttr.lastModifiedTime().toMillis();
            long diskSize = fileAttr.size();

            return new PhotoInfo(imageInfo, format, metadata, creationTime, lastModifiedTime, diskSize);
        }
    }
}
