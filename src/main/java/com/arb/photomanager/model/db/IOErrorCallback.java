package com.arb.photomanager.model.db;

/**
 * Callback method invoked in case of failure during an IO operation.
 *
 * @author Thomas Altenbach
 */
@FunctionalInterface
public interface IOErrorCallback {
    /**
     * Called after an IO operation encounters an exception.
     * @param t The {@link Throwable} encountered exception
     */
    void onError(Throwable t);
}
