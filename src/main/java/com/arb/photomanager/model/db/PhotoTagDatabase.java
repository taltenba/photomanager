package com.arb.photomanager.model.db;

import com.google.common.collect.MultimapBuilder;
import com.google.common.collect.Multimaps;
import com.google.common.collect.SetMultimap;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.*;

/**
 * @author Thomas Altenbach
 */
class PhotoTagDatabase {
    private static final String DB_FILE_NAME = ".tags.db";
    private static final String DB_BACKUP_FILE_NAME = DB_FILE_NAME + ".bak";

    private final AlbumFile mAlbumFile;
    private final SetMultimap<String, String> mTags;

    PhotoTagDatabase(AlbumFile albumFile) throws IOException {
        mAlbumFile = Objects.requireNonNull(albumFile);
        mTags = Multimaps.synchronizedSetMultimap(loadDatabase());
    }

    private SetMultimap<String, String> loadDatabase() throws IOException {
        Path dbPath = mAlbumFile.getPath().resolve(DB_FILE_NAME);

        if (!Files.exists(dbPath))
            return MultimapBuilder.hashKeys().hashSetValues().build();

        Gson gson = new Gson();
        TypeToken typeToken = new TypeToken<Map<String, List<String>>>() {};
        Map<String, List<String>> tagMap;

        try (FileReader fileReader = new FileReader(dbPath.toFile())) {
            tagMap = gson.fromJson(fileReader, typeToken.getType());
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            return MultimapBuilder.hashKeys().hashSetValues().build();
        }

        SetMultimap<String, String> tagMultimap = MultimapBuilder.hashKeys(tagMap.size())
                                                .hashSetValues().build();

        for (Map.Entry<String, List<String>> e : tagMap.entrySet())
            tagMultimap.putAll(e.getKey(), e.getValue());

        return tagMultimap;
    }

    Set<String> getTags(String photoName) {
        return Collections.unmodifiableSet(mTags.get(photoName));
    }

    boolean addTag(String photoName, String tag) {
        return mTags.put(photoName, tag);
    }

    boolean removeTag(String photoName, String tag) {
        return mTags.remove(photoName, tag);
    }

    void removeAllTags(String photoName) {
        mTags.removeAll(photoName);
    }

    void renamePhoto(String oldName, String newName) {
        synchronized (mTags) {
            Set<String> tags = mTags.removeAll(oldName);
            mTags.putAll(newName, tags);
        }
    }

    boolean contains(String photoName, String tag) {
        return mTags.containsEntry(photoName, tag);
    }

    void commit() throws IOException {
        Path dbPath = mAlbumFile.getPath().resolve(DB_FILE_NAME);
        Path backupPath = mAlbumFile.getPath().resolve(DB_BACKUP_FILE_NAME);

        if (Files.exists(dbPath)) {
            Files.copy(dbPath, backupPath, StandardCopyOption.REPLACE_EXISTING);
            Files.setAttribute(dbPath, "dos:hidden", false); // Need to un-hide the file before writing on Windows
        }

        Gson gson = new Gson();

        try (FileWriter writer = new FileWriter(dbPath.toFile())) {
            try {
                gson.toJson(mTags.asMap(), writer);
            } catch(Exception e) {
                if (Files.exists(backupPath)) {
                    Files.copy(backupPath, dbPath, StandardCopyOption.REPLACE_EXISTING);
                    Files.delete(backupPath);
                }

                throw e;
            }
        }

        Files.setAttribute(dbPath, "dos:hidden", true);

        Files.deleteIfExists(backupPath);
    }
}
