package com.arb.photomanager.model.db;

import com.google.common.collect.ImmutableSet;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.function.Predicate;

/**
 * A query that can be used to retrieve the whole photo list of an album in a
 * specified order, or to get a filtered view of that list.
 *
 * @author Thomas Altenbach
 */
public class PhotoQuery {
    /**
     * Enumeration of the sort order in which the query resulting list can be sorted.
     */
    public enum SortDirection {
        ASCENDING,
        DESCENDING
    }

    /**
     * Enumeration of the field on which the query resulting list can be sorted.
     */
    public enum SortableField {
        NAME(Comparator.comparing(Photo::getName)),
        DATE(Comparator.comparing(Photo::getCreationTime));

        private final Comparator<Photo> mComparator;

        SortableField(Comparator<Photo> comparator) {
            mComparator = comparator;
        }

        Comparator<Photo> getComparator() {
            return mComparator;
        }
    }

    private final SortableField mSortField;
    private final SortDirection mSortDirection;
    private final Filter mFilter;

    PhotoQuery(SortableField sortField, SortDirection sortDirection, @Nullable Filter filter) {
        mSortField = Objects.requireNonNull(sortField, "Sort field cannot be null.");
        mSortDirection = Objects.requireNonNull(sortDirection, "Sort direction cannot be null.");
        mFilter = filter;
    }

    SortableField getSortField() {
        return mSortField;
    }

    SortDirection getSortDirection() {
        return mSortDirection;
    }

    @Nullable
    Filter getFilter() {
        return mFilter;
    }

    Predicate<Photo> getFilterPredicate() {
        return mFilter == null ? Filter.ALL_PREDICATE : mFilter.asPredicate();
    }

    /**
     * Creates a new {@link PhotoQuery} that can be used to retrieve the whole photo list of an
     * album in a specified order.
     *
     * @param sortField the field on which the query resulting list must be sorted
     * @param sortDirection the sort order in which the query resulting must be sorted
     * @return the newly created photo query
     */
    public static PhotoQuery newSortQuery(SortableField sortField, SortDirection sortDirection) {
        return new PhotoQuery(sortField, sortDirection, null);
    }

    /**
     * Creates a new {@link PhotoQuery} that can be used to retrieve a filtered of the photo list
     * of an album.
     *
     * @param filter the filter that must be used to filter the photo list content
     * @return the newly created photo query
     */
    public static PhotoQuery newFilterQuery(Filter filter) {
        SortableField sortField = filter.isDateFilter() ?
                SortableField.DATE :
                SortableField.NAME;

        return new PhotoQuery(sortField, SortDirection.ASCENDING, filter);
    }

    /**
     * An album photo list filter.
     */
    public static final class Filter {
        static final Predicate<Photo> ALL_PREDICATE = x -> true;

        private final String mNamePrefix;
        private final long mStartDate;
        private final long mEndDate;
        private final Set<String> mTags;

        private Filter(String namePrefix, long startDate, long endDate, Set<String> tags) {
            mNamePrefix = namePrefix;
            mStartDate = startDate;
            mEndDate = endDate;
            mTags = tags;
        }

        /**
         * Returns the name prefix that filtered photos must start with, if set.
         * @return the name prefix that filtered photo must start with, if set,
         *         {@code null} otherwise
         */
        @Nullable
        public String getNamePrefix() {
            return mNamePrefix;
        }

        /**
         * Returns the inclusive creation date from which the filtered photos must have been created.
         * @return the inclusive creation date from which the filtered photos must have been created
         */
        public long getStartDate() {
            return mStartDate;
        }

        /**
         * Returns the inclusive creation date before which the filtered photos must have been created.
         * @return the inclusive creation date before which the filtered photos must have been created
         */
        public long getEndDate() {
            return mEndDate;
        }

        /**
         * Returns an unmodifiable view of the tag set for which the filtered photos must have a tag
         * set being a subset of the former.
         *
         * @return an unmodifiable view of the tag set for which the filtered photos must have a tag
         *         set being a subset of the former
         */
        public Set<String> getTags() {
            return Collections.unmodifiableSet(mTags);
        }

        /**
         * Indicates whether or not this filter filters the photo according to the photo name.
         * @return true iff this filter filters the photo according to the photo name
         */
        public boolean isNameFilter() {
            return mNamePrefix != null;
        }

        /**
         * Indicates whether or not this filter filters the photo according to the photo creation date.
         * @return true iff this filter filters the photo according to the photo creation date
         */
        public boolean isDateFilter() {
            return mStartDate != 0 || mEndDate != Long.MAX_VALUE;
        }

        /**
         * Indicates whether or not this filter filters the photo according to photo tags.
         * @return true iff this filter filters the photo according to the photo tags
         */
        public boolean isTagFilter() {
            return !mTags.isEmpty();
        }

        Predicate<Photo> asPredicate() {
            List<Predicate<Photo>> predicates = new ArrayList<>();

            if (isNameFilter())
                predicates.add(photo -> photo.getName().startsWith(mNamePrefix));

            if (isDateFilter()) {
                predicates.add(photo -> {
                    long creationTime = photo.getCreationTime();
                    return creationTime >= mStartDate && creationTime < mEndDate;
                });
            }

            if (isTagFilter())
                predicates.add(photo -> photo.getTags().containsAll(mTags));

            return predicates.stream().reduce(ALL_PREDICATE, Predicate::and);
        }

        /**
         * Builder that must be used to build a {@link Filter} instance.
         */
        public static final class Builder {
            private final Set<String> mTags;
            private String mNamePrefix;
            private long mStartDate;
            private long mEndDate;

            /**
             * Creates a new builder, representing a filter allowing all photos.
             */
            public Builder() {
                mTags = new HashSet<>();
                mEndDate = Long.MAX_VALUE;
            }

            /**
             * Creates a new builder from the specified filter.
             * @param filter the filter that the newly created builder must initially represent
             */
            public Builder(Filter filter) {
                mNamePrefix = filter.getNamePrefix();
                mStartDate = filter.getStartDate();
                mEndDate = filter.getEndDate();
                mTags = new HashSet<>(filter.getTags());
            }

            /**
             * Sets the name prefix constraint of this filter builder.
             * @param prefix the name prefix of the filter, set to {@code null} in
             *               order not to filter photo according to the photo name
             * @return the builder
             */
            public Builder filterName(@Nullable String prefix) {
                mNamePrefix = prefix;
                return this;
            }

            /**
             * Sets the creation dates between which filtered photos must have been created.
             * @param startDate the inclusive start date
             * @param endDate the inclusive end date
             * @return the builder
             */
            public Builder filterDates(Date startDate, Date endDate) {
                mStartDate = startDate.getTime();
                mEndDate = endDate.getTime();
                return this;
            }

            /**
             * Adds a tag in the filter builder tag set for which the filtered must have a tag
             * set being a subset of the former.
             * @param tag the tag
             * @return the builder
             */
            public Builder filterTag(String tag) {
                mTags.add(tag);
                return this;
            }

            /**
             * Adds several tags in the filter builder tag set for which the filtered must have a tag
             * set being a subset of the former.
             * @param tags the tags
             * @return the builder
             */
            public Builder filterTags(Collection<String> tags) {
                mTags.addAll(tags);
                return this;
            }

            /**
             * Removes the name prefix constraint from the filter builder
             * @return the builder
             */
            public Builder removeNameFilter() {
                mNamePrefix = null;
                return this;
            }

            /**
             * Removes the creation date constraint from the filter builder
             * @return the builder
             */
            public Builder removeDateFilter() {
                mStartDate = 0;
                mEndDate = Long.MAX_VALUE;
                return this;
            }

            /**
             * Removes the specified from the tag set of this filter builder
             * @param tag the tag
             * @return the builder
             */
            public Builder removeTag(String tag) {
                mTags.remove(tag);
                return this;
            }

            /**
             * Removes all the tags from the tag set of this filter builder
             * @return the builder
             */
            public Builder removeTags() {
                mTags.clear();
                return this;
            }

            /**
             * Getter for tags
             * @return tags
             */
            public Set<String> getTags() {
                return ImmutableSet.copyOf(mTags);
            }

            /**
             * Getter for name prefix
             * @return name prefix
             */
            public String getNamePrefix() {
                return mNamePrefix;
            }

            /**
             * Getter for starting date
             * @return starting date
             */
            public long getStartDate() {
                return mStartDate;
            }

            /**
             * Getter for ending date
             * @return ending date
             */
            public long getEndDate() {
                return mEndDate;
            }

            /**
             * Builds the filter represented by this builder
             * @return the filter represented by this builder
             */
            public Filter build() {
                Set<String> tags = ImmutableSet.copyOf(mTags);
                return new Filter(mNamePrefix, mStartDate, mEndDate, tags);
            }
        }
    }
}
