package com.arb.photomanager.model.db;

import com.arb.photomanager.util.WeakValueHashMap;

import java.nio.file.Path;
import java.util.Map;
import java.util.Objects;

/**
 * @author Thomas Altenbach
 */
final class AlbumFileRegister {
    private final DatabaseExecutor mExecutor;
    private final Map<Path, AlbumFile> mAlbumFiles;

    AlbumFileRegister(DatabaseExecutor executor) {
        mExecutor = Objects.requireNonNull(executor);
        mAlbumFiles = new WeakValueHashMap<>();
    }

    AlbumFile get(Path path) {
        AlbumFile albumFile = mAlbumFiles.get(path);

        if (albumFile != null)
            return albumFile;

        albumFile = new RegisteredAlbumFile(mExecutor, path);
        mAlbumFiles.put(path, albumFile);

        return albumFile;
    }

    private class RegisteredAlbumFile extends AlbumFile {
        RegisteredAlbumFile(DatabaseExecutor executor, Path path) {
            super(executor, path);
        }

        @Override
        void onMoved(Path oldPath, Path newPath) {
            mAlbumFiles.remove(oldPath);
            mAlbumFiles.put(newPath, this);
        }
    }
}
