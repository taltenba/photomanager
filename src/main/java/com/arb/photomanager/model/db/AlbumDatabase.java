package com.arb.photomanager.model.db;

import com.arb.photomanager.model.setting.Settings;
import com.arb.photomanager.util.DirectoryAlterationMonitor;
import com.arb.photomanager.util.ImageThumbnailer;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.io.FilenameUtils;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.Executor;
import java.util.function.BiConsumer;

/**
 * A database of photo albums, enabling to manage disk local albums.
 *
 * <p> Public methods of this class are only intended to be called in a single thread,
 * referred as "the callback thread".
 *
 * @author Thomas Altenbach
 */
public final class AlbumDatabase {
    private static final String DB_THREAD_NAME = "ALBUM_DB_IO_THREAD";
    private static final long MAX_CACHED_PIXEL_COUNT = 4196 * 128 * 128;

    private final Context mContext;
    private final Map<AlbumFile, Album> mAlbums;

    private final AlbumFile.ChangeListener mAlbumChangeListener = new AlbumFile.ChangeListener() {
        @Override
        public void onAlbumMoved(AlbumFile albumFile, Path oldPath, Path newPath) {
            // Empty
        }

        @Override
        public void onAlbumRemoved(AlbumFile albumFile) {
            mAlbums.remove(albumFile);
            albumFile.removeBackgroundListener(this);
        }
    };

    /**
     * Creates a new database with the specified callback executor.
     *
     * <p> The newly created database instance must imperatively be closed using {@link AlbumDatabase#close()}
     * before the program exits.
     *
     * @param callbackExecutor The executor to use for executing callbacks.
     *                         This executor must use a single worker thread, the latter
     *                         being the callback thread as defined in {@link AlbumDatabase}
     *                         documentation.
     */
    public AlbumDatabase(Executor callbackExecutor) {
        Objects.requireNonNull(callbackExecutor);

        DatabaseExecutor executor = new DatabaseExecutor(DB_THREAD_NAME, callbackExecutor);

        mContext = new Context(executor);
        mAlbums = new HashMap<>();
    }

    /**
     * Opens asynchronously an album at a specified path.
     *
     * <p> If the given callback is not {@code null}, the latter is invoked with the opened album
     * ({@code null} iff an error occurred) and the load exception ({@code null} iff none) as
     * arguments when the whole content of the album is loaded or an error occurred during the load.
     *
     * <p> In case the album has already been opened, its content is not reloaded.
     *
     * @param path Path of the album (must be a directory)
     * @param callback Optional callback invoked when the album is loaded or an error occurred
     *                 during the load.
     */
    public void openAlbum(Path path, @Nullable BiConsumer<Album, Throwable> callback) {
        CompletableFuture<Album> future = mContext.mExecutor.supplyBackground(() -> {
            if (!mContext.mAlterationMonitor.isRunning())
                mContext.mAlterationMonitor.startMonitoring();

            AlbumFile albumFile = mContext.getAlbumFile(path);
            Album album = mAlbums.get(albumFile);

            if (album != null)
                return album;

            try {
                album = new Album(mContext, path);
            } catch (IOException | ImageReadException e) {
                throw new CompletionException(e);
            }

            albumFile.addWeakBackgroundListener(mAlbumChangeListener);

            mAlbums.put(albumFile, album);
            return album;
        });

        if (callback != null) {
            future.whenCompleteAsync((r, t) -> {
                Throwable cause = t == null ? null : t.getCause();
                callback.accept(r, cause);
            }, mContext.mExecutor.getCallbackExecutor());
        }
    }

    /**
     * Creates asynchronously a new empty album at the specified location.
     * The specified target must not exist.
     *
     * @param path the path to the target file
     * @param errorCallback optional callback invoked in case of failure
     */
    public void createAlbum(Path path, @Nullable IOErrorCallback errorCallback) {
        mContext.mExecutor.runIOBackground(() -> {
            try {
                Files.createDirectory(path);
            } catch (IOException e) {
                throw new CompletionException(e);
            }
        }, errorCallback);
    }

    /**
     * Closes the database and all opened albums.
     * <p> This database instance and closed album instances must no longer be used.
     */
    public void close() {
        mContext.mExecutor.runBackground(() -> {
            mContext.mAlterationMonitor.stopMonitoring();

            for (Album a : mAlbums.values())
                a.close();

            mAlbums.clear();
            mContext.mExecutor.shutdown();
        });
    }

    static class Context {
        private final DatabaseExecutor mExecutor;
        private final ImageThumbnailer mThumbnailer;
        private final DirectoryAlterationMonitor mAlterationMonitor;
        private final AlbumFileRegister mAlbumFileRegister;

        private Context(DatabaseExecutor executor) {
            mExecutor = Objects.requireNonNull(executor);

            mThumbnailer = new ImageThumbnailer(Settings.MAX_THUMBNAIL_WIDTH,
                    Settings.MAX_THUMBNAIL_HEIGHT, MAX_CACHED_PIXEL_COUNT);

            mAlterationMonitor = new DirectoryAlterationMonitor(executor.getBackgroundExecutor(),
                                    path -> {
                                        String ext = FilenameUtils.getExtension(path.toString());
                                        return ext.isEmpty() || Photo.isSupportedImageFormat(ext);
                                    });

            mAlbumFileRegister = new AlbumFileRegister(executor);
        }

        DatabaseExecutor getDatabaseExecutor() {
            return mExecutor;
        }

        ImageThumbnailer getThumbnailer() {
            return mThumbnailer;
        }

        void registerAlterationListener(Path albumPath, DirectoryAlterationMonitor.AlterationListener l) throws IOException {
            mAlterationMonitor.register(albumPath, l);
        }

        void unregisterAlterationListener(Path albumPath) {
            mAlterationMonitor.unregister(albumPath);
        }

        AlbumFile getAlbumFile(Path albumPath) {
            return mAlbumFileRegister.get(albumPath);
        }
    }
}
