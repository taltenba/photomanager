package com.arb.photomanager.model.db;

import java.util.Collection;

/**
 * @author Thomas Altenbach
 */
interface LiveQueryablePhotoSource {
    void addWeakListener(ChangeListener listener);

    void removeListener(ChangeListener listener);

    Collection<Photo> query(PhotoQuery query);

    interface ChangeListener {
        void onPhotoAdded(LiveQueryablePhotoSource source, Photo photo);
        void onPhotoRemoved(LiveQueryablePhotoSource source, Photo photo);
        void onContentReplaced(LiveQueryablePhotoSource source);
    }
}
