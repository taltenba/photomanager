package com.arb.photomanager.model.db;

import com.google.common.util.concurrent.ListenableFuture;
import javafx.beans.InvalidationListener;
import javafx.collections.ListChangeListener;
import javafx.collections.ModifiableObservableListBase;
import javafx.collections.ObservableList;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Semaphore;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

// All functions, except constructor must be used in album database thread
/**
 * Result of the execution of a query on the content of an album.
 *
 * <p> A newly created {@code LiveQueryResult} is initially empty and is asynchronously populated
 * with the result of a {@link PhotoQuery}.
 *
 * <p> A callback invoked when the execution of the query associated with a {@code LiveQueryResult}
 * completes can be set with {@link LiveQueryResult#setQueryCompletedCallback(QueryCompletedCallback)}.
 *
 * <p> The content of the result is dynamically updated according to the changes made to the content
 * of the album from which a {@code LiveQueryResult} instance originates.
 *
 * <p> The {@link LiveQueryResult#stopSyncing()} method can be called to put an end to those dynamic
 * updates. This function can also be called before the query complete to cancel the execution of the
 * latter.
 *
 * <p> Ordering of the content of a {@code LiveQueryResult} can be changed using
 * {@link LiveQueryResult#changeOrdering(PhotoQuery.SortableField, PhotoQuery.SortDirection)}.
 *
 * <p> This class implements {@link ObservableList} and changes of its content can therefore
 * be observed through a {@link ListChangeListener} using {@link LiveQueryResult#addListener(ListChangeListener)}.
 *
 * <p> Note that only the read-only methods of the {@link ObservableList} interface can be used,
 * attempts to modify a {@code LiveQueryResult}, whether direct or via its iterator, result in
 * an {@code UnsupportedOperationException}.
 *
 * <p> Public methods of this class are only intended to be called in a single thread, being the callback
 * thread of the {@link AlbumDatabase} instance that has been used to open the album from which an instance
 * of this class originates.
 *
 * @author Thomas Altenbach
 */
public class LiveQueryResult implements ObservableList<Photo>, RandomAccess {
    private enum State {
        QUERYING,
        LIVING,
        DEAD
    }

    private final MutableList mList;
    private final List<Photo> mUnmodifiableView;
    private final List<ListChangeListener<? super Photo>> mChangeListeners;
    private final List<InvalidationListener> mInvalidationListeners;

    // Must be called in album database callback thread
    LiveQueryResult(DatabaseExecutor executor, LiveQueryablePhotoSource source, PhotoQuery query) {
        mList = new MutableList(executor, source, query);
        mUnmodifiableView = Collections.unmodifiableList(mList);
        mChangeListeners = new CopyOnWriteArrayList<>();
        mInvalidationListeners = new CopyOnWriteArrayList<>();

        mList.addListener((ListChangeListener<Photo>) c -> {
            final UnmodifiableListChangeWrapper changeWrapper =
                    new UnmodifiableListChangeWrapper(c, this);

            for (ListChangeListener<? super Photo> l : mChangeListeners)
                l.onChanged(changeWrapper);
        });

        mList.addListener((InvalidationListener) observable -> {
            for (InvalidationListener l : mInvalidationListeners)
                l.invalidated(this);
        });
    }

    /**
     * Changes the ordering of this {@code LiveQueryResult}.
     * @param sortField the new field on which the content must be sorted
     * @param sortDirection the new sort order in which the content must be sorted
     */
    public void changeOrdering(PhotoQuery.SortableField sortField, PhotoQuery.SortDirection sortDirection) {
        if (sortField == null || sortDirection == null)
            throw new NullPointerException("Neither sortField nor sortDirection can be null.");

        mList.changeOrdering(sortField, sortDirection);
    }

    /**
     * Stops syncing the content of this {@code LiveQueryResult} with the album from which it originates.
     *
     * <p> If the query associated with this {@code LiveQueryResult} has not yet completed, the execution
     * of that query is interrupted.
     *
     * <p> Calling this function effectively make the content of this {@code LiveQueryResult} immutable.
     */
    public void stopSyncing() {
        mList.stopSyncing();
        mChangeListeners.clear();
        mInvalidationListeners.clear();
    }

    /**
     * Sets the callback called when the execution of the query associated with this {@code LiveQueryResult}
     * completes.
     *
     * <p> In case the query is already completed, the callback is called immediately.
     *
     * @param callback the callback
     */
    public void setQueryCompletedCallback(@Nullable QueryCompletedCallback callback) {
        mList.setQueryCompletedCallback(callback);
    }

    @Override
    public void addListener(ListChangeListener<? super Photo> listener) {
        mChangeListeners.add(listener);
    }

    @Override
    public void removeListener(ListChangeListener<? super Photo> listener) {
        mChangeListeners.remove(listener);
    }

    @Override
    public boolean addAll(Photo... elements) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean setAll(Photo... elements) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean setAll(Collection<? extends Photo> col) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Photo... elements) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(Photo... elements) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void remove(int from, int to) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int size() {
        return mList.size();
    }

    @Override
    public boolean isEmpty() {
        return mList.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) != -1;
    }

    @NotNull
    @Override
    public Iterator<Photo> iterator() {
        return mUnmodifiableView.iterator();
    }

    @NotNull
    @Override
    public Object[] toArray() {
        return mList.toArray();
    }

    @NotNull
    @Override
    public <T> T[] toArray(@NotNull T[] a) {
        //noinspection SuspiciousToArrayCall
        return mList.toArray(a);
    }

    @Override
    public boolean add(Photo photo) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean containsAll(@NotNull Collection<?> c) {
        return mList.containsAll(c);
    }

    @Override
    public boolean addAll(@NotNull Collection<? extends Photo> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(int index, @NotNull Collection<? extends Photo> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(@NotNull Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(@NotNull Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Photo get(int index) {
        return mList.get(index);
    }

    @Override
    public Photo set(int index, Photo element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add(int index, Photo element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Photo remove(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int indexOf(Object o) {
        return mList.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return mList.lastIndexOf(o);
    }

    @NotNull
    @Override
    public ListIterator<Photo> listIterator() {
        return mUnmodifiableView.listIterator();
    }

    @NotNull
    @Override
    public ListIterator<Photo> listIterator(int index) {
        return mUnmodifiableView.listIterator();
    }

    @NotNull
    @Override
    public List<Photo> subList(int fromIndex, int toIndex) {
        return mUnmodifiableView.subList(fromIndex, toIndex);
    }

    @Override
    public void addListener(InvalidationListener listener) {
        mInvalidationListeners.add(listener);
    }

    @Override
    public void removeListener(InvalidationListener listener) {
        mInvalidationListeners.remove(listener);
    }

    /**
     * Callback method invoked when the execution of the query associated with a {@code LiveQueryResult}
     * completes.
     */
    public interface QueryCompletedCallback {
        /**
         * Called after the execution of the query associated with a {@code LiveQueryResult} completes.
         */
        void onQueryCompleted();
    }

    private class MutableList extends ModifiableObservableListBase<Photo> implements RandomAccess {
        private final DatabaseExecutor mExecutor;
        private final LiveQueryablePhotoSource mSource;
        private final Semaphore mSemaphore;
        private final List<Photo> mBackingList;
        private final PhotoQuery.Filter mFilter;
        private final Predicate<Photo> mFilterPredicate;

        private State mState;
        private PhotoQuery.SortableField mSortField;
        private PhotoQuery.SortDirection mSortDirection;
        private QueryCompletedCallback mQueryCompletedCallback;
        private ListenableFuture<Collection<Photo>> mQueryResultFuture;

        private final Photo.ChangeListener mPhotoChangeListener = new Photo.ChangeListener() {
            @Override
            public void onNameChanged(Photo photo, String oldName, String newName) {
                if (filterChangedPhoto(photo))
                    onPhotoNameChanged(photo);
            }

            @Override
            public void onImageChanged(Photo photo) {
                // Empty
            }

            @Override
            public void onTagAdded(Photo photo, String tag) {
                filterChangedPhoto(photo);
            }

            @Override
            public void onTagRemoved(Photo photo, String tag) {
                filterChangedPhoto(photo);
            }

            @Override
            public void onRemoved(Photo photo) {
                // Empty: will be handled by photo source listener
            }
        };

        private final LiveQueryablePhotoSource.ChangeListener mPhotoSourceListener = new LiveQueryablePhotoSource.ChangeListener() {
            @Override
            public void onPhotoAdded(LiveQueryablePhotoSource set, Photo photo) {
                addSorted(photo);
            }

            @Override
            public void onPhotoRemoved(LiveQueryablePhotoSource set, Photo photo) {
                remove(photo);
            }

            @Override
            public void onContentReplaced(LiveQueryablePhotoSource source) {
                reloadContent();
            }
        };

        // Must be called in database callback thread
        private MutableList(DatabaseExecutor executor, LiveQueryablePhotoSource source, PhotoQuery query) {
            mExecutor = Objects.requireNonNull(executor, "Database executor cannot be null.");
            mSource = Objects.requireNonNull(source, "Source cannot be null.");
            mSemaphore = new Semaphore(0);
            mBackingList = new ArrayList<>();
            mFilter = query.getFilter();
            mFilterPredicate = query.getFilterPredicate();

            mSortField = query.getSortField();
            mSortDirection = query.getSortDirection();
            mState = State.QUERYING;

            mQueryResultFuture = mExecutor.submitBackground(() -> {
                Collection<Photo> photos = source.query(query);

                for (Photo photo : photos)
                    photo.addBackgroundChangeListener(mPhotoChangeListener);

                mSource.addWeakListener(mPhotoSourceListener);
                return photos;
            });

            mQueryResultFuture.addListener(() -> {
                try {
                    setAll(mQueryResultFuture.get());
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }

                mQueryResultFuture = null;
                mState = State.LIVING;

                mSemaphore.release();

                if (mQueryCompletedCallback != null) {
                    mQueryCompletedCallback.onQueryCompleted();
                    mQueryCompletedCallback = null;
                }
            }, mExecutor.getCallbackExecutor());
        }

        private Comparator<Photo> getComparator(PhotoQuery.SortableField sortField, PhotoQuery.SortDirection sortDirection) {
            Comparator<Photo> comparator = sortField.getComparator();

            if (sortDirection == PhotoQuery.SortDirection.DESCENDING)
                comparator = comparator.reversed();

            return comparator;
        }

        private Comparator<Photo> getComparator() {
            return getComparator(mSortField, mSortDirection);
        }

        private int findIndex(Photo photo) {
            return Collections.binarySearch(mBackingList, photo, getComparator());
        }

        private int findInsertionIndex(Photo photo) {
            int index = findIndex(photo);
            return index >= 0 ? index : ~index;
        }

        // Must be called in album database thread
        private boolean filterChangedPhoto(Photo photo) {
            if (!mFilterPredicate.test(photo)) {
                remove(photo);
                return false;
            }

            return true;
        }

        // Must be called in album database thread
        private void onPhotoNameChanged(Photo photo) {
            try {
                mSemaphore.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }

            if (mSortField != PhotoQuery.SortableField.NAME) {
                mSemaphore.release();
                return;
            }

            /*// implements binary search with string for less hacky (and more efficient) way
            int currentIndex = Collections.binarySearch(mBackingList, oldName, (o1, o2) -> {
                String leftName, rightName;

                if (o1 instanceof String) {
                    leftName = (String) o1;
                    rightName = ((Photo) o2).getName();
                } else {
                    leftName = ((Photo) o1).getName();
                    rightName = (String) o2;
                }

                return leftName.compareTo(rightName);
            }); */

            // TODO: find a new to find more efficiently the renamed photo
            int currentIndex = mBackingList.indexOf(photo);

            if (currentIndex < 0) {
                mSemaphore.release();
                return;
            }

            mBackingList.remove(currentIndex);

            int newIndex = findInsertionIndex(photo);

            CompletableFuture.runAsync(() ->
                                {
                                    if (mState != State.LIVING)
                                        return;

                                    beginChange();
                                    nextRemove(currentIndex, photo);
                                    endChange();

                                    add(newIndex, photo);
                                }, mExecutor.getCallbackExecutor())
                    .whenComplete((r, t) -> mSemaphore.release());
        }

        private void reloadContent() {
            try {
                mSemaphore.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }

            if (mState != State.LIVING) {
                mSemaphore.release();
                return;
            }

            for (Photo photo : mBackingList)
                photo.removeBackgroundChangeListener(mPhotoChangeListener);

            PhotoQuery query = new PhotoQuery(mSortField, mSortDirection, mFilter);
            Collection<Photo> photos = mSource.query(query);

            for (Photo photo : photos)
                photo.addBackgroundChangeListener(mPhotoChangeListener);

            CompletableFuture.runAsync(() ->
                                {
                                    if (mState == State.LIVING)
                                        setAll(photos);
                                }, mExecutor.getCallbackExecutor())
                            .whenComplete((r, t) -> mSemaphore.release());
        }

        // Must be called in album database thread
        private void addSorted(Photo element) {
            if (!mFilterPredicate.test(element))
                return;

            try {
                mSemaphore.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }

            if (mState != State.LIVING) {
                mSemaphore.release();
                return;
            }

            int index = findInsertionIndex(element);

            element.addBackgroundChangeListener(mPhotoChangeListener);

            CompletableFuture.runAsync(() ->
                                {
                                    if (mState == State.LIVING)
                                        add(index, element);
                                }, mExecutor.getCallbackExecutor())
                    .whenComplete((r, t) -> mSemaphore.release());
        }

        // Must be called in album database thread and only from the photo set listener
        @Override
        public boolean remove(Object o) {
            try {
                mSemaphore.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
                return false;
            }

            if (mState != State.LIVING) {
                mSemaphore.release();
                return false;
            }

            int index = indexOf(o);

            if (index < 0) {
                mSemaphore.release();
                return false;
            }

            ((Photo) o).removeBackgroundChangeListener(mPhotoChangeListener);

            CompletableFuture.runAsync(() ->
                                {
                                    if (mState == State.LIVING)
                                        remove(index);
                                }, mExecutor.getCallbackExecutor())
                    .whenComplete((r, t) -> mSemaphore.release());

            return true;
        }

        // This function must only be called from the album callback thread
        void changeOrdering(PhotoQuery.SortableField sortField, PhotoQuery.SortDirection sortDirection) {
            if (sortDirection == mSortDirection && sortField == mSortField)
                return;

            mExecutor.runBackground(() -> {
                try {
                    mSemaphore.acquire();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }

                Collection<Photo> newPhotos;

                if (mFilterPredicate == PhotoQuery.Filter.ALL_PREDICATE) {
                    PhotoQuery query = PhotoQuery.newSortQuery(sortField, sortDirection);
                    newPhotos = mSource.query(query);
                } else {
                    if (sortField == mSortField) {
                        newPhotos = mBackingList.stream()
                                .collect(Collector.of(
                                        ArrayDeque::new,
                                        ArrayDeque::addFirst,
                                        (d1, d2) -> { d2.addAll(d1); return d2; }));
                    } else {
                        newPhotos = mBackingList.stream()
                                .sorted(getComparator(sortField, sortDirection))
                                .collect(Collectors.toList());
                    }
                }

                CompletableFuture.runAsync(() ->
                                    {
                                        mSortDirection = sortDirection;
                                        mSortField = sortField;
                                        setAll(newPhotos);
                                    }, mExecutor.getCallbackExecutor())
                        .whenComplete((r, t) -> mSemaphore.release());
            });
        }

        // This function must be called in album callback thread
        void stopSyncing() {
            mState = State.DEAD;

            if (mQueryResultFuture != null) {
                mQueryResultFuture.cancel(true);
                mQueryResultFuture = null;

                mSemaphore.release();
            }

            mExecutor.runBackground(() -> {
                try {
                    mSemaphore.acquire();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }

                for (Photo photo : mBackingList)
                    photo.removeBackgroundChangeListener(mPhotoChangeListener);

                mSource.removeListener(mPhotoSourceListener);
                mSemaphore.release();
            });
        }

        // This function must be called in album callback thread
        void setQueryCompletedCallback(@Nullable QueryCompletedCallback callback) {
            if (callback != null && mState != State.QUERYING)
                callback.onQueryCompleted();
            else
                mQueryCompletedCallback = callback;
        }

        @Override
        public Photo get(int index) {
            return mBackingList.get(index);
        }

        @Override
        public int size() {
            return mBackingList.size();
        }

        @Override
        protected void doAdd(int index, Photo element) {
            mBackingList.add(index, element);
        }

        @Override
        protected Photo doSet(int index, Photo element) {
            return mBackingList.set(index, element);
        }

        @Override
        protected Photo doRemove(int index) {
            return mBackingList.remove(index);
        }

        @Override
        public int indexOf(Object o) {
            Photo photo = (Photo) o;

            if (!mFilterPredicate.test(photo))
                return -1;

            if (mBackingList.isEmpty())
                return -1;

            int index = findIndex(photo);

            if (index < 0)
                return -1;

            if (mBackingList.get(index).equals(photo))
                return index;

            Comparator<Photo> sortComparator = mSortField.getComparator();

            for (int i = index - 1; i >= 0; --i) {
                Photo cur = mBackingList.get(i);

                if (sortComparator.compare(cur, photo) != 0)
                    break;

                if (cur.equals(photo))
                    return i;
            }

            for (int i = index + 1, end = mBackingList.size(); i < end; ++i) {
                Photo cur = mBackingList.get(i);

                if (sortComparator.compare(cur, photo) != 0)
                    break;

                if (cur.equals(photo))
                    return i;
            }

            return -1;
        }

        @Override
        public int lastIndexOf(Object o) {
            return indexOf(o); // All photos are unique
        }
    }

    private static class UnmodifiableListChangeWrapper extends ListChangeListener.Change<Photo> {
        private final ListChangeListener.Change<? extends Photo> mWrapped;
        private int[] mPerms;

        private UnmodifiableListChangeWrapper(ListChangeListener.Change<? extends Photo> change, ObservableList<Photo> unmodifiableList) {
            super(unmodifiableList);
            mWrapped = change;
        }

        @Override
        public boolean next() {
            mPerms = null;
            return mWrapped.next();
        }

        @Override
        public void reset() {
            mWrapped.reset();
        }

        @Override
        public int getFrom() {
            return mWrapped.getFrom();
        }

        @Override
        public int getTo() {
            return mWrapped.getTo();
        }

        @SuppressWarnings("unchecked")
        @Override
        public List<Photo> getRemoved() {
            return (List<Photo>) mWrapped.getRemoved();
        }

        @Override
        protected int[] getPermutation() {
            if (mPerms == null) {
                if (mWrapped.wasPermutated()) {
                    final int from = mWrapped.getFrom();
                    final int n = mWrapped.getTo() - from;

                    mPerms = new int[n];

                    for (int i = 0; i < n; ++i)
                        mPerms[i] = mWrapped.getPermutation(from + i);
                } else {
                    mPerms = new int[0];
                }
            }

            return mPerms;
        }
    }
}
