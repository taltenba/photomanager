package com.arb.photomanager.model.db;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

/**
 * @author Thomas Altenbach
 */
class DatabaseExecutor {
    private final ListeningExecutorService mBackgroundExecutor;
    private final Executor mCallbackExecutor;

    DatabaseExecutor(String databaseThreadName, Executor callbackExecutor) {
        mBackgroundExecutor = MoreExecutors.listeningDecorator(
                Executors.newSingleThreadExecutor(r -> {
                    Thread t = new Thread(r, databaseThreadName);
                    t.setUncaughtExceptionHandler((t1, e) -> e.printStackTrace());
                    return t;
                }));

        mCallbackExecutor = callbackExecutor;
    }

    Executor getBackgroundExecutor() {
        return mBackgroundExecutor;
    }

    Executor getCallbackExecutor() { return mCallbackExecutor; }

    void runBackground(Runnable runnable) {
        mBackgroundExecutor.execute(runnable);
    }

    CompletableFuture<Void> runIOBackground(Runnable ioRunnable, @Nullable IOErrorCallback errorCallback) {
        CompletableFuture<Void> future = CompletableFuture.runAsync(ioRunnable, mBackgroundExecutor);

        if (errorCallback != null) {
            future.whenCompleteAsync((r, t) -> {
                if (t != null)
                    errorCallback.onError(t.getCause());
            }, mCallbackExecutor);
        }

        return future;
    }

    <V> ListenableFuture<V> submitBackground(Callable<V> task) {
        return mBackgroundExecutor.submit(task);
    }

    <V> CompletableFuture<V> supplyBackground(Supplier<V> supplier) {
        return CompletableFuture.supplyAsync(supplier, mBackgroundExecutor);
    }

    <V> CompletableFuture<V> supplyIOBackground(Supplier<V> supplier, @Nullable IOErrorCallback errorCallback) {
        CompletableFuture<V> future = CompletableFuture.supplyAsync(supplier, mBackgroundExecutor);

        if (errorCallback != null) {
            future.whenCompleteAsync((r, t) -> {
                if (t != null)
                    errorCallback.onError(t.getCause());
            }, mCallbackExecutor);
        }

        return future;
    }

    void shutdown() {
        mBackgroundExecutor.shutdown();
    }
}
