package com.arb.photomanager.model.db;

import com.arb.photomanager.util.JoinedCollectionView;
import com.arb.photomanager.util.WeakListenerCollection;
import com.google.common.collect.Ordering;
import com.google.common.collect.TreeMultimap;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author Thomas Altenbach
 */
class PhotoSet implements LiveQueryablePhotoSource {
    private final NavigableMap<String, Photo> mPhotoByNames;
    private final TreeMultimap<Long, Photo> mPhotoByDates;
    private final TreeMultimap<String, Photo> mPhotoByTags;
    private final Collection<ChangeListener> mChangeListeners;

    private final Photo.ChangeListener mPhotoChangeListener = new Photo.ChangeListener() {
        @Override
        public void onNameChanged(Photo photo, String oldName, String newName) {
            mPhotoByNames.remove(oldName);

            Photo old = mPhotoByNames.put(newName, photo);

            if (old != null)
                finishRemoveAndNotify(photo);
        }

        @Override
        public void onImageChanged(Photo photo) {
            // Empty
        }

        @Override
        public void onTagAdded(Photo photo, String tag) {
            mPhotoByTags.put(tag, photo);
        }

        @Override
        public void onTagRemoved(Photo photo, String tag) {
            mPhotoByTags.remove(tag, photo);
        }

        @Override
        public void onRemoved(Photo photo) {
            doRemoveAndNotify(photo);
        }
    };

    PhotoSet() {
        mPhotoByNames = new TreeMap<>();
        mPhotoByDates = TreeMultimap.create(Ordering.natural(), PhotoQuery.SortableField.NAME.getComparator());
        mPhotoByTags = TreeMultimap.create(Ordering.natural(), PhotoQuery.SortableField.NAME.getComparator());
        mChangeListeners = new WeakListenerCollection<>();
    }

    private void doRemoveAndNotify(Photo photo) {
        mPhotoByNames.remove(photo.getName());
        finishRemoveAndNotify(photo);
    }

    private void finishRemoveAndNotify(Photo photo) {
        mPhotoByDates.remove(photo.getCreationTime(), photo);

        for (String tag : photo.getTags())
            mPhotoByTags.remove(tag, photo);

        photo.removeBackgroundChangeListener(mPhotoChangeListener);

        for (ChangeListener l : mChangeListeners)
            l.onPhotoRemoved(this, photo);
    }

    Photo add(Photo photo) {
        Photo old = mPhotoByNames.put(photo.getName(), photo);

        if (photo.equals(old))
            return old;

        if (old != null)
            finishRemoveAndNotify(photo);

        mPhotoByDates.put(photo.getCreationTime(), photo);

        for (String tag : photo.getTags())
            mPhotoByTags.put(tag, photo);

        photo.addBackgroundChangeListener(mPhotoChangeListener);

        for (ChangeListener l : mChangeListeners)
            l.onPhotoAdded(this, photo);

        return old;
    }

    boolean remove(Photo photo) {
        Photo contained = mPhotoByNames.get(photo.getName());

        if (photo != contained) // There is only one instance of Photo for each photo
            return false;

        doRemoveAndNotify(contained);
        return true;
    }

    boolean remove(String photoName) {
        Photo photo = mPhotoByNames.remove(photoName);

        if (photo == null)
            return false;

        finishRemoveAndNotify(photo);
        return true;
    }

    @Nullable
    Photo get(String photoName) {
        return mPhotoByNames.get(photoName);
    }

    boolean contains(String photoName) {
        return mPhotoByNames.containsKey(photoName);
    }

    int size() {
        return mPhotoByNames.size();
    }

    void replaceContent(Collection<Photo> photos) {
        Objects.requireNonNull(photos);

        mPhotoByNames.clear();
        mPhotoByDates.clear();
        mPhotoByTags.clear();

        for (Photo photo : photos) {
            Photo old = mPhotoByNames.put(photo.getName(), photo);

            if (old != null)
                photo.removeBackgroundChangeListener(mPhotoChangeListener);

            mPhotoByDates.put(photo.getCreationTime(), photo);

            for (String tag : photo.getTags())
                mPhotoByTags.put(tag, photo);

            photo.addBackgroundChangeListener(mPhotoChangeListener);
        }

        for (ChangeListener l : mChangeListeners)
            l.onContentReplaced(this);
    }

    @Override
    public Collection<Photo> query(PhotoQuery query) {
        if (query.getFilter() == null)
            return getAllSorted(query.getSortField(), query.getSortDirection());
        else
            return getFilteredList(query.getFilter());
    }

    @Override
    public void addWeakListener(ChangeListener listener) {
        mChangeListeners.add(listener);
    }

    @Override
    public void removeListener(ChangeListener listener) {
        mChangeListeners.remove(listener);
    }

    private Collection<Photo> getAllSorted(PhotoQuery.SortableField sortField, PhotoQuery.SortDirection sortDirection) {
        Collection<Photo> photos;

        switch (sortField) {
            case NAME:
                if (sortDirection == PhotoQuery.SortDirection.ASCENDING)
                    photos = mPhotoByNames.values();
                else
                    photos = mPhotoByNames.descendingMap().values();
                break;
            case DATE:
                if (sortDirection == PhotoQuery.SortDirection.ASCENDING)
                    photos = mPhotoByDates.values();
                else
                    photos = new JoinedCollectionView<>(mPhotoByDates.asMap().descendingMap().values());
                break;
            default:
                photos = null;
                break;
        }

        return Collections.unmodifiableCollection(photos);
    }

    private Collection<Photo> getFilteredList(PhotoQuery.Filter filter) {
        PhotoQuery.Filter.Builder filterBuilder = new PhotoQuery.Filter.Builder(filter);
        Collection<Photo> photos;

        if (filter.isNameFilter()) {
            String namePrefix = filter.getNamePrefix();

            //noinspection ConstantConditions
            int lastCharIndex = namePrefix.length() - 1;
            char nextChar = (char) (namePrefix.charAt(lastCharIndex) + 1);
            String lastKey = namePrefix.substring(0, lastCharIndex) + nextChar;

            photos = mPhotoByNames.subMap(namePrefix, lastKey).values();
            filterBuilder.removeNameFilter();
        } else if (filter.isDateFilter()) {
            long start = filter.getStartDate();
            long end = filter.getEndDate();

            /*photoStream = mPhotoByDates.asMap()
                    .subMap(start, true, end, true)
                    .values()
                    .stream()
                    .flatMap(Collection::stream);*/

            photos = new JoinedCollectionView<>(
                            mPhotoByDates.asMap()
                            .subMap(start, true, end, true)
                            .values());

            filterBuilder.removeDateFilter();
        } else if (filter.isTagFilter()) {
            String tag = filter.getTags().iterator().next();
            photos = mPhotoByTags.get(tag);
            filterBuilder.removeTag(tag);
        } else {
            return mPhotoByNames.values();
        }

        Predicate<Photo> filterPredicate = filterBuilder.build().asPredicate();

        if (filterPredicate == PhotoQuery.Filter.ALL_PREDICATE)
            return Collections.unmodifiableCollection(photos);

        return photos.stream()
                .filter(filterPredicate)
                .collect(Collectors.toList());
    }
}
