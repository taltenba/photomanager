package com.arb.photomanager.model.db;

import com.arb.photomanager.util.DirectoryAlterationMonitor;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.io.FilenameUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * An album of photos, enabling to manage photos and sub albums of an album.
 *
 * <p> Public methods of this class are only intended to be called in a single thread,
 * being the callback thread of the {@link AlbumDatabase} instance that has been used
 * to open this album.
 *
 * @author Thomas Altenbach
 */
public class Album {
    private final AlbumDatabase.Context mContext;
    private final DatabaseExecutor mExecutor;
    private final AlbumFile mAlbumFile;
    private final PhotoSet mPhotoSet;
    private final PhotoTagDatabase mPhotoTagDatabase;
    private final Set<AlbumFile> mSubAlbums;

    private final List<ChangeListener> mChangeListeners;

    private final AlbumFile.ChangeListener mSubAlbumChangeListener = new AlbumFile.ChangeListener() {
        @Override
        public void onAlbumMoved(AlbumFile albumFile, Path oldPath, Path newPath) {
            if (newPath.getParent().equals(mAlbumFile.getPath())) {
                mExecutor.getCallbackExecutor().execute(() -> {
                    for (ChangeListener l : mChangeListeners)
                        l.onSubAlbumRenamed(Album.this, albumFile, oldPath, newPath);
                });
            } else {
                removeSubAlbum(albumFile, oldPath);
            }
        }

        @Override
        public void onAlbumRemoved(AlbumFile albumFile) {
            removeSubAlbum(albumFile, albumFile.getPath());
        }
    };

    @SuppressWarnings("FieldCanBeLocal")
    private final DirectoryAlterationMonitor.AlterationListener mAlbumAlterationListener = new DirectoryAlterationMonitor.AlterationListener() {
        @Override
        public void onAdded(Path path) {
            if (Files.isDirectory(path)) {
                AlbumFile subAlbum = mContext.getAlbumFile(path);
                addSubAlbum(subAlbum);
            } else {
                String name = path.getFileName().toString();

                if (mPhotoSet.contains(name))
                    return;

                tryLoadAndAddPhoto(path);
            }
        }

        @Override
        public void onDeleted(Path path) {
            if (FilenameUtils.getExtension(path.toString()).isEmpty()) {
                AlbumFile albumFile = mContext.getAlbumFile(path);

                if (mSubAlbums.contains(albumFile))
                    albumFile.notifyRemoved();
            } else {
                String name = path.getFileName().toString();
                mPhotoSet.remove(name);
            }
        }

        @Override
        public void onModified(Path path) {
            String name = path.getFileName().toString();

            // TODO: use that when the view will listen to image changes event in Photo.ChangeListener
            /*Photo photo = mPhotoSet.get(name);

            if (photo == null || photo.isUpToDate())
                return;

            mPhotoSet.remove(name);
            tryLoadAndAddPhoto(path);*/

            if (!mPhotoSet.remove(name))
                return;

            tryLoadAndAddPhoto(path);
        }

        @Override
        public void onOverflow() {
            try {
                refreshContent();
            } catch (IOException | ImageReadException e) {
                e.printStackTrace();
            }
        }
    };

    private final AlbumFile.ChangeListener mAlbumFileChangeListener = new AlbumFile.ChangeListener() {
        @Override
        public void onAlbumMoved(AlbumFile albumFile, Path oldPath, Path newPath) {
            mContext.unregisterAlterationListener(oldPath);

            try {
                mContext.registerAlterationListener(newPath, mAlbumAlterationListener);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onAlbumRemoved(AlbumFile albumFile) {
            disconnectListeners();
        }
    };

    Album(AlbumDatabase.Context context, Path path) throws IOException, ImageReadException {
        Objects.requireNonNull(path, "Path cannot be null");

        if (!Files.exists(path))
            throw new FileNotFoundException("File " + path + " does not exist");

        if (!Files.isDirectory(path))
            throw new IOException("File " + path + " is not a directory");

        mContext = Objects.requireNonNull(context, "Database context cannot be null");
        mExecutor = mContext.getDatabaseExecutor();
        mAlbumFile = mContext.getAlbumFile(path);
        mSubAlbums = ConcurrentHashMap.newKeySet();
        mPhotoSet = new PhotoSet();
        mChangeListeners = new CopyOnWriteArrayList<>();
        mPhotoTagDatabase = new PhotoTagDatabase(mAlbumFile);

        mContext.registerAlterationListener(path, mAlbumAlterationListener);

        try {
            load();
        } catch (Throwable e) {
            mContext.unregisterAlterationListener(path);
            throw e;
        }

        mAlbumFile.addWeakBackgroundListener(mAlbumFileChangeListener);
    }

    private void load() throws IOException, ImageReadException {
        List<Path> subFiles;

        try (Stream<Path> subFilesStream = Files.list(mAlbumFile.getPath())) {
            subFiles = subFilesStream.collect(Collectors.toList());
        }

        for (Path file : subFiles) {
            if (Files.isDirectory(file)) {
                AlbumFile subAlbum = mContext.getAlbumFile(file);
                subAlbum.addWeakBackgroundListener(mSubAlbumChangeListener);

                mSubAlbums.add(subAlbum);
            } else if (isPhoto(file)) {
                mPhotoSet.add(loadPhoto(file));
            }
        }
    }

    private boolean isPhoto(Path path) {
        return Photo.isSupportedImageFormat(FilenameUtils.getExtension(path.toString()));
    }

    private Photo loadPhoto(Path path) throws IOException, ImageReadException {
        String name = path.getFileName().toString();

        return new Photo(mExecutor, mContext.getThumbnailer(), name, mAlbumFile, mPhotoTagDatabase);
    }

    private void tryLoadAndAddPhoto(Path path) {
        Photo newPhoto;

        try {
            newPhoto = loadPhoto(path);
        } catch (IOException | ImageReadException e) {
            e.printStackTrace();
            return;
        }

        mPhotoSet.add(newPhoto);
    }

    private void refreshContent() throws IOException, ImageReadException {
        List<Path> subFiles = Files.list(mAlbumFile.getPath())
                .collect(Collectors.toList());

        List<Photo> newPhotos = new ArrayList<>(mPhotoSet.size());
        Set<AlbumFile> newSubAlbums = new HashSet<>(mSubAlbums.size());

        for (Path file : subFiles) {
            if (Files.isDirectory(file))
                newSubAlbums.add(mContext.getAlbumFile(file));
            else if (isPhoto(file))
                newPhotos.add(loadPhoto(file));
        }

        boolean subAlbumChanged = false;
        Iterator<AlbumFile> it = mSubAlbums.iterator();

        while (it.hasNext()) {
            AlbumFile subAlbum = it.next();

            if (!newSubAlbums.contains(subAlbum)) {
                it.remove();
                subAlbum.removeBackgroundListener(mSubAlbumChangeListener);
                subAlbumChanged = true;
            }
        }

        for (AlbumFile subAlbum : newSubAlbums) {
            if (mSubAlbums.add(subAlbum)) {
                subAlbum.addWeakBackgroundListener(mSubAlbumChangeListener);
                subAlbumChanged = true;
            }
        }

        mPhotoSet.replaceContent(newPhotos);

        // TODO: Check if we need to execute callback to ensure good publication
        //       of the changes to the callback thread
        if (subAlbumChanged) {
            mExecutor.getCallbackExecutor().execute(() -> {
                for (ChangeListener l : mChangeListeners)
                    l.onSubAlbumListReloaded(this);
            });
        }
    }

    private void addSubAlbum(AlbumFile subAlbum) {
        if (!mSubAlbums.add(subAlbum))
            return;

        subAlbum.addWeakBackgroundListener(mSubAlbumChangeListener);

        mExecutor.getCallbackExecutor().execute(() -> {
            for (ChangeListener l : mChangeListeners)
                l.onSubAlbumAdded(Album.this, subAlbum);
        });
    }

    // The subAlbum must be contained in mSubAlbums
    private void removeSubAlbum(AlbumFile subAlbum, Path oldPath) {
        subAlbum.removeBackgroundListener(mSubAlbumChangeListener);

        mSubAlbums.remove(subAlbum);

        mExecutor.getCallbackExecutor().execute(() -> {
            for (ChangeListener l : mChangeListeners)
                l.onSubAlbumRemoved(this, oldPath);
        });
    }

    /**
     * Returns the name of this album.
     * @return the name of this album.
     */
    public String getName() {
        return mAlbumFile.getAlbumName();
    }

    /**
     * Returns the path of this album.
     * @return the path of this album.
     */
    public Path getPath() {
        return mAlbumFile.getPath();
    }

    /**
     * Returns the file associated to this album.
     * @return the file associated to this album.
     */
    public AlbumFile getFile() {
        return mAlbumFile;
    }

    /**
     * Returns an unmodifiable view of the file associated to the albums
     * contained in this album.
     *
     * @return an unmodifiable view of the file associated to the albums
     *         contained in this album.
     */
    public Set<AlbumFile> getSubAlbumFiles() {
        return Collections.unmodifiableSet(mSubAlbums);
    }

    /**
     * Executes asynchronously a query on the photos of this album.
     *
     * <p> The {@link LiveQueryResult} instance returned by this function is
     * initially empty and populated with the result of the query when the
     * latter complete. That {@link LiveQueryResult} instance is then updated
     * when the content of this album change until the {@link LiveQueryResult#stopSyncing()}
     * method is called.
     *
     * @param query The query to execute.
     * @return an empty {@link LiveQueryResult} instance, with the result of the query when the
     *         latter complete and updating its content as the content of this album change.
     */
    // TODO: Create recursive query executor
    public LiveQueryResult executeQuery(PhotoQuery query) {
        return new LiveQueryResult(mExecutor, mPhotoSet, query);
    }

    /**
     * Adds a change listener to this album.
     * @param listener The listener.
     */
    public void addListener(ChangeListener listener) {
        mChangeListeners.add(listener);
    }

    /**
     * Removes a listener from this album.
     * If the listener is not attached to this album, nothing happens.
     *
     * @param listener The listener.
     */
    public void removeListener(ChangeListener listener) {
        mChangeListeners.remove(listener);
    }

    private void disconnectListeners() {
        mContext.unregisterAlterationListener(mAlbumFile.getPath());
        mAlbumFile.removeBackgroundListener(mAlbumFileChangeListener);

        for (AlbumFile subAlbum : mSubAlbums)
            subAlbum.removeBackgroundListener(mSubAlbumChangeListener);
    }

    void close() {
        disconnectListeners();

        try {
            mPhotoTagDatabase.commit();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Interface that receives notifications of changes to an album.
     */
    public interface ChangeListener {
        /**
         * Called after a sub-album has been added to an album.
         * @param album the album in which the sub-album has been added
         * @param newSubAlbum the new sub-album file
         */
        void onSubAlbumAdded(Album album, AlbumFile newSubAlbum);

        /**
         * Called after a sub-album has been removed from an album.
         * @param album the album from which the sub-album has been removed
         * @param removedPath the path of the removed sub-album
         */
        void onSubAlbumRemoved(Album album, Path removedPath);

        /**
         * Called after a sub-album of an album has been renamed.
         * @param album the album containing the renamed sub-album
         * @param albumFile the renamed sub-album file
         * @param oldPath the old sub-album path
         * @param newPath the new sub-album path
         */
        void onSubAlbumRenamed(Album album, AlbumFile albumFile, Path oldPath, Path newPath);

        /**
         * Called after the whole sub-album list of an album has been reloaded.
         * @param album the album containing the sub-albums.
         */
        void onSubAlbumListReloaded(Album album);
    }
}
