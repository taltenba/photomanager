package com.arb.photomanager.model.db;

import com.arb.photomanager.util.WeakListenerCollection;
import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletionException;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * The file associated with an album, enabling to perform changes on that file.
 *
 * <p> Public methods of this class are only intended to be called in a single thread,
 * being the callback thread of the {@link AlbumDatabase} instance that has been used
 * to open the album from which an instance of this class originates.
 *
 * @author Thomas Altenbach
 */
public class AlbumFile {
    private final DatabaseExecutor mExecutor;

    private Path mPath;
    private String mAlbumName;

    private final List<ChangeListener> mChangeListeners;
    private final Collection<ChangeListener> mBackgroundListeners;

    AlbumFile(DatabaseExecutor executor, Path path) {
        mExecutor = Objects.requireNonNull(executor);
        mPath = Objects.requireNonNull(path);
        mAlbumName = path.getFileName().toString();
        mChangeListeners = new CopyOnWriteArrayList<>();
        mBackgroundListeners = new WeakListenerCollection<>();
    }

    /**
     * Returns the path of this album file.
     * @return the path of this album file
     */
    public Path getPath() {
        return mPath;
    }

    /**
     * Returns the name of this album file.
     * @return the name of this album file
     */
    public String getAlbumName() {
        return mAlbumName;
    }

    /**
     * Renames asynchronously this album file.
     *
     * @param newName the new album name
     * @param errorCallback optional callback invoked in case of failure
     */
    public void rename(String newName, @Nullable IOErrorCallback errorCallback) {
        Objects.requireNonNull(newName);

        mExecutor.supplyIOBackground(() -> {
            if (mAlbumName.equals(newName))
                return null;

            Path oldPath = mPath;
            Path newPath = mPath.resolveSibling(newName);

            try {
                Files.move(oldPath, newPath);
            } catch (IOException e) {
                throw new CompletionException(e);
            }

            mPath = newPath;
            mAlbumName = newName;

            onMoved(oldPath, newPath);

            for (ChangeListener l : mBackgroundListeners)
                l.onAlbumMoved(this, oldPath, newPath);

            return oldPath;
        }, errorCallback)
        .thenAcceptAsync((oldPath) -> {
            if (oldPath == null)
                return;

            for (ChangeListener l : mChangeListeners)
                l.onAlbumMoved(this, oldPath, oldPath.resolveSibling(newName));
        }, mExecutor.getCallbackExecutor());
    }

    /**
     * Copies asynchronously this album to the specified location.
     * The specified target must not exist.
     *
     * @param path the path to the target file
     * @param errorCallback optional callback invoked in case of failure
     */
    public void copyTo(Path path, @Nullable IOErrorCallback errorCallback) {
        Objects.requireNonNull(path);

        mExecutor.runIOBackground(() -> {
            if (mPath.equals(path))
                return;

            if (Files.exists(path))
                throw new CompletionException(new FileAlreadyExistsException(path.toString()));

            try {
                FileUtils.copyDirectory(mPath.toFile(), path.toFile());
            } catch (IOException e) {
                throw new CompletionException(e);
            }
        }, errorCallback);
    }

    /**
     * Copies asynchronously this album to within the specified album file, with the specified target name.
     * The specified target must not exist.
     *
     * @param albumFile the album file in which to copy this album
     * @param outName the target album name
     * @param errorCallback optional callback invoked in case of failure
     */
    public void copyTo(AlbumFile albumFile, String outName, @Nullable IOErrorCallback errorCallback) {
        copyTo(albumFile.getPath().resolve(outName), errorCallback);
    }

    /**
     * Copies asynchronously this album to within the specified album file, with the current album name.
     * The specified target must not exist.
     *
     * @param albumFile the album file in which to copy this album
     * @param errorCallback optional callback invoked in case of failure
     */
    public void copyTo(AlbumFile albumFile, @Nullable IOErrorCallback errorCallback) {
        copyTo(albumFile, mAlbumName, errorCallback);
    }

    /**
     * Moves asynchronously this album to the specified location.
     * The specified target must not exist.
     *
     * @param newPath the path to the target file
     * @param errorCallback optional callback invoked in case of failure
     */
    public void moveTo(Path newPath, @Nullable IOErrorCallback errorCallback) {
        Objects.requireNonNull(newPath);

        mExecutor.supplyIOBackground(() -> {
            if (mPath.equals(newPath))
                return null;

            Path oldPath = mPath;

            try {
                FileUtils.moveDirectory(mPath.toFile(), newPath.toFile());
            } catch (FileExistsException e) {
                throw new CompletionException(new FileAlreadyExistsException(e.getMessage()));
            } catch (IOException e) {
                throw new CompletionException(e);
            }

            mPath = newPath;
            mAlbumName = newPath.getFileName().toString();

            onMoved(oldPath, newPath);

            for (ChangeListener l : mBackgroundListeners)
                l.onAlbumMoved(this, oldPath, newPath);

            return oldPath;
        }, errorCallback)
        .thenAcceptAsync((oldPath) -> {
            if (oldPath == null)
                return;

            for (ChangeListener l : mChangeListeners)
                l.onAlbumMoved(this, oldPath, newPath);
        }, mExecutor.getCallbackExecutor());
    }

    /**
     * Removes this album file.
     *
     * @param errorCallback optional callback invoked in case of failure
     */
    public void remove(@Nullable IOErrorCallback errorCallback) {
        mExecutor.runIOBackground(() -> {
            if (!Files.exists(mPath))
                return;

            try {
                FileUtils.deleteDirectory(mPath.toFile());
            } catch (IOException e) {
                throw new CompletionException(e);
            }

            notifyRemoved();
        }, errorCallback);
    }

    /**
     * Adds a change listener to this album file.
     * @param listener The listener
     */
    public void addListener(ChangeListener listener) {
        mChangeListeners.add(listener);
    }

    /**
     * Removes a listener from this album file.
     * If the listener is not attached to this album file, nothing happens.
     *
     * @param listener The listener
     */
    public void removeListener(ChangeListener listener) {
        mChangeListeners.remove(listener);
    }

    Path resolve(String name) {
        return mPath.resolve(name);
    }

    void addWeakBackgroundListener(ChangeListener listener) {
        mBackgroundListeners.add(listener);
    }

    void removeBackgroundListener(ChangeListener listener) {
        mBackgroundListeners.remove(listener);
    }

    // Must only be called by an Album to notify that this AlbumFile was removed without using AlbumFile.delete
    // (was for example removed by the user using the OS file explorer)
    void notifyRemoved() {
        for (ChangeListener l : mBackgroundListeners)
            l.onAlbumRemoved(this);

        mExecutor.getCallbackExecutor().execute(() -> {
            for (ChangeListener l : mChangeListeners)
                l.onAlbumRemoved(this);
        });
    }

    // Callback for subclasses, must not be called by other classes !
    // This callback is called before change listeners callback are called.
    void onMoved(Path oldPath, Path newPath) {
        // Empty
    }

    /*@Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (!(o instanceof AlbumFile))
            return false;

        AlbumFile albumFile = (AlbumFile) o;
        return mPath.equals(albumFile.mPath);
    }

    @Override
    public int hashCode() {
        return mPath.hashCode();
    }*/

    /**
     * Interface that receives notifications of changes to an album file.
     */
    public interface ChangeListener {
        /**
         * Called after an album file has been moved.
         * @param albumFile the moved album file
         * @param oldPath the old path of the album file
         * @param newPath the new path of the album file
         */
        void onAlbumMoved(AlbumFile albumFile, Path oldPath, Path newPath);

        /**
         * Called after an album file has been removed
         * @param albumFile the removed album file
         */
        void onAlbumRemoved(AlbumFile albumFile);
    }
}
