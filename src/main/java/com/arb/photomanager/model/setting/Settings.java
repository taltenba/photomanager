package com.arb.photomanager.model.setting;


import com.arb.photomanager.PhotoManagerApp;

import java.util.Collection;
import java.util.EnumMap;
import java.util.Set;
import java.util.prefs.Preferences;

/**
 * A singleton to store the application settings
 * @author Aymeric Robitaille
 */
public final class Settings {

    /*  Internal settings (can't be modified by user)   */
    //Views
        //Files
    static final public String EDIT_WINDOW_FILE = "/fxml/edit_window.fxml";
    static final public String NEW_VIEWER_WINDOW_FILE = "/fxml/new_viewer_window.fxml";
    static final public String HELP_WINDOW_FILE = "/fxml/help_window.fxml";
    static final public String SETTINGS_WINDOW_FILE = "/fxml/setting_window.fxml";
    static final public String MAIN_WINDOW_FILE = "/fxml/main_window.fxml";
    static final public String META_WINDOW_FILE = "/fxml/meta_window.fxml";
    static final public String PHOTO_VIEWER_WIDGET_WINDOW_FILE = "/fxml/photo_viewer_widget.fxml";
    static final public String EDIT_PARAM_FILE = "/fxml/edit_param_widget.fxml";

        //Window names
    static final public String EDIT_WINDOW_NAME = "Edition";
    static final public String NEW_VIEWER_WINDOW_NAME = "New manager";
    static final public String HELP_WINDOW_NAME = "Help";
    static final public String SETTINGS_WINDOW_NAME = "Settings";
    static final public String MAIN_WINDOW_NAME = "PhotoManager";
    static final public String META_WINDOW_NAME = "Metadata";
    static final public String PHOTO_VIEWER_WIDGET_WINDOW_NAME = "Photo viewer";

        //Ico
    static final public String ICO_FILE = "/img/Logo/logo_ico.png";

        //Placeholder
    static final public String PLACEHOLDER_FILE = "/img/basic_placeholder.png";

    public static final int MAX_THUMBNAIL_WIDTH = 256;
    public static final int MAX_THUMBNAIL_HEIGHT = 256;
    public static final int MAX_STRING_LENGTH = Preferences.MAX_VALUE_LENGTH;

    /*  Enumeration of editable parameters  */
    public enum EditableSetting {
        UI_THEME,
        OVERLAY_DELAY,
        THUMBNAIL_WIDTH,
        THUMBNAIL_HEIGHT,
        PREVIOUSLY_OPENED_ALBUMS;

        static public boolean contains(String key) {

            for(EditableSetting e : values()) {
                if(e.name().equals(key))
                    return true;
            }

            return false;
        }

        static public boolean haveAllSettingsKeys(Set<String> keys) {

            for(EditableSetting e : values()) {
                if(!keys.contains(e.name()))
                    return false;
            }

            return true;
        }
    }

    /*  Internal class attributes   */
    final private Preferences mPref;
    private EnumMap<EditableSetting, Setting> mEditableSettings;


    final private static Settings ourInstance = new Settings();

    public static Settings getInstance() {
        return ourInstance;
    }

    /**
     * Initiates settings according to default values and the data base of preferences
     */
    private Settings() {
        mEditableSettings = defaultSettings();

        mPref = Preferences.userNodeForPackage(PhotoManagerApp.class);

        try{
            String[] keys = mPref.keys();

            if (!EditableSetting.haveAllSettingsKeys(Set.of(keys)))
                setDefaultSettings();     //write the default settings to the DB
            else
                setSettingsFromDB(keys);

        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Stores the settings in the data base
     */
    private void updateDB() {

        for(EditableSetting key : mEditableSettings.keySet()) {
            mPref.put(key.name(), mEditableSettings.get(key).get().toString());
        }
    }

    /**
     * Takes settings from the data base according to the given keys
     * @param keys setting keys
     */
    private void setSettingsFromDB(String[] keys) {
        String val;

        for(String key : keys) {
            if(EditableSetting.contains(key)) {
                val = mPref.get(key, "undefined");
                mEditableSettings.get(EditableSetting.valueOf(key)).safeSetFromString(val);
            }
        }
    }

    /**
     * Set back the default settings
     */
    public void setDefaultSettings() {
        mEditableSettings = defaultSettings();
        updateDB();
    }

    /**
     * Gives the default settings map
     * @return the default settings map
     */
    public EnumMap<EditableSetting, Setting> defaultSettings() {
        EnumMap<EditableSetting, Setting> sMap = new EnumMap<>(EditableSetting.class);


        //Settings <Key, Setting>
        sMap.put(EditableSetting.UI_THEME, new Setting(
                "Dark",
                Set.of("Dark", "White"),
                Setting::listCheck,
                Setting::stringFromString
        ));

        sMap.put(EditableSetting.OVERLAY_DELAY, new Setting(
                3000,
                Set.of(1000, 60000),
                Setting::intBoundCheck,
                Setting::intFromString
        ));

        sMap.put(EditableSetting.THUMBNAIL_HEIGHT, new Setting(
                128,
                Set.of(
                        64,
                        MAX_THUMBNAIL_HEIGHT
                ),
                Setting::intBoundCheck,
                Setting::intFromString
        ));

        sMap.put(EditableSetting.THUMBNAIL_WIDTH, new Setting(
                128,
                Set.of(
                        64,
                        MAX_THUMBNAIL_WIDTH
                ),
                Setting::intBoundCheck,
                Setting::intFromString
        ));

        sMap.put(EditableSetting.PREVIOUSLY_OPENED_ALBUMS, new Setting(
                "",
                Set.of(
                        0,
                        MAX_STRING_LENGTH
                ),
                Setting::stringSizeCheck,
                Setting::stringFromString
        ));

        return sMap;
    }

    /**
     * Gives the setting according to the key setting
     * @param key of the setting
     * @return setting at the corresponding key
     */
    public Setting get(EditableSetting key) {
        return mEditableSettings.get(key);
    }

    /**
     * Sets the setting according to the key setting and the new value
     * @param key of the setting in the EnumMap
     * @param newVal the new value
     */
    public void set(EditableSetting key, Object newVal) {
        get(key).safeSet(newVal);
        updateDB();
    }

    /**
     * Gives the allowed values for editable settings
     * @return the corresponding collection
     */
    public Collection<Object> getAllowedValues(EditableSetting key) {
        return mEditableSettings.get(key).getAllowedValues();
    }


    /**
     * Gives the current UI theme
     * @return the current UI theme
     */
    public String getUITheme() {
        return (String)mEditableSettings.get(EditableSetting.UI_THEME).get();
    }

    /**
     * Gives the overlay delay in ms
     * @return the overlay delay in ms
     */
    public int getOverlayDelay() {
        return (int)mEditableSettings.get(EditableSetting.OVERLAY_DELAY).get();
    }

    /**
     * Gives the Thumbnails width
     * @return the Thumbnails width
     */
    public int getThumbnailWidth() {
        return (int)mEditableSettings.get(EditableSetting.THUMBNAIL_WIDTH).get();
    }

    /**
     * Gives the Thumbnails height
     * @return the Thumbnails height
     */
    public int getThumbnailHeight() {
        return (int)mEditableSettings.get(EditableSetting.THUMBNAIL_HEIGHT).get();
    }

    public String getPreviouslyOpenedAlbums() {
        return (String)mEditableSettings.get(EditableSetting.PREVIOUSLY_OPENED_ALBUMS).get();
    }
}
