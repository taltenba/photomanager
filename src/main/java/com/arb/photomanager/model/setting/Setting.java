package com.arb.photomanager.model.setting;

import javafx.beans.property.StringProperty;
import javafx.util.Pair;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.function.BiFunction;

public class Setting {

    private Object mVal;
    private Object mDefaultValue;
    private Collection<Object> mAllowedValues;
    private BiFunction<Object, Collection<Object>, Boolean> mCheckFunc;
    private BiFunction<String, Object, Object> mStringToTFunc;

    /**
     * Creates a Setting object
     * @param val value of the setting
     * @param defaultValue default value
     * @param allowedValues allowed values, needs an immutable copy
     * @param checkFunc function called to check if a value is correct
     *                  takes allowed values and an Object to test,
     *                  returns a boolean
     * @param stringToTFunc function to transform a string to the proper object
     *                      takes a String and the default value object
     *                      returns the new object
     */
    Setting(
            Object val,
            Object defaultValue,
            Collection<Object> allowedValues,
            BiFunction<Object, Collection<Object>, Boolean> checkFunc,
            BiFunction<String, Object, Object> stringToTFunc
    ) {
        mVal = Objects.requireNonNull(val);
        mDefaultValue = Objects.requireNonNull(defaultValue);
        mAllowedValues = Objects.requireNonNull(allowedValues);
        mCheckFunc = Objects.requireNonNull(checkFunc);
        mStringToTFunc = Objects.requireNonNull(stringToTFunc);
    }

    /**
     * Creates a Setting object
     * @param defaultValue default value
     * @param allowedValues allowed values
     * @param checkFunc function called to check if a value is correct
     *                  takes allowed values and an Object to test,
     *                  returns a boolean
     * @param stringToTFunc function to transform a string to the proper object
     *                      takes a String and the default value object
     *                      returns the new object
     */
    public Setting(
            Object defaultValue,
            Collection<Object> allowedValues,
            BiFunction<Object, Collection<Object>, Boolean> checkFunc,
            BiFunction<String, Object, Object> stringToTFunc
    ) {
       this(defaultValue, defaultValue, allowedValues, checkFunc, stringToTFunc);
    }

    /**
     * Gives the value of the setting
     * @return the value of the setting
     */
    public Object get() {
        return mVal;
    }

    /**
     * Gives the allowed values for the setting
     * @return the allowed values for the setting
     */
    public Collection<Object> getAllowedValues() {
        return mAllowedValues;
    }

    /**
     * Set the setting's value with a string, if the string cannot be converted the default value is set
     * @param val new value
     * @return true if the new value is set
     */
    public boolean safeSetFromString(String val) {
        return safeSet(mStringToTFunc.apply(val, mDefaultValue));
    }

    /**
     * Set the setting's value, if the value cannot be converted the default value is set
     * @param val new value
     * @return true if the new value is set
     */
    public boolean safeSet(Object val) {
        this.mVal = val;
        return checkAndSetDef();
    }

    /**
     * Checks if the new value is correct
     * @return true or false
     */
    private boolean check() {
        return mCheckFunc.apply(mVal, mAllowedValues);
    }

    /**
     * Checks if the new value is correct, if not set the value to its default value
     * @return true or false
     */
    private boolean checkAndSetDef() {
        if(check())
            return true;

        mVal = mDefaultValue;
        return false;
    }

    /**
     * Checks if val is in the collection allowedValues
     * @param val an Object
     * @param allowedValues a Collection of Object
     * @return true or false
     */
    static public boolean listCheck(Object val, Collection<Object> allowedValues) {
        return allowedValues.contains(val);
    }

    /**
     * Checks if val.length is in the collection allowedValues
     * @param val an Object
     * @param allowedValues a Collection of Object
     * @return true or false
     */
    static public boolean stringSizeCheck(Object val, Collection<Object> allowedValues) {
        String str = (String)val;

        return intBoundCheck(str.length(), allowedValues);
    }

    /**
     * Considers allowedValues as an integer array, sorts it and checks if val is between the min and the max value
     * @param val an Object
     * @param allowedValues a Collection of Object
     * @return true or false
     */
    static public boolean intBoundCheck(Object val, Collection<Object> allowedValues) {
        Pair<Integer, Integer> bounds = toBounds(allowedValues);

        return (int) val <= bounds.getValue() && (int) val >= bounds.getKey();
    }

    /**
     * Transforms the string to a string ...
     * @param val string to transform
     * @param def default value
     * @return val
     */
    static public String stringFromString(String val, Object def) {
        return val;
    }

    /**
     * Transforms a string to an integer
     * @param val string to transform
     * @param def default value
     * @return corresponding integer
     */
    static public int intFromString(String val, Object def) {
        try {
            return Integer.parseInt(val);
        }
        catch( Exception e ) {
            return (int)def;
        }
    }

    /**
     * Transforms the Object collection c to a String collection
     * @param c Object collection
     * @return String collection (HashSet)
     */
    static public Collection<String> toStringCollection(Collection<Object> c) {
        Collection<String> s = new HashSet<>();

        for(Object o : c)
            s.add((String)o);

        return s;
    }

    /**
     * Transforms the Object collection c to a Integer collection
     * @param c Object collection
     * @return Integer collection (HashSet)
     */
    static public Collection<Integer> toIntegerCollection(Collection<Object> c) {
        Collection<Integer> s = new HashSet<>();

        for(Object o : c)
            s.add((Integer)o);

        return s;
    }

    /**
     *  Gives the bounds in accordance with the allowed values
     * @param c Object collection
     * @return Pair of Integer, first is the lower bound, second is the higher bound
     */
    static public Pair<Integer, Integer> toBounds(Collection<Object> c) {
        int i = 0;
        int[] bounds = new int[c.size()];

        for(Object o : c) {
            bounds[i] = (int)o;
            ++i;
        }

        Arrays.sort(bounds);

        return new Pair<>(bounds[0], bounds[bounds.length - 1]);
    }
}
