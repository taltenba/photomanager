package com.arb.photomanager.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Controller of the help view
 * @author Aymeric Robitaille
 */
public class HelpController extends AbstractController{

    /*  Private attributes  */

    //UI sizes
    static final private int WINDOW_WIDTH = 200;
    static final private int WINDOW_HEIGHT = 350;

    //UI links
    static final private String HELP_LINK = "https://www.utbm.fr/";

    //UI controls
    @FXML private Button mOkBtn;

    /**
     * Gets the stage of the controlled view
     * @return the stage of the controlled view
     */
    public Stage getStage() {
        return (Stage)mOkBtn.getScene().getWindow();
    }

    /**
     * Initializes the controller by adding eventFilters on mouse pressed and on key pressed
     * <p>Is called by the init() method</p>
     */
    final protected void initController() {
        getStage().setMinHeight(WINDOW_HEIGHT);
        getStage().setMinWidth(WINDOW_WIDTH);
    }

    /**
     * Closes the application
     */
    @FXML
    public void handleOkBtn(){
        close();
    }

    /**
     * Opens the link
     */
    @FXML
    public void handleLinkClicked() {
        if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
            try {
                Desktop.getDesktop().browse(new URI(HELP_LINK));
            } catch (IOException | URISyntaxException e) {
                e.printStackTrace();
            }
        }
    }
}
