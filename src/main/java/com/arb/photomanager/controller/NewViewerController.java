package com.arb.photomanager.controller;

import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 *
 * @author Aymeric Robitaille
 */
public class NewViewerController extends AbstractController {

    /*  Private attributes  */

    /*  UI attributes   */

    //UI sizes
    static final private int WINDOW_WIDTH = 600;
    static final private int WINDOW_HEIGHT = 300;

    //Controls
    @FXML private Button mDirChoiceBtn;

    @FXML private TextField mTxtFolder;


    /*  Methods redefinition    */

    /**
     * Initializes the controller by adding eventFilters on mouse pressed and on key pressed
     * <p>Is called by the init() method</p>
     */
    final protected void initController(){
        getStage().setMinHeight(WINDOW_HEIGHT);
        getStage().setMinWidth(WINDOW_WIDTH);
    }

    /**
     * Gives the stage of the dialog
     * @return the stage of the dialog
     */
    public Stage getStage() {
        return (Stage)mDirChoiceBtn.getScene().getWindow();
    }

    public String getSelectedPath() {
        return mTxtFolder.getText();
    }

    public boolean checkPathInput() {
        if (getSelectedPath().isBlank())
            return false;

        Path path = Paths.get(getSelectedPath());
        return Files.exists(path);
    }

    public StringProperty getTextFieldProperty() {
        return mTxtFolder.textProperty();
    }

    @FXML
    public void handleBtnDirChoose() {
        DirectoryChooser dirChooser = new DirectoryChooser();
        dirChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        File selectedDir = dirChooser.showDialog(mDirChoiceBtn.getScene().getWindow());

        if (selectedDir == null)
            return;

        mTxtFolder.setText(selectedDir.getAbsolutePath());
    }
}
