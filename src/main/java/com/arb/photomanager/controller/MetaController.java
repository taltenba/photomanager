package com.arb.photomanager.controller;

import com.arb.photomanager.model.db.Photo;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import org.apache.commons.imaging.formats.tiff.TiffImageMetadata;

public class MetaController extends AbstractController{

    //UI sizes
    static final private int WINDOW_WIDTH = 600;
    static final private int WINDOW_HEIGHT = 400;

    @FXML private Button mOkBtn;
    @FXML private TextArea mTextArea;

    /**
     * Initializes the controller by adding eventFilters on mouse pressed and on key pressed
     * <p>Is called by the init() method</p>
     */
    final protected void initController(){
        getStage().setMinHeight(WINDOW_HEIGHT);
        getStage().setMinWidth(WINDOW_WIDTH);
    }

    public Stage getStage() {
        return (Stage)mOkBtn.getScene().getWindow();
    }

    @FXML
    public void handleOkBtn(){
        close();
    }

    public void setData(Photo photo) {
        if (photo == null || !photo.hasMetadata())
            close();

        //noinspection ConstantConditions
        TiffImageMetadata metadata = photo.getMetadata();

        if (metadata != null)
            mTextArea.setText(metadata.toString());

    }

 }


