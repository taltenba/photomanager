package com.arb.photomanager.controller;

import com.arb.photomanager.model.db.*;
import com.arb.photomanager.model.setting.Settings;
import com.arb.photomanager.util.AlbumPathsParser;
import com.arb.photomanager.util.FileUtil;
import com.arb.photomanager.util.SortingParser;
import com.arb.photomanager.view.*;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.Window;
import org.apache.commons.imaging.ImageInfo;
import org.apache.commons.io.FilenameUtils;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
/**
 * Controller of the main view
 * @author Aymeric Robitaille
 */
public class MainController extends AbstractController{

    private enum SortingMethod {
        NAME("Name"),
        DATE("Date");

        private final String name;

        SortingMethod(String s) {
            name = s;
        }

        public boolean equalsName(String otherName) {
            return name.equals(otherName);
        }

        public String toString() {
            return this.name;
        }
    }

    private enum FilteringMethod {
        NAME("Name"),
        DATE("Date"),
        TAGS("Tags");

        private final String name;

        FilteringMethod(String s) {
            name = s;
        }

        public boolean equalsName(String otherName) {
            return name.equals(otherName);
        }

        public String toString() {
            return this.name;
        }
    }

    /*  Private attributes  */

    private double[] mOldDividersPos;

    //Photo information
    private ObservableList<Photo> mCurrentPhotoList;
    private int mSelectedPhotoIndex;
    private PhotoQuery.Filter.Builder mQueryBuilder;
    private SortingMethod mSortingMethod;
    private FilteringMethod mSelectedFilteringMethod;

    private Album mCurrentAlbum;

    //Booleans
    private boolean mIsEditTag;

    //Default values
    static final private String UNDEFINED_MSG = "Undefined";
    static final private String RENAME_MENU_MSG = "Rename";
    static final private String REMOVE_MENU_MSG = "Remove";
    static final private String ADD_MENU_MSG = "Add";
    static final private String REMOVE_ALL_MENU_MSG = "Remove all";


    /*  UI Controls */

    //UI sizes
    static final private int WINDOW_WIDTH = 1200;
    static final private int WINDOW_HEIGHT = 500;

    //Personal
    private AlbumTree mTreeView;
    private PhotoGridView mGridView;

    //Containers
    @FXML private VBox mTreeViewContainer;
    @FXML private Node mMainContainer;
    @FXML private SplitPane mMainSplitPane;
    @FXML private HBox mTitledLeftPanContainer;
    @FXML private HBox mTitledRightPanContainer;
    @FXML private ScrollPane mRightPanContainer;
    @FXML private VBox mPhotoGridContainer;

    //Buttons
    @FXML private Button mRetractLeftPanBtn;
    @FXML private Button mRetractRightPanBtn;
    @FXML private MenuButton mOpenMenuBtn;
    @FXML private MenuItem mOpenAllBtn;
    @FXML private Button mEditBtn;
    @FXML private Button mAddAlbumBtn;

    //ComboBoxes
    @FXML private ComboBox<String> mSearchFieldCombo;
    @FXML private ComboBox<String> mSortCombo;

    //Inputs
    @FXML private TextField mTagTxtField;
    @FXML private TextField mFilterTxtField;

    //Outputs
    @FXML private ListView<String> mTagsList;

    //Labels
    @FXML private Label mLoadingLbl;
    //Photo info labels
    //General
    @FXML private Label mPhotoNameLbl;
    @FXML private Label mPhotoDateLbl;
    @FXML private Label mPhotoAlbumLbl;

    //Picture properties
    @FXML private Label mPhotoResolutionLbl;
    @FXML private Label mPhotoHeightLbl;
    @FXML private Label mPhotoWidthLbl;
    @FXML private Label mPhotoColorDepthLbl;

    //File properties
    @FXML private Label mSizeOnDiskLbl;
    @FXML private Label mFilePathLbl;


    /*  Constructors    */

    /**
     * Default constructor of Controller()
     */
    public MainController() {
        mOldDividersPos = new double[2];

        mSelectedPhotoIndex = -1;
        mCurrentPhotoList = FXCollections.observableArrayList();
        mQueryBuilder = new PhotoQuery.Filter.Builder();
        mCurrentAlbum = null;

        mIsEditTag = false;
        mSortingMethod = SortingMethod.NAME;
        mSelectedFilteringMethod = FilteringMethod.NAME;
    }

    /*  Methods redefinitions   */

    /**
     * Initializes the controller by adding eventFilters on mouse pressed and on key pressed
     * <p>Is called by the init() method</p>
     */
    final protected void initController(){
        //init the model
        setDataBase(new AlbumDatabase(Platform::runLater));

        //controller and view
        getStage().setMinHeight(WINDOW_HEIGHT);
        getStage().setMinWidth(WINDOW_WIDTH);

        setData();

        initPhotoGridView();
        initTreeView();
        initTagsView();
        initOpenMenu();

        setRightPanelToDefAndBlocked();
        setLoading(false);

        mFilterTxtField.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
            if (event.getCode() == KeyCode.ENTER) {
                handleFilterBtn();
            }
        });

        //For tests
//        mTreeView.open(System.getProperty("user.dir") + "\\test\\Root album\\Crunch 2017");
//        mTreeView.open(System.getProperty("user.dir") + "\\test\\Root album\\Crunch 2019");
    }

    /**
     * Returns the stage corresponding to the controller
     * @return the stage corresponding to the controller
     */
    public Stage getStage() {
        return (Stage)mMainContainer.getScene().getWindow();
    }

    /**
     * Close properly the controller
     * <p>This method can be redefined by children classes</p>
     */
    final protected void closeController() {
        mAlbumDataBase.close();

        Collection<Path> openedFolderStringPath = mTreeView.getRootFolderPaths();
        if (!openedFolderStringPath.isEmpty()) { //if there are opened folder we save it
            mSettings.set(
                    Settings.EditableSetting.PREVIOUSLY_OPENED_ALBUMS,
                    AlbumPathsParser.toString(openedFolderStringPath)
            );
        }
    }


    /*  Private methods */

    /**
     * Set the data of the view
     */
    private void setData(){

        //Search combo
        mSearchFieldCombo.getItems().clear();

        for (FilteringMethod m : FilteringMethod.values())
            mSearchFieldCombo.getItems().add(m.toString());

        mSearchFieldCombo.getSelectionModel().selectFirst();

        mSelectedFilteringMethod = FilteringMethod.values()[0];

        //Sort combo
        mSortCombo.getItems().clear();

        for (SortingMethod m : SortingMethod.values())
            mSortCombo.getItems().add(m.toString());

        mSortCombo.getSelectionModel().selectFirst();

        mSortingMethod = SortingMethod.values()[0];
    }

    /**
     * Initiates the tags view
     *  * Adds a contextual menu
     */
    private void initTagsView() {
        ContextMenu contextMenu = new ContextMenu();

        MenuItem remove = new MenuItem(REMOVE_MENU_MSG);
        MenuItem removeAll = new MenuItem(REMOVE_ALL_MENU_MSG);
        MenuItem rename = new MenuItem(RENAME_MENU_MSG);
        MenuItem add = new MenuItem(ADD_MENU_MSG);

        removeAll.setAccelerator(new KeyCodeCombination(KeyCode.DELETE, KeyCombination.CONTROL_DOWN));

        contextMenu.getItems().addAll(
                add,
                rename,
                remove,
                removeAll
        );

        remove.setOnAction(event -> handleRemoveTagBtn());
        removeAll.setOnAction(event -> mTagsList.getItems().clear());
        rename.setOnAction(event -> handleEditTagBtn());
        add.setOnAction(event -> mTagTxtField.requestFocus());

        mTagsList.setContextMenu(contextMenu);
    }

    /**
     * Initiates the photo grid view widget
     */
    private void initPhotoGridView() {
        mPhotoGridContainer.getChildren().clear();

        //Construction of the GridView
        mGridView = new PhotoGridView(mCurrentPhotoList);
        mGridView.setOnSelectionChangedCallbackFct(this::handlePhotoSelectionChanged);
        mGridView.setOpenFct(this::openAll);
        mGridView.setRemoveFct(this::removeAll);
        mGridView.setMoveFct(this::moveAll);
        mPhotoGridContainer.getChildren().add(mGridView);

    }

    /**
     * Initiates the albums tree view
     */
    private void initTreeView() {
        mTreeView = new AlbumTree();
        mTreeView.setAlbumDatabase(getDataBase());
        mTreeView.setLoadingStateCallback(this::setLoading);
        mTreeView.setOnSelectionChangedCallback(this::handleAlbumSelectionChanged);
        mTreeView.selectRoot();
        mTreeViewContainer.getChildren().clear();
        mTreeViewContainer.getChildren().add(mTreeView);
    }

    /**
     * Initiates the open menu with the previously opened albums
     */
    private void initOpenMenu() {
        //Recently opened files
        Collection<Path> lastPaths = AlbumPathsParser.fromString(mSettings.getPreviouslyOpenedAlbums());

        for (Path path : lastPaths) {
            MenuItem item = new MenuItem(path.toString());
            item.setOnAction(event -> mTreeView.open(item.getText()));

            mOpenMenuBtn.getItems().add(item);
        }

        mOpenAllBtn.setAccelerator(
                new KeyCodeCombination(KeyCode.O, KeyCombination.CONTROL_DOWN, KeyCombination.ALT_DOWN)
        );

        mOpenAllBtn.setOnAction(event -> {
            for (Path path : AlbumPathsParser.fromString(mSettings.getPreviouslyOpenedAlbums())) {
                mTreeView.open(path.toString());
            }
        });
    }

    /**
     * Sets the loading state
     * @param isLoading if true the view goes to a loading view (here a label is displayed)
     */
    private void setLoading(boolean isLoading) {
        if (isLoading)
            mLoadingLbl.setVisible(true);
        else
            mLoadingLbl.setVisible(false);
    }

    /**
     * Resets all fields and block the buttons
     *  * Sets the selected photo index to -1
     */
    private void setRightPanelToDefAndBlocked() {
        mSelectedPhotoIndex = -1;
        mRightPanContainer.setDisable(true);

        updateGeneralPhotoInfo(null);
        updateTagsPhotoInfo(null);
        updatePhotoPropertiesInfo(null);
        updateFilePropertiesInfo(null);
    }

    /**
     * Updates all information according to the selected photo
     */
    private void updatePhotoInfo() {
        Photo selectedPhoto = null;

        if (mSelectedPhotoIndex != -1)
            selectedPhoto = mCurrentPhotoList.get(mSelectedPhotoIndex);
        else {
            setRightPanelToDefAndBlocked();
            mGridView.unSelectAll();
        }

        mRightPanContainer.setDisable(selectedPhoto == null);

        if (selectedPhoto != null) {
            mEditBtn.setDisable(FilenameUtils.getExtension(selectedPhoto.getName()).equals("gif"));
        }

        updateGeneralPhotoInfo(selectedPhoto);
        updateTagsPhotoInfo(selectedPhoto);
        updatePhotoPropertiesInfo(selectedPhoto);
        updateFilePropertiesInfo(selectedPhoto);
    }

    /**
     * Updates general information about the selected photo
     * @param photo to use for updating information
     */
    private void updateGeneralPhotoInfo(@Nullable Photo photo) {
        if (photo == null) {
            mPhotoNameLbl.setText(UNDEFINED_MSG);
            mPhotoDateLbl.setText(UNDEFINED_MSG);
            mPhotoAlbumLbl.setText(UNDEFINED_MSG);
        } else {
            mPhotoNameLbl.setText(photo.getName());
            mPhotoDateLbl.setText(FileUtil.readableFileDate(photo.getCreationTime()));
            mPhotoAlbumLbl.setText(photo.getAlbumFile().getAlbumName());
        }
    }

    /**
     * Updates tags information about the selected photo
     * @param photo to use for updating information
     */
    private void updateTagsPhotoInfo(@Nullable Photo photo) {
        mTagsList.getItems().clear();

        if (photo == null)
            return;

        mTagsList.getItems().addAll(photo.getTags());
    }

    /**
     * Updates photo properties information about the selected photo
     * @param photo to use for updating information
     */
    private void updatePhotoPropertiesInfo(@Nullable Photo photo) {
        if (photo == null) {
            mPhotoResolutionLbl.setText(UNDEFINED_MSG);
            mPhotoHeightLbl.setText(UNDEFINED_MSG);
            mPhotoWidthLbl.setText(UNDEFINED_MSG);
            mPhotoColorDepthLbl.setText(UNDEFINED_MSG);
        } else {
            ImageInfo imgInfo = photo.getImageInfo();

            if (imgInfo.getPhysicalHeightDpi() == -1)
                mPhotoResolutionLbl.setText("Undefined");
            else
                mPhotoResolutionLbl.setText(imgInfo.getPhysicalHeightDpi() + "dpi");

            mPhotoHeightLbl.setText(imgInfo.getHeight() + "px");
            mPhotoWidthLbl.setText(imgInfo.getWidth() + "px");
            mPhotoColorDepthLbl.setText(String.valueOf(imgInfo.getBitsPerPixel()));
        }
    }

    /**
     * Updates file properties information about the selected photo
     * @param photo to use for updating information
     */
    private void updateFilePropertiesInfo(@Nullable Photo photo) {
        if (photo == null) {
            mSizeOnDiskLbl.setText(UNDEFINED_MSG);
            mFilePathLbl.setText(UNDEFINED_MSG);
        } else {
            mSizeOnDiskLbl.setText(FileUtil.readableFileSize(photo.getDiskSize()));
            mFilePathLbl.setText(photo.getPath().toString());
        }
    }

    /**
     * Hides or shows the slide pane divider according to the index
     * @param index index of the split pane divider in the array
     * @param btn button for hiding and showing
     * @param titleNode the node that contains the tile of the panel
     * @param containerNode the main container of the panel
     */
    private void changePanelState(int index, Button btn, Node titleNode, Node containerNode){
        String newText;
        double newPos;
        Window primary = getStage();

        //Get the current divider
        SplitPane.Divider div = mMainSplitPane.getDividers().get(index);

        //We find the dividers in the main splitPan and order by LayoutX property
        List<Node> dividers = new ArrayList<>(mMainSplitPane.lookupAll(".split-pane-divider"));
        dividers.sort((Node e1, Node e2) -> {
                    if(e1.getLayoutX() == e2.getLayoutX()){
                        return 0;
                    }
                    return e1.getLayoutX() < e2.getLayoutX() ? -1 : 1;
                }
        );

        //We hide/display the components of the panel
        titleNode.setVisible(!titleNode.isVisible());
        containerNode.setVisible(!containerNode.isVisible());


        //We change the button view and retract or extend the panel
        if(containerNode.isVisible()) {
            if(index == 0){
                newText = "<";
            }else{
                newText = ">";
            }

            newPos = mOldDividersPos[index];
            dividers.get(index).setMouseTransparent(false);
            dividers.get(index).getStyleClass().removeAll("hide");
        }else {
            if(index == 0){
                newText = ">";
                newPos = (btn.getWidth() + 5) / primary.getWidth();
            }else{
                newText = "<";
                newPos = (primary.getWidth() - btn.getWidth() - 5) / primary.getWidth();
            }
            mOldDividersPos[index] = div.getPosition();

            dividers.get(index).setMouseTransparent(true);
            dividers.get(index).getStyleClass().add("hide");
        }

        btn.setText(newText);
        div.setPosition(newPos);
    }

    /**
     * Handler for the album selection changing
     * @param album new selected album
     */
    private void handleAlbumSelectionChanged(@Nullable Album album) {
        setRightPanelToDefAndBlocked();
        mGridView.unSelectAll();

        mCurrentAlbum = album;

        if (album == null) {
            mAddAlbumBtn.setDisable(true);
            mCurrentPhotoList = FXCollections.observableArrayList();
            return;
        }

        mAddAlbumBtn.setDisable(false);

        filter(mCurrentAlbum);
    }

    /**
     * Handler for the photo grid selection changed
     * @param selectedIndexes selected photos indexes
     */
    private void handlePhotoSelectionChanged(Collection<Integer> selectedIndexes) {
        if (selectedIndexes == null)
            return;

        if (selectedIndexes.size() != 1) {
            mSelectedPhotoIndex = -1;
            setRightPanelToDefAndBlocked();
        } else {
            Iterator<Integer> it = selectedIndexes.iterator();
            mSelectedPhotoIndex = it.next();
            updatePhotoInfo();
        }
    }

    /**
     * Remove all selected photos
     * @param selectedIndexes selected photos indexes
     */
    private void removeAll(Collection<Integer> selectedIndexes) {
        if (selectedIndexes == null)
            return;

        for (int i : selectedIndexes)
            remove(i);
    }

    /**
     * Removes the photo according to the index
     * @param index index of the photo to delete
     */
    private void remove(int index) {
        if (index < 0 || index >= mCurrentPhotoList.size())
            return;

        Photo photo = mCurrentPhotoList.get(index);
        Photo.ChangeListener changeListener = getPhotoChangeListener();
        photo.addListener(changeListener);

        photo.remove(t -> {
            if (t == null)
                return;

            photo.removeListener(changeListener);

            CustomAlert al = new CustomAlert(Alert.AlertType.ERROR, getStage());
            al.setTexts(
                    "Error occurred when deleting the photo",
                    t.getMessage()
            );

            al.showAndWait();
        });

        mSelectedPhotoIndex = -1;
        updatePhotoInfo();
    }

    /**
     * Opens all selected photos or all if no or one photo is selected
     * @param selectedIndexes selected photos indexes
     */
    private void openAll(@Nullable Collection<Integer> selectedIndexes) {
        if (mCurrentPhotoList == null || mCurrentPhotoList.size() == 0)
            return;

        List<Photo> photoToDisplayList = new ArrayList<>();
        int startIndex;

        if (
                selectedIndexes == null ||
                        selectedIndexes.size() == 0 ||
                        selectedIndexes.size() == 1
        ) {
            startIndex = mSelectedPhotoIndex;
            photoToDisplayList = mCurrentPhotoList;
        } else {
            startIndex = 0;

            for (int i : selectedIndexes)
                photoToDisplayList.add(mCurrentPhotoList.get(i));
        }


        Window primary = getStage();

        PhotoViewerWidgetController c = (PhotoViewerWidgetController)UiUtility.openSoftDialogFromFxml(
                Settings.PHOTO_VIEWER_WIDGET_WINDOW_FILE,
                primary,
                Settings.PHOTO_VIEWER_WIDGET_WINDOW_NAME,
                (int)(primary.getWidth() * 0.9),
                (int)(primary.getHeight() * 0.9)
        );

        c.setPhotoList(photoToDisplayList, startIndex);
    }

    /**
     * Moves all selected photos
     * @param selectedIndexes selected photos indexes
     */
    private void moveAll(Collection<Integer> selectedIndexes) {
        if (selectedIndexes == null)
            return;

        Path path = selectAlbum();

        if (path == null)
            return;

        for (Integer i : selectedIndexes)
            move(i, path);
    }

    /**
     * Moves one photo
     * @param index of photo to move
     * @param path the path where photo will be moved, if null the path is asked to the user
     */
    private void move(int index, @Nullable Path path) {
        if (index < 0 || index >= mCurrentPhotoList.size())
            return;

        if (path == null)
            path = selectAlbum();

        if (path == null)
            return;

        final Photo photo = mCurrentPhotoList.get(index);

        photo.copyTo(
                new File(path.toString(), Path.of(photo.getName()).toString()).toPath()
                , false,
                t -> {
                    if (t == null)
                        return;

                    CustomAlert al = new CustomAlert(Alert.AlertType.ERROR, getStage());
                    al.setTexts(
                            "Error occurred when deleting the photo",
                            t.getMessage()
                    );
                    al.showAndWait();
                }
        );

        remove(index);
    }

    /**
     * Asks the user to select an album in the tree
     * @return selected album
     */
    @Nullable
    private Path selectAlbum() {
        AlbumTreeSelectorDialog treeDial = new AlbumTreeSelectorDialog(
                mTreeView.getRootItem(),
                getStage()
        );
        Optional<String> result = treeDial.showAndWait();

        if (result.isEmpty())
            return null;
        else {
            Path path= mTreeView.getFullPath(result.get());
            if (path == null || !Files.exists(path))
                return null;

            return path;
        }
    }

    /**
     * Filters the current photo set
     */
    private void filter(Album album) {
        if (album == null)
            return;

        mCurrentPhotoList = album.executeQuery(PhotoQuery.newFilterQuery(mQueryBuilder.build()));
        mGridView.setPhotos(mCurrentPhotoList);
        order();
    }

    /**
     * Orders the photo list view according to selected sorting methods
     */
    private void order() {
        if (mCurrentPhotoList == null)
            return;

        LiveQueryResult liveQueryResult = (LiveQueryResult)mCurrentPhotoList;

        switch (mSortingMethod) {
            case DATE:
                liveQueryResult.changeOrdering(
                        PhotoQuery.SortableField.DATE,
                        PhotoQuery.SortDirection.ASCENDING
                );
                break;
            case NAME:
                liveQueryResult.changeOrdering(
                        PhotoQuery.SortableField.NAME,
                        PhotoQuery.SortDirection.ASCENDING
                );
                break;
        }
    }

    /*  FXML Methods    */

    /**
     * Opens the new album window
     */
    @FXML
    public void handleNewBtn(){
        OpenNewAlbumDialog dial = new OpenNewAlbumDialog(getStage());
        Optional<String> res = dial.showAndWait();

        if (res.isEmpty() || res.get().isBlank())
            return;

        mTreeView.open(res.get());
    }

    /**
     * Opens the help window
     */
    @FXML
    public void handleBtnHelp() {
        UiUtility.openDialogFromFxml(
                Settings.HELP_WINDOW_FILE,
                getStage(),
                Settings.HELP_WINDOW_NAME);
    }

    /**
     * Opens the setting window
     */
    @FXML
    public void handleBtnSettings(){
        UiUtility.openDialogFromFxml(
                Settings.SETTINGS_WINDOW_FILE,
                getStage(),
                Settings.SETTINGS_WINDOW_NAME
        );
    }

    /**
     * Opens the edit window
     */
    @FXML
    public void handleBtnEdit(){
        EditController c = (EditController)UiUtility.openDialogFromFxml(
                Settings.EDIT_WINDOW_FILE,
                getStage(),
                Settings.EDIT_WINDOW_NAME
        );

        c.setPhoto(mCurrentPhotoList.get(mSelectedPhotoIndex));
    }

    /**
     * Renames the photo file
     */
    @FXML
    public void handleRenameBtn() {
        if (mSelectedPhotoIndex == -1)
            return;

        Photo photo = mCurrentPhotoList.get(mSelectedPhotoIndex);

        TextInputDialog mDial = new CustomTextDialog(photo.getName(), getStage());
        mDial.setTitle("New file name");
        mDial.setHeaderText("New file name");
        mDial.setContentText("Please enter the new file name :");
        Optional<String> result = mDial.showAndWait();

        if (result.isEmpty() || result.get().isBlank())
            return;

        Photo.ChangeListener changeListener = getPhotoChangeListener();
        photo.addListener(changeListener);

        photo.rename(result.get(), false, t -> {
            photo.removeListener(changeListener);

            CustomAlert al = new CustomAlert(Alert.AlertType.ERROR, getStage());
            al.setTexts(
                    "Error occurred when renaming the file",
                    t.getMessage()
            );
            al.showAndWait();
        });

    }

    private Photo.ChangeListener getPhotoChangeListener() {
        return new Photo.ChangeListener() {
            @Override
            public void onNameChanged(Photo photo, String oldName, String newName) {
                updatePhotoInfo();
                photo.removeListener(this);
            }

            @Override
            public void onImageChanged(Photo photo) {

            }

            @Override
            public void onTagAdded(Photo photo, String tag) {

            }

            @Override
            public void onTagRemoved(Photo photo, String tag) {

            }

            @Override
            public void onRemoved(Photo photo) {
                setRightPanelToDefAndBlocked();
                photo.removeListener(this);
            }
        };
    }

    /**
     * Deletes the selected photo
     */
    @FXML
    public void handleDeleteBtn() {
        remove(mSelectedPhotoIndex);
    }

    /**
     * Moves the selected photo
     */
    @FXML
    public void handleMoveBtn() {
        move(mSelectedPhotoIndex, null);
    }

    /**
     * Opens the viewer widget with the corresponding photo
     */
    @FXML
    public void handleBtnOpen(){
        openAll(null);
    }

    /**
     * Hides or shows the left panel
     */
    @FXML
    public void handleRetractLeftPan(){
        changePanelState( 0, mRetractLeftPanBtn, mTitledLeftPanContainer, mTreeView);
    }

    /**
     * Hides or shows the right panel
     */
    @FXML
    public void handleRetractRightPan(){
        changePanelState(1, mRetractRightPanBtn, mTitledRightPanContainer, mRightPanContainer);
    }

    /*  Tags listeners  */

    /**
     * Adds a tag to the list of tags
     */
    @FXML
    public void handleAddTagBtn() {
        String newTag = mTagTxtField.getText();
        boolean isEdit = mIsEditTag;

        mTagTxtField.clear();
        mIsEditTag = false;

        if(newTag.isBlank())
            return;

        if(mTagsList.getItems().contains(newTag))
            return;

        if (mSelectedPhotoIndex == -1)
            return;

        newTag = newTag.replace(" ", "");
        newTag = newTag.toUpperCase();

        if(isEdit) {
            String selected = mTagsList.getSelectionModel().getSelectedItem();
            if(selected != null) {
                mTagsList.getItems().set(
                        mTagsList.getSelectionModel().getSelectedIndex(),
                        newTag
                );
                mCurrentPhotoList.get(mSelectedPhotoIndex).removeTag(selected);
            }
        } else {
            mTagsList.getItems().add(newTag);
        }

        mCurrentPhotoList.get(mSelectedPhotoIndex).addTag(newTag);

    }

    /**
     * Loads a tags in the text input which need to be modified by the user
     */
    @FXML
    public void handleEditTagBtn() {
        String selectedIndex = mTagsList.getSelectionModel().getSelectedItem();

        if(selectedIndex == null)
            return;

        mTagTxtField.setText(selectedIndex);
        mTagTxtField.requestFocus();

        mIsEditTag = true;
    }

    /**
     * Removes a tag from the list
     */
    @FXML
    public void handleRemoveTagBtn() {
        int selectedIndex = mTagsList.getSelectionModel().getSelectedIndex();
        String selectedTag = mTagsList.getSelectionModel().getSelectedItem();

        if(selectedIndex < 0)
            return;

        mTagsList.getItems().remove(selectedIndex);

        mCurrentPhotoList.get(mSelectedPhotoIndex).removeTag(selectedTag);
    }

    /**
     * Calls the AddTagBtn handler if the user press ENTER
     * @param e KeyEvent
     */
    @FXML
    public void handleTagTxtFiledKeyPressed(KeyEvent e) {
        if(e.getCode() == KeyCode.ENTER)
            handleAddTagBtn();
    }

    /**
     * Calls :
     *  * the RemoveTagBtn handler if the user press DELETE
     *  * the EditTagBtn handler if the user press ENTER
     * @param e KeyEvent
     */
    @FXML
    public void handleTagsListKeyPressed(KeyEvent e) {
        switch (e.getCode()) {
            case ENTER:
                handleEditTagBtn();
                break;
            case DELETE:
                handleRemoveTagBtn();
                break;
        }

    }

    /**
     * Removes all filters
     */
    @FXML
    public void handleRemoveFieldsSearchBtnClicked() {
        mFilterTxtField.clear();
        mQueryBuilder.removeDateFilter();
        mQueryBuilder.removeNameFilter();
        mQueryBuilder.removeTags();
        filter(mCurrentAlbum);
    }

    /**
     * Filters the displayed photos
     */
    @FXML
    public void handleFilterBtn() {
        String filter = mFilterTxtField.getText();

        if (filter.isBlank()) {
            switch (mSelectedFilteringMethod) {
                case NAME:
                    mQueryBuilder.removeNameFilter();
                    break;
                case DATE:
                    mQueryBuilder.removeDateFilter();
                    break;
                case TAGS:
                    mQueryBuilder.removeTags();
                    break;
            }
        } else {

            switch (mSelectedFilteringMethod) {
                case TAGS:
                    Set<String> tags = SortingParser.stringToTags(filter);

                    if (tags == null) {
                        CustomAlert al = new CustomAlert(
                                Alert.AlertType.ERROR,
                                getStage()
                        );

                        al.setTexts(
                                "Error when parsing tags",
                                "Please follow this structure : TAG1 TAG2 TAG3"
                        );
                        al.showAndWait();

                        mFilterTxtField.requestFocus();

                        return;
                    }

                    mQueryBuilder
                            .removeTags()
                            .filterTags(tags);

                    break;
                case DATE:
                    Date startDate = SortingParser.stringToStartDate(filter);
                    Date endDate = SortingParser.stringToEndDate(filter);

                    if (startDate == null)
                        startDate = new Date(0);

                    if (endDate == null)
                        endDate = new Date(Long.MAX_VALUE);

                    mQueryBuilder.filterDates(startDate, endDate);
                    mFilterTxtField.setText(SortingParser.datesToString(
                            startDate.getTime(),
                            endDate.getTime()
                    ));
                    break;
                case NAME:
                    mQueryBuilder.filterName(filter);
                    break;
            }
        }
        filter(mCurrentAlbum);
    }

    /**
     * Changes the sorting method
     */
    @FXML
    public void handleSortCombo() {
        String sortingString = mSortCombo.getSelectionModel().getSelectedItem();

        for (SortingMethod s : SortingMethod.values()) {
            if (s.equalsName(sortingString)) {
                mSortingMethod = s;
                break;
            }
        }

        order();
    }

    /**
     * Updates the current search fields
     */
    @FXML
    public void handleSearchFieldCombo() {
        String filteringString = mSearchFieldCombo.getSelectionModel().getSelectedItem();

        for (FilteringMethod s : FilteringMethod.values()) {
            if (s.equalsName(filteringString)) {
                mSelectedFilteringMethod = s;
                break;
            }
        }

        switch (mSelectedFilteringMethod) {
            case NAME:
                mFilterTxtField.setText(mQueryBuilder.getNamePrefix());
                break;
            case DATE:
                mFilterTxtField.setText(
                        SortingParser.datesToString(
                                mQueryBuilder.getStartDate(),
                                mQueryBuilder.getEndDate()
                        )
                );
                break;
            case TAGS:
                mFilterTxtField.setText(
                        SortingParser.tagsToString(
                                mQueryBuilder.getTags()
                        )
                );
                break;
        }
    }

    /**
     * Adds an album
     */
    @FXML
    public void handleAddAlbumBtn() {
        mTreeView.createSubAlbum();
    }
}
