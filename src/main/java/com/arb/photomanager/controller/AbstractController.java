package com.arb.photomanager.controller;

import com.arb.photomanager.model.db.AlbumDatabase;
import com.arb.photomanager.model.setting.Settings;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * Base class of all controllers of the PhotoManager app
 * @author Aymeric Robitaille
 */
public abstract class AbstractController {

    /*  Attributes  */
    AlbumDatabase mAlbumDataBase;
    static Settings mSettings;

    /**
     * Init the controller
     * <p>Call the initController() method</p>
     */
    final public void init()  {
        loadSettings();
        initController();
        getStage().setOnCloseRequest((WindowEvent we) -> close());
    }

    /**
     * Init the controller
     * <p>This method can be redefined by children classes</p>
     */
    protected void initController() { /* EMPTY */ }

    /**
     * Close properly the controller
     * <p>Call the closeController() method</p>
     */
    final public void close() {
        closeController();
        getStage().close();
    }

    /**
     * Close properly the controller
     * <p>This method can be redefined by children classes</p>
     */
    protected void closeController() { /* EMPTY */ }

    /**
     * Return the stage corresponding to the controller
     * @return the stage corresponding to the controller
     */
    public abstract Stage getStage();

    /**
     * Gives the actual application settings
     * @return the application settings
     */
    final public Settings getSettings() {
        return mSettings;
    }

    /**
     * Gives the controller's model
     * @return the controller's model
     */
    final AlbumDatabase getDataBase() {
        return mAlbumDataBase;
    }

    /**
     * Set the new model for the controller
     * @param model new model
     */
    final void setDataBase(AlbumDatabase model) {
        mAlbumDataBase = model;
    }

    /**
     * Loads the settings
     */
    private void loadSettings() {
        mSettings = Settings.getInstance();
    }

}
