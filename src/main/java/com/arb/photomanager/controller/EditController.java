package com.arb.photomanager.controller;

import com.arb.photomanager.PhotoManagerApp;
import com.arb.photomanager.controller.editparams.*;
import com.arb.photomanager.model.db.Photo;
import com.arb.photomanager.model.setting.Settings;
import com.arb.photomanager.model.transform.ImageFilter;
import com.arb.photomanager.model.transform.ImageTransform;
import com.arb.photomanager.model.transform.Rectangle;
import com.arb.photomanager.view.CustomAlert;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.*;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.Objects;


/**
 * Controller of the edit window
 * @author Aymeric Robitaille
 */
public class EditController extends AbstractController{
    private static final int HEXVIEW_BYTES_PER_LINE = 25;

    private enum Filter{
        BLACK_AND_WHITE("Black&White", "black_and_white_filter"),
        SEPIA("Sepia", "sepia_filter"),

        NEGATIVE_RGB("Negative RGB", "negative_rgb_filter"),
        NEGATIVE_COLOR("Negative Color","negative_filter"),

        STRONG_CONTRAST("Strong contrast", "strong_contrast_filter"),
        MEAN_CONTRAST("Mean contrast", "mean_contrast_filter"),
        LINEAR_CONTRAST("Linear contrast", "linear_contrast_filter"),
        ACCENTED_CONTRAST("Accented contrast", "accented_contrast_filter"),

        BRIGHTEN_FILTER("Brighten", "brighten_filter"),
        DARKEN("Darken", "darken_filter"),

        CROSSED("Crossed", "crossed_filter");

        private final String mFilterName;
        private final ImageFilter mFilter;

        Filter(String name, String fileName) {
            mFilterName = name;

            ImageFilter filter;

            try {
                Path path = Path.of(PhotoManagerApp.class
                                    .getResource("/filters/" + fileName + ".json")
                                    .toURI());

                filter = ImageFilter.deserialize(path);
            } catch (URISyntaxException | IOException e) {
                e.printStackTrace();
                filter = null;
            }

            mFilter = filter;
        }

        public String toString() {
            return mFilterName;
        }

        public ImageFilter getFilter() {
            return mFilter;
        }
    }

    /*  private attributes  */
    static final private int IMAGE_VIEW_OFFSET = 50;
    static final private double CANVAS_OPACITY = 0.5;
    static final private String SAVED_MSG = "Saved";
    static final private String SAVING_MSG = "Saving...";
    static final private String UNSAVED_MSG = "Unsaved";

    private Photo mPhoto;
    //private Image mOriginalImage;
    private Image mWorkingImage;

    private ImageTransform mImageTransform;

    private AbstractEditParam mEditParam;

    //Booleans
    private boolean mIsHexaView;

    //UI sizes
    static final private int WINDOW_WIDTH = 900;
    static final private int WINDOW_HEIGHT = 550;

    //UI strings
    private static final String VIEW_PHOTO_BTN_TXT = "View photo";
    private static final String VIEW_HEXA_BTN_TXT = "View hexa";


    /*  UI objects  */

    //Containers
    @FXML private BorderPane mMainContainer;
    @FXML private StackPane mViewContainer;
    @FXML private VBox mParamContainer;
    @FXML private HBox mToolsContainer;

    //Menu
    @FXML private MenuButton mFiltersMenu;

    //Buttons
    @FXML private Button mChangeViewBtn;
    @FXML private Button mMetaBtn;
    @FXML private Button mSaveBtn;

    //Views
    @FXML private ListView<String> mHexaView;
    @FXML private ImageView mImageView;
    @FXML private Canvas mCanvas;

    //Label
    @FXML private Label mSavedLbl;

    /*  change listener */
    final private Photo.ChangeListener mChangeListener = new Photo.ChangeListener() {
        @Override
        public void onNameChanged(Photo photo, String oldName, String newName) {

        }

        @Override
        public void onImageChanged(Photo photo) {
            mSavedLbl.setText(SAVED_MSG);
            photo.getImage(EditController.this::onImageLoaded);
        }

        @Override
        public void onTagAdded(Photo photo, String tag) {

        }

        @Override
        public void onTagRemoved(Photo photo, String tag) {

        }

        @Override
        public void onRemoved(Photo photo) {

        }
    };


    /*  Constructors    */

    //Redefinition of default constructor
    public EditController() {
        /*mIsHexaView = false;

        mWorkingImage = new WritableImage(200,200);
        mOriginalImage = null;
        mImageTransform = null;
        mPhoto = null;
        mEditParam = null;*/

    }

    /*  Methods redefinition    */

    /**
     * Initializes the controller by adding eventFilters on mouse pressed and on key pressed
     * <p>Is called by the init() method</p>
     */
    final protected void initController() {
        mHexaView.setCellFactory(param -> {
            ListCell<String> cell = new ListCell<>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(Objects.requireNonNullElse(item, ""));
                }
            };

            cell.setFont(Font.font("Courier New"));
            cell.setMouseTransparent(true);
            cell.setFocusTraversable(true);
            return cell;
        });

        adjustPhotoView();

        mViewContainer.widthProperty().addListener((obs, oldVal, newVal) -> adjustPhotoView());
        mViewContainer.heightProperty().addListener((obs, oldVal, newVal) -> adjustPhotoView());

        getStage().setMinHeight(WINDOW_HEIGHT);
        getStage().setMinWidth(WINDOW_WIDTH);

        initFiltersMenu();
    }

    /**
     * Close properly the controller
     * <p>This method can be redefined by children classes</p>
     */
    protected void closeController() {
        if (mPhoto != null)
            mPhoto.removeListener(mChangeListener);

//        saveRotation();

    }

    /**
     * Returns the stage corresponding to the controller
     * @return the stage corresponding to the controller
     */
    public Stage getStage() {
        return (Stage)mMainContainer.getScene().getWindow();
    }

    /**
     * Sets the photo to edit
     * @param photo photo to edit
     */
    public void setPhoto(Photo photo) {

        if (photo.hasMetadata())
            mMetaBtn.setDisable(false);
        else
            mMetaBtn.setDisable(true);

        this.mPhoto = photo;

        photo.addListener(mChangeListener);

        mPhoto.getImage(this::onImageLoaded);
    }

    /*  Private methods */

    private void initFiltersMenu() {
        for (Filter filter : Filter.values()) {
            final MenuItem menuItem = new MenuItem();

            menuItem.setText(filter.toString());
            menuItem.setOnAction(event -> loadFilter(filter));

            mFiltersMenu.getItems().add(menuItem);
        }
    }

    private void onImageLoaded(Image image, Throwable throwable) {
        if (throwable != null) {
            CustomAlert al = new CustomAlert(Alert.AlertType.ERROR, getStage());
            al.setTexts(
                    "Error occurred when loading the thumbnail",
                    throwable.getMessage()
            );
            al.showAndWait();
        } else {
            mToolsContainer.setDisable(false);
            mParamContainer.setDisable(false);
            //mOriginalImage = image;
            mWorkingImage = image;
            mImageTransform = getIdentityTransform();
            mImageView.setImage(mWorkingImage);

            adjustPhotoView();

            if (mEditParam != null)
                mEditParam.updateImage(mWorkingImage);
//                updateHexaView();
        }
    }

    private ImageTransform getIdentityTransform() {
        return new ImageTransform(
                new ImageFilter(new ColorAdjust()),
                new Rectangle(0, 0, (int)mWorkingImage.getWidth(), (int)mWorkingImage.getHeight()),
                0
        );
    }

    /**
     * Switches views between image view ands hexadecimal view
     */
    private void updateViewer() {
        if(mIsHexaView){
            mHexaView.setVisible(true);
            mImageView.setVisible(false);
            mCanvas.setVisible(false);
            updateHexaView();
        }else{
            mHexaView.setVisible(false);
            mImageView.setVisible(true);
            mCanvas.setVisible(true);
            mHexaView.setItems(null);
        }
    }

    /**
     * Adjusts the photo according to width, height and offset
     * @param width width to fit
     * @param height height to fit
     * @param offset internal offset to be kept
     */
    @SuppressWarnings("SameParameterValue")
    private void adjustPhotoView(double width, double height, double offset) {
        if(offset < 0)
            offset = 0;

        mImageView.setFitWidth(width - offset);
        mImageView.setFitHeight(height - offset);
        mCanvas.setHeight(height - offset);
        mCanvas.setWidth(width - offset);

        if (mEditParam != null)
            mEditParam.redraw();
    }

    /**
     * Adjusts the photo to its container size
     */
    private void adjustPhotoView(){
        adjustPhotoView(mViewContainer.getWidth(), mViewContainer.getHeight(), IMAGE_VIEW_OFFSET);
    }

    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static String bytesToHex(byte[] bytes, int offset, int length) {
        char[] hexChars = new char[length * 3];

        for (int j = 0; j < length; j++) {
            int v = bytes[offset + j] & 0xFF;
            hexChars[j * 3] = hexArray[v >>> 4];
            hexChars[j * 3 + 1] = hexArray[v & 0x0F];
            hexChars[j * 3 + 2] = ' ';
        }

        return new String(hexChars);
    }

    /**
     * Updates the hexadecimal view
     */
    private void updateHexaView() {
        ObservableList<String> hexaStrings = FXCollections.observableArrayList();
        hexaStrings.add("Loading...");

        mHexaView.setItems(hexaStrings);

        mPhoto.getImageBytes((bytes, throwable) -> {
            hexaStrings.clear();

            if (throwable != null) {
                CustomAlert al = new CustomAlert(Alert.AlertType.ERROR, getStage());
                al.setTexts(
                        "Error when getting bytes",
                        throwable.getMessage()
                );
                al.showAndWait();
                return;
            }

            int lineCount = bytes.length / HEXVIEW_BYTES_PER_LINE;

            for (int i = 0; i < lineCount; ++i)
                hexaStrings.add(bytesToHex(bytes, i * HEXVIEW_BYTES_PER_LINE, HEXVIEW_BYTES_PER_LINE));

            int remainingBytes = bytes.length % HEXVIEW_BYTES_PER_LINE;

            if (remainingBytes != 0)
                hexaStrings.add(bytesToHex(bytes, lineCount * HEXVIEW_BYTES_PER_LINE, remainingBytes));
        });

    }

    /**
     * Changes the save button and label state
     */
    private void changeSaveState(boolean isSaved) {
        if (!isSaved) {
            mSavedLbl.setText(UNSAVED_MSG);
            mSaveBtn.setDisable(false);
        } else {
            mSavedLbl.setText(SAVED_MSG);
            mSaveBtn.setDisable(true);
        }
    }

    /**
     * Saves the current image transformation
     */
    private void save() {

        changeSaveState(true);

        mSavedLbl.setText(SAVING_MSG);
        mParamContainer.setDisable(true);
        mToolsContainer.setDisable(true);

        if (mImageTransform == null || mPhoto == null)
            return;

        mPhoto.transformOverwrite(mImageTransform, t -> {
            if (t == null)
                return;

            CustomAlert al = new CustomAlert(Alert.AlertType.ERROR, getStage());
            al.setTexts(
                    "Error when saving the photo",
                    t.getMessage()
            );

            al.showAndWait();
        });

        mImageTransform = null;
    }

    private void initParams() {
        if (mEditParam == null)
            return;

        //mWorkingImage = ImageUtil.copyImage(mOriginalImage);
        //mImageView.setImage(mWorkingImage);

        mCanvas.setVisible(true);
        mCanvas.setMouseTransparent(false);
        mCanvas.setOpacity(CANVAS_OPACITY);

        mParamContainer.getChildren().clear();
        mEditParam.setOnSaveChange(this::changeSaveState);
        mEditParam.init();
        mParamContainer.getChildren().add(mEditParam.getWidget());
    }

    private void onError(String errorType, Exception e) {
        if (e == null)
            return;

        CustomAlert al = new CustomAlert(Alert.AlertType.ERROR, getStage());
        al.setTexts(
                "Error : " + errorType,
                e.getMessage()
        );
        al.showAndWait();
    }

    private void newParam(AbstractEditParam param) {
        if (mEditParam != null)
            mEditParam.reset(new ActionEvent());

        mEditParam = param;

        mEditParam.isWorkingProperty().addListener((observable, oldValue, newValue) -> {
            mParamContainer.setDisable(newValue);
            mToolsContainer.setDisable(newValue);
            mSaveBtn.setDisable(newValue);
        });

        initParams();
    }

    /**
     * Loads the specified filter param widget
     * @param filter the filter to load
     */
    private void loadFilter(Filter filter) {
        try {

            newParam(
                    new EditFilterParam(
                            getIdentityTransform(),
                            mImageView,
                            mCanvas,
                            filter.getFilter()
                    )
            );


        } catch (Exception e) {
            onError("cannot load filter params", e);
        }
    }

    /*  FXML methods    */

    /**
     * Initializes the view
     */
    @FXML
    public void initialize() {
        updateViewer();
        mImageView.setPreserveRatio(true);
    }

    /**
     * Switches the view between image and hexadecimal views
     */
    @FXML
    public void handleChangeViewBtn() {
        mIsHexaView = !mIsHexaView;

        if(mIsHexaView)
            mChangeViewBtn.setText(VIEW_PHOTO_BTN_TXT);
        else
            mChangeViewBtn.setText(VIEW_HEXA_BTN_TXT);

        updateViewer();
    }

    /**
     * Closes the edit window
     * <p>Does not save the image before exiting</p>
     */
    @FXML
    public void handleBtnClose(){
        close();
    }

    /**
     * Opens the metadata window
     */
    @FXML
    public void handleBtnMeta() {
        MetaController c = (MetaController)UiUtility.openDialogFromFxml(
                Settings.META_WINDOW_FILE,
                getStage(),
                Settings.META_WINDOW_NAME
        );

        c.setData(mPhoto);
    }

    /**
     * Saves the photo by applying definitively the filter
     */
    @FXML
    public void handleSaveBtn() {
        if (mEditParam != null)
            mImageTransform = mEditParam.getImageTransformAndSave();

        save();

    }

    @FXML
    public void handleCanvasClick(MouseEvent e) {

        if (mEditParam == null)
            return;

        mEditParam.handleCanvasClick(e);
    }

    @FXML
    public void handleCanvasMouseMove(MouseEvent e) {

        if (mEditParam == null)
            return;

        mEditParam.handleCanvasMouseMove(e);

    }

    /*  Transformation button handlers   */

    /**
     * Load rotation filters
     */
    @FXML
    public void handleRotate() {
        try {
            newParam(
                    new EditRotateParam(
                            getIdentityTransform(),
                            mImageView,
                            mCanvas
                    )
            );
        } catch (Exception e) {
            onError("cannot load Rotation params", e);
        }
    }

    /**
     * Cropping functionality
     */
    @FXML
    public void handleCrop() {
        try {

            newParam(
                    new EditCropParam(
                            getIdentityTransform(),
                            mImageView,
                            mCanvas
                    )
            );

        } catch (IOException e) {
            onError("cannot load Crop params", e);
        }
    }

    /**
     * Adjustments (brightness, hue, contrast, saturation) functionality
     */
    @FXML
    public void handleAdjustments() {
        try {

            newParam(
                    new EditAdjustmentsParam(
                            getIdentityTransform(),
                            mImageView,
                            mCanvas
                    )
            );

        } catch (IOException e) {
            onError("cannot load adjustments params", e);
        }
    }

    /**
     * RGB adjustments functionality
     */
    @FXML
    public void handleRGB() {
        try {

            newParam(
                    new EditRGBParam(
                            getIdentityTransform(),
                            mImageView,
                            mCanvas
                    )
            );

        } catch (Exception e) {
            onError("cannot load RGB params", e);
        }
    }

}
