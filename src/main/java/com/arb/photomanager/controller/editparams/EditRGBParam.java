package com.arb.photomanager.controller.editparams;

import com.arb.photomanager.PhotoManagerApp;
import com.arb.photomanager.model.transform.ImageFilter;
import com.arb.photomanager.model.transform.ImageTransform;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Slider;
import javafx.scene.image.ImageView;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 * @author Aymeric Robitaille
 */
public class EditRGBParam extends AbstractEditFilteredParam {

    static final private String RGB_FILTER_PATH = "/filters/rgb_filter.json";

    static final private String RED_STR = "Red";
    static final private String GREEN_STR = "Green";
    static final private String BLUE_STR = "Blue";

    public EditRGBParam(
            ImageTransform imageTransform,
            ImageView imageView,
            Canvas canvas
    ) throws IOException, URISyntaxException {
        super(
                imageTransform,
                imageView,
                canvas,
                List.of(RED_STR, GREEN_STR, BLUE_STR),
                0.0, 255.0, 128.0);

        Path path = Path.of(PhotoManagerApp.class.getResource(RGB_FILTER_PATH).toURI());
        mBuilder.setInputFilter(ImageFilter.deserialize(path));
    }

    protected void initSliderListeners(Map<String, Slider> params) {
        /*  Empty   */
    }

    protected void onApply() {
        /*  Empty   */
    }

    protected ImageFilter build() {

        final double[] x = {0.0, 128.0, 255.0};
        double[] y = {0.0, 128.0, 255.0};

        EnumMap<ImageFilter.ColorChannel, Slider> relationMap = new EnumMap<>(ImageFilter.ColorChannel.class);

        relationMap.put(ImageFilter.ColorChannel.RED, mSliders.get(RED_STR));
        relationMap.put(ImageFilter.ColorChannel.GREEN, mSliders.get(GREEN_STR));
        relationMap.put(ImageFilter.ColorChannel.BLUE, mSliders.get(BLUE_STR));

        for (ImageFilter.ColorChannel channel : ImageFilter.ColorChannel.values()) {
            y[1] = relationMap.get(channel).getValue();
            mBuilder.setColorCurve(channel, x, Arrays.copyOf(y, 3));
        }

        return mBuilder.build();
    }
}
