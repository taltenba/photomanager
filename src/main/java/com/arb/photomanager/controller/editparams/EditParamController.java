package com.arb.photomanager.controller.editparams;

import com.arb.photomanager.controller.AbstractController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.function.Consumer;

/**
 * @author Aymeric Robitaille
 */
public class EditParamController extends AbstractController {
    private Consumer<ActionEvent> mResetCallback;
    private Consumer<ActionEvent> mApplyCallback;

    @FXML private Button mApplyBtn;
    @FXML private VBox mParamContainer;
    @FXML private VBox mMainContainer;

    public EditParamController() {
        mResetCallback = null;
        mApplyCallback = null;
    }

    /**
     * Return the stage corresponding to the controller
     * @return the stage corresponding to the controller
     */
    public Stage getStage() {
        return (Stage)mApplyBtn.getScene().getWindow();
    }

    void setResetCallback(Consumer<ActionEvent> resetCallback) {
        this.mResetCallback = resetCallback;
    }

    void setApplyCallback(Consumer<ActionEvent> applyCallback) {
        this.mApplyCallback = applyCallback;
    }

    VBox getParamContainer() {
        return mParamContainer;
    }

    VBox getMainContainer() {
        return mMainContainer;
    }

    @FXML
    public void handleResetBtn(ActionEvent e) {
        if (mResetCallback == null)
            return;

        mResetCallback.accept(e);
    }

    @FXML
    public void handleApplyBtn(ActionEvent e) {
        if (mApplyCallback == null)
            return;

        mApplyCallback.accept(e);
    }
}
