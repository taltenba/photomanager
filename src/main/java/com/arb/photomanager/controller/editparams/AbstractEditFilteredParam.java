package com.arb.photomanager.controller.editparams;

import com.arb.photomanager.model.transform.ImageFilter;
import com.arb.photomanager.model.transform.ImageTransform;
import com.arb.photomanager.view.CustomAlert;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Aymeric Robitaille
 */
abstract public class AbstractEditFilteredParam extends AbstractEditParam {

    private final double MIN_VALUE;
    private final double MAX_VALUE;
    private final double DEF_VALUE;

    final Map<String, Slider> mSliders;
    final ImageFilter.Builder mBuilder;

    AbstractEditFilteredParam(
            ImageTransform imageTransform,
            ImageView imageView,
            Canvas canvas,
            Collection<String> params,
            double min, double max, double def
    ) throws IOException {
        super(
                imageTransform,
                imageView,
                canvas
        );

        MIN_VALUE = min;
        MAX_VALUE = max;
        DEF_VALUE = def;

        mBuilder = new ImageFilter.Builder();
        mBuilder.setInputFilter(imageTransform.getImageFilter());

        mSliders = new HashMap<>();

        initUI(params);
        initSliderListeners(mSliders);
    }

    abstract protected void initSliderListeners(Map<String, Slider> params);

    abstract protected void onApply();

    abstract protected ImageFilter build();

    protected void handleReset(ActionEvent e) {

        resetSliders();

        mImageView.setImage(
                mOriginalImage
        );
    }

    protected void handleApply(@Nullable ActionEvent e) {
        if (mImageView == null)
            return;

        notifyBackgroundWorkStarted();

        Thread t = new Thread(new FilteringTask(build()));
        t.setDaemon(true);
        t.start();
    }

    private class FilteringTask extends Task<Image> {
        private final ImageFilter mImageFilter;

        private FilteringTask(ImageFilter filter) {
            mImageFilter = filter;
        }

        @Override
        protected Image call() throws Exception {
            return mImageFilter.filter(mOriginalImage);
        }

        @Override
        protected void succeeded() {
            mImageView.setImage(getValue());
            onApply();
            notifyBackgroundWorkCompleted();
        }

        @Override
        protected void failed() {
            CustomAlert al = new CustomAlert(Alert.AlertType.ERROR, (Stage)mImageView.getScene().getWindow());
            al.setTexts(
                    "Error when applying filter",
                    getException().getMessage()
            );
            al.showAndWait();

            notifyBackgroundWorkCompleted();
        }
    }

    protected void transform() {
        ImageTransform oldImageTransform = getImageTransform();

        ImageTransform newImageTransform = new ImageTransform(
                build(),
                getImageTransform().getRectangle(),
                oldImageTransform.getRotation()
        );

        setImageTransform(newImageTransform);

        mOriginalImage = mImageView.getImage();
    }

    public void init() {
        if (mCanvas == null)
            return;

        mCanvas.getGraphicsContext2D().clearRect(
                0,
                0,
                mCanvas.getWidth(),
                mCanvas.getHeight()
        );

        mOriginalImage = mImageView.getImage();
    }

    public void handleCanvasMouseMove(MouseEvent e) {
        /*  Empty   */
    }

    public void handleCanvasClick(MouseEvent e) {
        /*  Empty   */
    }

    public void redraw() {
        /*  Empty   */
    }

    private void initUI(Collection<String> params) {
        VBox mainContainer = mParamContainer;

        GridPane gridPane;
        HBox leftHBox;
        HBox rightHBox;
        Slider slider;

        ColumnConstraints leftColumnConstraints;
        ColumnConstraints rightColumnConstraints;

        mainContainer.getChildren().clear();
        mainContainer.setPadding(new Insets(10, 10, 10, 10));
        mainContainer.setSpacing(10);

        for (String param : params) {
            final Label counterLbl = new Label(String.valueOf(DEF_VALUE));

            slider = new Slider();
            slider.setMin(MIN_VALUE);
            slider.setMax(MAX_VALUE);
            slider.valueProperty().addListener((observable, oldValue, newValue) ->
                    counterLbl.setText(String.format("%.2f", newValue.doubleValue()))
            );
            slider.setValue(DEF_VALUE);


            leftHBox = new HBox();
            leftHBox.setAlignment(Pos.CENTER_LEFT);
            leftHBox.getChildren().add(new Label(param));

            rightHBox = new HBox();
            rightHBox.setAlignment(Pos.CENTER_RIGHT);
            HBox.setHgrow(rightHBox, Priority.ALWAYS);
            rightHBox.getChildren().add(counterLbl);

            rightColumnConstraints = new ColumnConstraints();
            rightColumnConstraints.setPercentWidth(50);
            rightColumnConstraints.setFillWidth(true);

            leftColumnConstraints = new ColumnConstraints();
            leftColumnConstraints.setPercentWidth(50);
            leftColumnConstraints.setFillWidth(true);

            gridPane = new GridPane();
            gridPane.setMaxWidth(Double.MAX_VALUE);
            gridPane.getColumnConstraints().addAll(
                    leftColumnConstraints,
                    rightColumnConstraints
            );
            gridPane.add(leftHBox, 0, 0);
            gridPane.add(rightHBox, 1, 0);


            mainContainer.getChildren().addAll(gridPane, slider);

            mSliders.put(param, slider);
        }
    }

    private void resetSliders() {
        for (Slider slider : mSliders.values())
            slider.setValue(DEF_VALUE);
    }

}
