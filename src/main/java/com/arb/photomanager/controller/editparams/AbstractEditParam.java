package com.arb.photomanager.controller.editparams;

import com.arb.photomanager.PhotoManagerApp;
import com.arb.photomanager.model.setting.Settings;
import com.arb.photomanager.model.transform.ImageTransform;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * @author Aymeric Robitaille
 */
public abstract class AbstractEditParam {
    private ImageTransform mImageTransform;
    private Consumer<Boolean> mOnSaveChange;

    final protected ImageView mImageView;
    final Canvas mCanvas;
    final VBox mParamContainer;
    final private VBox mContainer;

    Image mOriginalImage;

    private final ReadOnlyBooleanWrapper mIsWorking;

    AbstractEditParam(
            ImageTransform imageTransform,
            ImageView imageView,
            Canvas canvas
    ) throws IOException {
        Objects.requireNonNull(imageTransform);
        Objects.requireNonNull(imageView);
        Objects.requireNonNull(canvas);

        mIsWorking = new ReadOnlyBooleanWrapper(false);

        mImageView = imageView;
        mCanvas = canvas;
        mImageTransform = imageTransform;
        mOriginalImage = mImageView.getImage();

        mOnSaveChange = null;

        mContainer = new VBox();
        VBox.setVgrow(mContainer, Priority.ALWAYS);

        FXMLLoader loader = new FXMLLoader(PhotoManagerApp.class.getResource(Settings.EDIT_PARAM_FILE));
        Parent root = loader.load();
        mContainer.getChildren().clear();
        mContainer.getChildren().add(root);
        EditParamController mController = loader.getController();
        mController.setResetCallback(this::reset);
        mController.setApplyCallback(this::apply);
        mParamContainer = mController.getParamContainer();
        VBox.setVgrow(mController.getMainContainer(), Priority.ALWAYS);
    }

    public void reset(ActionEvent e) {
        handleReset(e);

        if (mOnSaveChange != null)
            mOnSaveChange.accept(true);
    }

    public void apply(ActionEvent e) {
        handleApply(e);

        if (mOnSaveChange != null)
            mOnSaveChange.accept(false);
    }

    public final ReadOnlyBooleanProperty isWorkingProperty() {
        return mIsWorking.getReadOnlyProperty();
    }

    protected final void notifyBackgroundWorkStarted() {
        mIsWorking.setValue(true);
    }

    protected final void notifyBackgroundWorkCompleted() {
        mIsWorking.setValue(false);
    }

    abstract protected void transform();

    abstract protected void handleReset(ActionEvent e);

    abstract protected void handleApply(ActionEvent e);

    abstract public void init();

    abstract public void handleCanvasMouseMove(MouseEvent e);

    abstract public void handleCanvasClick(MouseEvent e);

    abstract public void redraw();

    final ImageTransform getImageTransform() {
        return mImageTransform;
    }

    final public ImageTransform getImageTransformAndSave() {
        transform();

        if (mOnSaveChange != null)
            mOnSaveChange.accept(false);

        return mImageTransform;
    }

    final void setImageTransform(ImageTransform imageTransform) {
        this.mImageTransform = imageTransform;
    }

    final public VBox getWidget() {
        return mContainer;
    }

    final public void updateImage(Image image) {
        mOriginalImage = image;
        reset(new ActionEvent());
    }

    final public void setOnSaveChange(Consumer<Boolean> onChange) {
        this.mOnSaveChange = onChange;
    }
}
