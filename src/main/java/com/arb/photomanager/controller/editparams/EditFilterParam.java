package com.arb.photomanager.controller.editparams;

import com.arb.photomanager.model.transform.ImageFilter;
import com.arb.photomanager.model.transform.ImageTransform;
import javafx.event.ActionEvent;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Slider;
import javafx.scene.image.ImageView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public class EditFilterParam extends AbstractEditFilteredParam{
    public EditFilterParam(
            ImageTransform imageTransform,
            ImageView imageView,
            Canvas canvas,
            ImageFilter filter
    ) throws IOException {
        super(
                imageTransform,
                imageView,
                canvas,
                new ArrayList<>(),
                0.0, 0.0, 0.0);

        mBuilder.setInputFilter(filter);
    }

    public void init() {
        super.init();
        apply(new ActionEvent());
    }

    protected void initSliderListeners(Map<String, Slider> params) {
        /*  Empty   */
    }

    protected void onApply() {
        /*  Empty   */
    }

    protected ImageFilter build() {
        return mBuilder.build();
    }
}
