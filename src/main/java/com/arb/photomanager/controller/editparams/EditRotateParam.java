package com.arb.photomanager.controller.editparams;

import com.arb.photomanager.model.transform.ImageFilter;
import com.arb.photomanager.model.transform.ImageTransform;
import com.arb.photomanager.model.transform.Rectangle;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class EditRotateParam extends AbstractEditFilteredParam {

    static final private String ROTATION_STR = "Rotation angle";
    static final private double MIN_VALUE = 0.0;
    static final private double MAX_VALUE = 360.0;
    static final private double DEF_VALUE = 0.0;

    private double mOldRotation;

    public EditRotateParam(
            ImageTransform imageTransform,
            ImageView imageView,
            Canvas canvas
    ) throws IOException {
        super(
                imageTransform,
                imageView,
                canvas,
                List.of(ROTATION_STR),
                MIN_VALUE, MAX_VALUE, DEF_VALUE
        );

        mOldRotation = 0;
    }

    protected void initSliderListeners(Map<String, Slider> params) {
        final Slider slider = params.get(ROTATION_STR);

        if (slider == null)
            return;

        slider.valueProperty().addListener( (observable, oldValue, newValue) -> {
            int effectiveVal = newValue.intValue();
            int remaining = effectiveVal % 90;

            if (remaining < 45) {
                effectiveVal -= remaining;
            } else {
                effectiveVal += 90 - remaining;
            }

            slider.setValue(effectiveVal);
            mImageView.setRotate(effectiveVal - mOldRotation);
        });

        HBox hBox = new HBox();
        Button clockWiseBtn = new Button("+90"+ (char)186);
        Button counterClockWiseBtn = new Button("-90"+ (char)186);

        HBox.setHgrow(clockWiseBtn, Priority.ALWAYS);
        HBox.setHgrow(counterClockWiseBtn, Priority.ALWAYS);

        clockWiseBtn.setMaxWidth(Double.MAX_VALUE);
        counterClockWiseBtn.setMaxWidth(Double.MAX_VALUE);

        clockWiseBtn.setOnAction(event -> updateRotation(90.0, slider));
        counterClockWiseBtn.setOnAction(event -> updateRotation(-90.0, slider));

        hBox.setSpacing(10);
        hBox.getChildren().addAll(counterClockWiseBtn, clockWiseBtn);

        mParamContainer.getChildren().add(hBox);
    }

    private void updateRotation(double addVal, Slider slider) {
        double actualVal = slider.getValue();
        double newVal = actualVal + addVal;

        if (newVal < 0)
            newVal = MAX_VALUE + newVal;

        newVal %= MAX_VALUE;

        slider.setValue(newVal);
    }

    protected void onApply() {
        /*  Empty   */
    }

    protected ImageFilter build() {
        ImageTransform oldImageTransform = getImageTransform();

        ImageTransform newImageTransform = new ImageTransform(
                oldImageTransform.getImageFilter(),
                new Rectangle(0, 0, (int)mOriginalImage.getWidth(), (int)mOriginalImage.getHeight()),
                (int)mSliders.get(ROTATION_STR).getValue()
        );

        setImageTransform(newImageTransform);

        return mBuilder.build();
    }
}
