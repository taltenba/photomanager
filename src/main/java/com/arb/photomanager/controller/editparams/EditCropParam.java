package com.arb.photomanager.controller.editparams;

import com.arb.photomanager.model.transform.ImageTransform;
import com.arb.photomanager.model.transform.Rectangle;
import javafx.event.ActionEvent;
import javafx.geometry.Rectangle2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.io.IOException;

/**
 * @author Aymeric Robitaille
 */
final public class EditCropParam extends AbstractEditParam {

    static final private Color CANVAS_BG_COLOR = Color.BLACK;
    static final private Color CANVAS_SELECTED_COLOR = Color.GREEN;

    private double mStartX;
    private double mStartY;

    private Rectangle mRectangle;

    public EditCropParam(
            ImageTransform imageTransform,
            ImageView imageView,
            Canvas canvas
    ) throws IOException {
        super(
                imageTransform,
                imageView,
                canvas
        );

        mParamContainer.setDisable(true);

        mRectangle = new Rectangle(
                0,
                0,
                (int)imageView.getImage().getWidth(),
                (int)imageView.getImage().getHeight()
        );
    }

    public void init() {
        GraphicsContext gc = mCanvas.getGraphicsContext2D();
        fill(gc);
    }

    protected void transform() {
        ImageTransform oldImageTransform = getImageTransform();

        ImageTransform newImageTransform = new ImageTransform(
                oldImageTransform.getImageFilter(),
                mRectangle,
                oldImageTransform.getRotation()
        );

        setImageTransform(newImageTransform);
    }

    private void clear() {
        mCanvas.getGraphicsContext2D().clearRect(
                0,
                0,
                mCanvas.getWidth(),
                mCanvas.getHeight()
        );
    }

    protected void handleApply(ActionEvent e) {
        if (mRectangle == null)
            return;

        mImageView.setViewport(new Rectangle2D(
                mRectangle.getX(),
                mRectangle.getY(),
                mRectangle.getWidth(),
                mRectangle.getHeight()
        ));

        clear();
    }

    protected void handleReset(ActionEvent e) {
        Image img = mImageView.getImage();
        mImageView.setViewport(new Rectangle2D(0, 0, img.getWidth(), img.getHeight()));

        setRectangle(0, 0, img.getWidth(), img.getHeight());

        clear();
    }

    public void redraw() {
        Rectangle2D viewport = mImageView.getViewport();

        if (
                viewport == null || mRectangle == null || !(
                        (int)viewport.getMinX() == mRectangle.getX() &&
                                (int)viewport.getMinY() == mRectangle.getY() &&
                                (int)viewport.getHeight() == mRectangle.getHeight() &&
                                (int)viewport.getWidth() == mRectangle.getWidth()
                )
        ) {
            fill(mCanvas.getGraphicsContext2D());
        } else {
            clear();
        }
    }

    private double fitToBorder(double val, double border, double length) {
        if (val <= border)
            return 0;

        if (val >= length - border)
            return length - border;

        return val - border;
    }

    private void setRectangle(double startX, double startY, double endX, double endY) {
        Image img = mImageView.getImage();

        double sX;
        double sY;
        double eX;
        double eY;

        double hZoom = img.getHeight() / mImageView.getFitHeight();
        double wZoom = img.getWidth() / mImageView.getFitWidth();
        double zoom;

        if (hZoom > wZoom)
            zoom = hZoom;
        else
            zoom = wZoom;

        double wBorder = (mImageView.getFitWidth() - img.getWidth() / zoom) / 2;
        double hBorder = (mImageView.getFitHeight() - img.getHeight() / zoom) / 2;

        startX = fitToBorder(startX, wBorder, mImageView.getFitWidth());
        endX = fitToBorder(endX, wBorder, mImageView.getFitWidth());

        startY = fitToBorder(startY, hBorder, mImageView.getFitHeight());
        endY = fitToBorder(endY, hBorder, mImageView.getFitHeight());


        if (startX < endX) {
            sX = startX;
            eX = endX;
        } else {
            sX = endX;
            eX = startX;
        }

        if (startY < endY) {
            sY = startY;
            eY = endY;
        } else {
            sY = endY;
            eY = startY;
        }

        sX = sX*zoom;
        eX = eX*zoom;
        sY = sY*zoom;
        eY = eY*zoom;

        int w = (int)(eX - sX);
        int h = (int)(eY - sY);

        if (w < 0)
            w = 0;
        if (h < 0)
            h = 0;

        mRectangle = new Rectangle(
                (int)sX,
                (int)sY,
                w,
                h
        );
    }

    public void handleCanvasMouseMove(MouseEvent e) {
        GraphicsContext gc = mCanvas.getGraphicsContext2D();

        fill(gc);

        gc.setFill(CANVAS_SELECTED_COLOR);
        gc.fillPolygon(new double[]{mStartX, e.getX(), e.getX(), mStartX},
                new double[]{mStartY, mStartY, e.getY(), e.getY()}, 4);

        setRectangle(mStartX, mStartY, e.getX(), e.getY());
    }

    public void handleCanvasClick(MouseEvent e) {
        mStartX = e.getX();
        mStartY = e.getY();
    }

    private void fill(GraphicsContext gc) {
        double w = mImageView.getFitWidth();
        double h = mImageView.getFitHeight();

        gc.setFill(CANVAS_BG_COLOR);
        gc.setStroke(Color.TRANSPARENT);

        gc.fillPolygon(new double[]{0, w, w, 0},
                new double[]{0, 0, h, h}, 4);
    }
}
