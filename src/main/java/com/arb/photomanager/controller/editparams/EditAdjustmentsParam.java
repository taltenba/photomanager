package com.arb.photomanager.controller.editparams;

import com.arb.photomanager.model.transform.ImageFilter;
import com.arb.photomanager.model.transform.ImageTransform;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Slider;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.ImageView;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author Aymeric Robitaille
 */
public class EditAdjustmentsParam extends AbstractEditFilteredParam {

    static final private String HUE_STR = "Hue";
    static final private String SATURATION_STR = "Saturation";
    static final private String BRIGHTNESS_STR = "Brightness";
    static final private String CONTRAST_STR = "Contrast";

    public EditAdjustmentsParam(
            ImageTransform imageTransform,
            ImageView imageView,
            Canvas canvas
    ) throws IOException {
        super(
                imageTransform,
                imageView,
                canvas,
                List.of(HUE_STR, SATURATION_STR, BRIGHTNESS_STR, CONTRAST_STR),
                -1, 1, 0);
    }

    protected void initSliderListeners(Map<String, Slider> params) {
        for (Slider slider : params.values()) {
            slider.valueProperty().addListener((observable, oldValue, newValue) -> {
                        mImageView.setImage(mOriginalImage);
                        mImageView.setEffect(getColorAdjust());
                    }
            );
        }
    }
    protected void onApply() {
        mImageView.setEffect(
                new ColorAdjust()
        );
    }


    private ColorAdjust getColorAdjust() {
        return new ColorAdjust(
                mSliders.get(HUE_STR).getValue(),
                mSliders.get(SATURATION_STR).getValue(),
                mSliders.get(BRIGHTNESS_STR).getValue(),
                mSliders.get(CONTRAST_STR).getValue()
        );
    }

    protected ImageFilter build() {
        mBuilder.setColorAdjust(getColorAdjust());

        return mBuilder.build();
    }
}
