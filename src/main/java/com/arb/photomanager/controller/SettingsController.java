package com.arb.photomanager.controller;

import com.arb.photomanager.model.setting.Setting;
import com.arb.photomanager.model.setting.Settings;
import com.arb.photomanager.view.CustomAlert;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Pair;

/**
 *
 * @author Aymeric Robitaille
 */
public class SettingsController extends AbstractController {

    /*  Private attributes  */

    /*  UI Controls */

    //UI sizes
    static final private int WINDOW_WIDTH = 600;
    static final private int WINDOW_HEIGHT = 450;

    //Buttons
    @FXML private Button mOkBtn;

    //ComboBoxes
    @FXML private ComboBox<String> mUiThemeCombo;

    //Spinners
    @FXML private Spinner<Integer> mOverlayDelaySpinner;
    @FXML private Spinner<Integer> mThumbnailWidth;
    @FXML private Spinner<Integer> mThumbnailHeight;


    /*  Methods redefinition    */

    /**
     * Initializes the controller by adding eventFilters on mouse pressed and on key pressed
     * <p>Is called by the init() method</p>
     */
    final protected void initController(){
        setData();
        getStage().setMinHeight(WINDOW_HEIGHT);
        getStage().setMinWidth(WINDOW_WIDTH);
    }

    /**
     * Sets the data view and factories
     */
    private void setData() {
        //UI Theme comboBox
        setUiThemeData();

        //Thumbnail size
        setSpinnerData(
                mThumbnailWidth,
                mSettings.get(Settings.EditableSetting.THUMBNAIL_WIDTH)
        );

        setSpinnerData(
                mThumbnailHeight,
                mSettings.get(Settings.EditableSetting.THUMBNAIL_HEIGHT)
        );

        //Overlay delay
        setSpinnerData(
                mOverlayDelaySpinner,
                mSettings.get(Settings.EditableSetting.OVERLAY_DELAY)
        );
    }

    /**
     * Sets the data view and factories for the UI Theme comboBox
     */
    private void setUiThemeData() {
        Setting s = mSettings.get(Settings.EditableSetting.UI_THEME);
        mUiThemeCombo.getItems().addAll(
                Setting.toStringCollection(
                        s.getAllowedValues()
                )
        );

        mUiThemeCombo.getSelectionModel().select(
                (String)s.get()
        );
    }

    /**
     * Sets the data view and factories for the given spinner
     * <p>Considers that the allowed values are bounded</p>
     * @param spin the Integer spin to configure
     * @param s the setting that must be controlled by the spinner
     */
    private void setSpinnerData(Spinner<Integer> spin, Setting s) {

        Pair<Integer, Integer> bounds = Setting.toBounds(
                s.getAllowedValues()
        );

        SpinnerValueFactory<Integer> svf = new SpinnerValueFactory.IntegerSpinnerValueFactory(
                bounds.getKey(),
                bounds.getValue()
        );

        spin.setValueFactory(svf);
        spin.getValueFactory().setValue(
                (int)s.get()
        );
    }

    /**
     * Gives the stage of the dialog
     * @return the stage of the dialog
     */
    public Stage getStage() {
        return (Stage)mOkBtn.getScene().getWindow();
    }

    /**
     * Tells if the settings are modified, if yes update the database
     * @return true or false
     */
    private boolean modifiedWithUpdate() {
        boolean isModified = false;

        if(!mUiThemeCombo.getValue().equals(mSettings.getUITheme())) {
            isModified = true;
            mSettings.set(Settings.EditableSetting.UI_THEME, mUiThemeCombo.getValue());
        }

        if(!mThumbnailWidth.getValue().equals(mSettings.getThumbnailWidth())) {
            isModified = true;
            mSettings.set(Settings.EditableSetting.THUMBNAIL_WIDTH, mThumbnailWidth.getValue());
        }

        if(!mThumbnailHeight.getValue().equals(mSettings.getThumbnailHeight())) {
            isModified = true;
            mSettings.set(Settings.EditableSetting.THUMBNAIL_HEIGHT, mThumbnailHeight.getValue());
        }

        if(!mOverlayDelaySpinner.getValue().equals(mSettings.getOverlayDelay())) {
            isModified = true;
            mSettings.set(Settings.EditableSetting.OVERLAY_DELAY, mOverlayDelaySpinner.getValue());
        }

        return isModified;
    }


    /**
     * Saves settings and closes the dialog
     */
    @FXML
    public void handleBtnOk(){
        if(modifiedWithUpdate()) {
            CustomAlert al = new CustomAlert(Alert.AlertType.INFORMATION, getStage());
            al.setTexts(
                    "Reboot information",
                    "The settings are saved but you need to close and open the application to see the changes."
            );

            al.showAndWait();
        }

        close();
    }

    /**
     * Closes the dialog
     */
    @FXML
    public void handleBtnCancel(){
        close();
    }
}
