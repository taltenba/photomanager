package com.arb.photomanager.controller;

import com.arb.photomanager.model.db.Photo;
import com.arb.photomanager.model.setting.Settings;
import com.arb.photomanager.view.CustomAlert;
import com.arb.photomanager.view.SlideShowSettingsDialog;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.List;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Controller of the photo_viewer_widget view
 * @author Aymeric Robitaille
 */
public class PhotoViewerWidgetController extends AbstractController {

    /*  Private attributes  */

    //Timers
    private Timer mInterfaceTimer;
    private Timer mDiapoTimer;

    static final private int INTERFACE_TIMER_DELAY = Settings.getInstance().getOverlayDelay();
    private int mDiapoTimerDelay;

    private List<Photo> mPhotoList;

    final private ChangeListener<Boolean> mOnFocusedListener;

    static final private float SCALE_FACTOR = 0.02f;

    //Starts
    private int mStartIndex;
    private double mStartDropX;
    private double mStartDropY;
    private Rectangle2D mStartViewport;

    //Booleans
    private boolean mIsFullScreen;
    private boolean mIsSlideShowing;

    //UI strings
    static final private String START_SLIDE_SHOW_TXT = "Diapo";
    static final private String STOP_SLIDE_SHOW_TXT = "Stop";

    /*  UI objects  */

    //Containers
    @FXML
    private VBox mMainContainer;
    @FXML
    private HBox mToolsContainer;
    @FXML
    private HBox mWindowToolsContainer;

    //Buttons
    @FXML
    private Button mDiapoBtn;
    @FXML
    private Button mLeftArrowBtn;
    @FXML
    private Button mRightArrowBtn;

    //Others
    @FXML
    private ImageView mImageView;


    /*  Constructors    */

    //Redefinition of default constructor
    public PhotoViewerWidgetController() {
        mInterfaceTimer = new Timer();
        mDiapoTimer = new Timer();

        mDiapoTimerDelay = 2000;
        mStartIndex = -1;

        mIsSlideShowing = false;
        mIsFullScreen = false;

        mOnFocusedListener = this::closeIfNotFocused;
    }

    /*  Methods redefinition    */

    /**
     * Initializes the controller by adding eventFilters on mouse pressed and on key pressed
     * <p>Is called by the init() method</p>
     */
    final protected void initController() {
        mMainContainer.getScene().getWindow().addEventFilter(MouseEvent.MOUSE_PRESSED,
                (MouseEvent me) -> handleMouseMove());

        mMainContainer.getScene().getWindow().addEventFilter(KeyEvent.KEY_PRESSED, this::handleKeyPressed);

        getStage().focusedProperty().addListener(mOnFocusedListener);
    }

    /**
     * Closes properly the controller by stopping the timers.
     * <p>Is called by the close() method</p>
     */
    final protected void closeController() {
        stopTimer(mDiapoTimer);
        stopTimer(mInterfaceTimer);
    }

    /**
     * Returns the stage corresponding to the controller
     * @return the stage corresponding to the controller
     */
    public Stage getStage() {
        return (Stage) mMainContainer.getScene().getWindow();
    }


    /*  Setters */

    /**
     * Sets the list of photo that need to be displayed
     *
     * @param imageList the new list of photo to be displayed
     */
    void setPhotoList(List<Photo> imageList, int startIndex) {
        this.mPhotoList = imageList;

        setStartIndex(startIndex);
    }

    /**
     * Sets the index to start displaying
     *
     * @param startIndex index of the photo to display
     */
    private void setStartIndex(int startIndex) {
        if (startIndex < 0)
            mStartIndex = 0;
        else if (startIndex >= mPhotoList.size())
            mStartIndex = mPhotoList.size() - 1;
        else
            mStartIndex = startIndex;

        printImage(mStartIndex);
    }


    /*  Private methods */

    /**
     * Closes the window if not selected
     */
    @SuppressWarnings("unused")
    private void closeIfNotFocused(Observable obs, boolean wasFocused, boolean isNowFocused) {
        if (!isNowFocused) {
            close();
        }
    }

    /**
     * Stops the timer t
     * @param t Timer to stop
     */
    private void stopTimer(Timer t) {
        t.cancel();
        t.purge();
    }

    /**
     * Sets the overlay controls visible or not according to bool
     * @param bool true for setVisible(true)
     */
    private void setAllVisible(boolean bool) {
        mWindowToolsContainer.setVisible(bool);
        mLeftArrowBtn.setVisible(bool);
        mRightArrowBtn.setVisible(bool);
        mToolsContainer.setVisible(bool);
    }

    /**
     * Adapts the image size according to w and h
     *
     * @param w width to fit
     * @param h height to fit
     */
    private void adaptView(double w, double h) {
        mImageView.setFitWidth(w);
        mImageView.setFitHeight(h);

        mImageView.setViewport(new Rectangle2D(
                0,
                0,
                mImageView.getImage().getWidth(),
                mImageView.getImage().getHeight()
        ));
    }

    /**
     * Adapts the image size according to the stage size
     */
    private void adaptView() {
        adaptView(mMainContainer.getWidth(), mMainContainer.getHeight());
    }

    /**
     * Switches the screen mode (e.g. full screen to normal screen and reverse)
     */
    private void switchScreenMode() {
        Stage s = (Stage) mMainContainer.getScene().getWindow();
        mIsFullScreen = !mIsFullScreen;
        s.setFullScreen(mIsFullScreen);
        adaptView();
    }

    /**
     * Zooms in (or out) the image
     * @param inOut -1 for zoom out, 1 for zoom in
     */
    private void zoom(int inOut) {
        Rectangle2D oldViewPort = mImageView.getViewport();
        double minX, minY, width, height;

        //Set the new viewport size
        minX = oldViewPort.getMinX() + inOut * SCALE_FACTOR * mImageView.getImage().getWidth();
        minY = oldViewPort.getMinY() + inOut * SCALE_FACTOR * mImageView.getImage().getHeight();
        width = oldViewPort.getWidth() - 2 * inOut * SCALE_FACTOR * mImageView.getImage().getWidth();
        height = oldViewPort.getHeight() - 2 * inOut * SCALE_FACTOR * mImageView.getImage().getHeight();

        if (minX < 0)
            minX = 0;
        if (minY < 0)
            minY = 0;
        if (width > mImageView.getImage().getWidth())
            width = mImageView.getImage().getWidth();
        if (height > mImageView.getImage().getHeight())
            height = mImageView.getImage().getHeight();

        if(width <= 0 && height <= 0)
            return;

        //apply the new viewport
        mImageView.setViewport(new Rectangle2D(
                minX,
                minY,
                width,
                height
        ));
    }

    /**
     * Starts a new slide show, open a dialog to ask the delay between each slides
     */
    private void startNewSlideShow() {
        SlideShowSettingsDialog tid = new SlideShowSettingsDialog(
                mDiapoTimerDelay / 1000,
                getStage()
        );
        Optional<Integer> result = tid.showAndWait();

        if (result.isPresent()) {
            mDiapoTimerDelay = result.get() * 1000;
            stopTimer(mDiapoTimer);
            mDiapoTimer = new Timer();

            mDiapoTimer.schedule(new TimerTask() {
                public void run() {
                    printNext();
                }
            }, mDiapoTimerDelay, mDiapoTimerDelay);
            mIsSlideShowing = !mIsSlideShowing;
            mDiapoBtn.setText(STOP_SLIDE_SHOW_TXT);
        }
    }

    /**
     * Stops the slide show
     */
    private void stopSlideShow() {
        mIsSlideShowing = !mIsSlideShowing;
        stopTimer(mDiapoTimer);
        mDiapoBtn.setText(START_SLIDE_SHOW_TXT);
    }

    /**
     * Prints the next photo
     */
    private void printNext() {
        mStartIndex = (mStartIndex + 1) % mPhotoList.size();

        printImage(mStartIndex);
    }

    /**
     * Prints the previous photo
     */
    private void printPrev() {
        --mStartIndex;

        if (mStartIndex < 0)
            mStartIndex = mPhotoList.size() - 1;

        printImage(mStartIndex);
    }

    /**
     * Prints the image at the given index in the image list
     * @param index index of the image to print in the image list
     */
    private void printImage(int index) {
        //mImageView.setImage(mPhotoList.get(index));
        mPhotoList.get(index).getImage((image, throwable) -> {
            if (throwable != null) {
                CustomAlert al = new CustomAlert(Alert.AlertType.ERROR, getStage());
                al.setTexts(
                        "Error occurred when loading the thumbnail",
                        throwable.getMessage()
                );
                al.showAndWait();
            } else {
                mImageView.setImage(image);
                adaptView();
            }
        });
    }

    /**
     * Reacts in accordance to the given key pressed
     * @param e KeyEvent
     */
    private void handleKeyPressed(KeyEvent e) {
        switch (e.getCode()) {
            case RIGHT:
            case UP:
                printNext();
                break;
            case LEFT:
            case DOWN:
                printPrev();
                break;
            case ESCAPE:
                if (mIsFullScreen)
                    switchScreenMode();
                break;
        }
    }


    /*  FXML methods    */


    /**
     * Initializes the view
     */
    @FXML
    public void initialize() {
        setAllVisible(false);
        mImageView.setPreserveRatio(true);
        mImageView.setSmooth(true);
        mImageView.setCache(true);
    }

    /**
     * Closes the controller and its view
     */
    @FXML
    public void handleCloseBtn() {
        close();
    }

    /**
     * Switches the screen mode
     */
    @FXML
    public void handleFullScreenBtn() {
        switchScreenMode();
    }

    /**
     * Handles mouse move for the overlay functionality
     */
    @FXML
    public void handleMouseMove() {
        setAllVisible(true);
        stopTimer(mInterfaceTimer);
        mInterfaceTimer = new Timer();

        mInterfaceTimer.schedule(new TimerTask() {
            public void run() {
                setAllVisible(false);
            }
        }, INTERFACE_TIMER_DELAY);
    }

    /**
     * Prints the next photo
     */
    @FXML
    public void handleNextBtn() {
        printNext();
    }

    /**
     * Prints the previous photo
     */
    @FXML
    public void handlePrevBtn() {
        printPrev();
    }

    /**
     * Starts or stops the slide show
     */
    @FXML
    public void handleDiapoBtn() {

        getStage().focusedProperty().removeListener(mOnFocusedListener);

        if (mIsSlideShowing)
            stopSlideShow();
        else
            startNewSlideShow();

        getStage().focusedProperty().addListener(mOnFocusedListener);

    }

    /**
     * Opens the edit window with the displayed photo
     */
    @FXML
    public void handleEditBtn() {
        EditController c = (EditController)UiUtility.openDialogFromFxml(
                Settings.EDIT_WINDOW_FILE,
                getStage().getOwner(),
                Settings.EDIT_WINDOW_NAME
        );

        c.setPhoto(mPhotoList.get(mStartIndex));
    }

    /**
     * Zooms in the photo
     */
    @FXML
    public void handleZoomIn() {
        zoom(1);
    }

    /**
     * Zooms out the photo
     */
    @FXML
    public void handleZoomOut() {
        zoom(-1);
    }

    /**
     * Zooms in or out the photo according to the mouse scroll
     * @param event ScrollEvent
     */
    @FXML
    public void handleScrollZoom(ScrollEvent event) {
        if (event.getDeltaY() > 0)
            zoom(1);
        else
            zoom(-1);
    }

    /**
     * Stores the starting points of the drag
     * @param e MouseEvent
     */
    @FXML
    public void handleMousePressedImageView(MouseEvent e) {
        mStartDropX = e.getX();
        mStartDropY = e.getY();
        mStartViewport = mImageView.getViewport();
    }

    /**
     * Drags the image
     * <p>Useful on zoomed image</p>
     * @param e MouseEvent
     */
    @FXML
    public void handleMouseDraggedImageView(MouseEvent e) {
        double minX     = mStartViewport.getMinX() - 2 * (mStartDropX - e.getX());
        double minY     = mStartViewport.getMinY() - 2 * (mStartDropY - e.getY());
        double width    = mStartViewport.getWidth();
        double height   = mStartViewport.getHeight();

        if (minX < 0)
            minX = 0;
        if (minY < 0)
            minY = 0;

        if (minX + width > mImageView.getImage().getWidth())
            minX = mImageView.getImage().getWidth() - width;
        if (minY + height > mImageView.getImage().getHeight())
            minY = mImageView.getImage().getHeight() - height;

        mImageView.setViewport(new Rectangle2D(minX, minY, width, height));
    }
}