package com.arb.photomanager.controller;

import com.arb.photomanager.PhotoManagerApp;
import com.arb.photomanager.model.setting.Settings;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

import java.io.IOException;

/**
 *
 * @author Aymeric Robitaille
 */
final class UiUtility {
    private UiUtility() { /* Empty */ }

    static private AbstractController openFromFxml(String file, Window primaryWindow, String windowName, Integer v, Integer v1){
        Stage dial = new Stage();
        AbstractController c = null;
        try {
            FXMLLoader loader = new FXMLLoader(PhotoManagerApp.class.getResource(file));
            Parent root = loader.load();

            dial.initModality(Modality.APPLICATION_MODAL);
            dial.initOwner(primaryWindow);
            dial.setTitle(windowName);
            if(v == null && v1 == null)
                dial.setScene(new Scene(root));
            else
                dial.setScene(new Scene(root, v, v1));
            root.getStylesheets().addAll(primaryWindow.getScene().getRoot().getStylesheets());
            dial.getIcons().add(new Image(PhotoManagerApp.class.getResource(Settings.ICO_FILE).toString()));

            c = loader.getController();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return c;
    }

    static AbstractController openDialogFromFxml(String file, Window primaryWindow, String windowName, Integer v, Integer v1) {
        AbstractController c = openFromFxml(file, primaryWindow, windowName, v, v1);

        Stage dial = c.getStage();
        dial.show();

        c.init();

        return c;
    }

    static AbstractController openDialogFromFxml(String file, Window primaryWindow, String windowName) {
        return openDialogFromFxml(file, primaryWindow, windowName, null, null);
    }

    static AbstractController openSoftDialogFromFxml(String file, Window primaryWindow, String windowName, Integer v, Integer v1) {
        AbstractController c = openFromFxml(file, primaryWindow, windowName, v, v1);

        Stage dial = c.getStage();
        dial.initModality(Modality.NONE);
        dial.initStyle(StageStyle.UNDECORATED);
        dial.show();

        c.init();

        return c;
    }

    static AbstractController openSoftDialogFromFxml(String file, Window primaryWindow, String windowName) {
        return openSoftDialogFromFxml(file, primaryWindow, windowName, null, null);
    }

}
