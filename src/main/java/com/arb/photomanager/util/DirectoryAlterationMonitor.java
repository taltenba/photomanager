package com.arb.photomanager.util;

import java.io.IOException;
import java.nio.file.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.function.Consumer;
import java.util.function.Predicate;

import static java.nio.file.StandardWatchEventKinds.*;

/**
 * @author Thomas Altenbach
 */
public class DirectoryAlterationMonitor {
    private final WatchService mWatcher;
    private final Map<Path, WatchKey> mKeys;
    private final Map<WatchKey, AlterationListener> mListeners;
    private final Thread mThread;
    private final Executor mCallbackExecutor;
    private final Predicate<Path> mFilter;

    private boolean mStopped;

    public DirectoryAlterationMonitor(Executor callbackExecutor, Predicate<Path> filter) {
        if (callbackExecutor == null)
            throw new NullPointerException("Callback executor cannot be null.");

        WatchService watcher = null;

        try {
            watcher = FileSystems.getDefault().newWatchService();
        } catch (IOException e) {
            // Never happen: newWatchService does not thrown IOException by FileSystems created by the default provider
        }

        mWatcher = watcher;
        mKeys = new HashMap<>();
        mListeners = new HashMap<>();
        mThread = new Thread(new MonitorRunnable());
        mCallbackExecutor = Objects.requireNonNull(callbackExecutor);
        mFilter = Objects.requireNonNull(filter);
    }

    public void register(Path path, AlterationListener listener) throws IOException {
        WatchKey key = path.register(mWatcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);

        mKeys.put(path, key);
        mListeners.put(key, listener);
    }

    public void unregister(Path path) {
        WatchKey key = mKeys.remove(path);

        if (key == null)
            return;

        key.cancel();

        mListeners.remove(key);
    }

    public void startMonitoring() {
        if (mStopped)
            throw new IllegalStateException("The monitor has been stopped.");

        if (mThread.isAlive())
            return;

        mThread.start();
    }

    public void stopMonitoring() {
        if (mStopped)
            return;

        mThread.interrupt();

        try {
            mWatcher.close();
        } catch (IOException e) {
            // Empty
        }

        mStopped = true;
    }

    public boolean isRunning() {
        return !mStopped && mThread.isAlive();
    }

    private class MonitorRunnable implements Runnable {
        @Override
        public void run() {
            while (true) {
                WatchKey key;

                try {
                    key = mWatcher.take();
                } catch (InterruptedException | ClosedWatchServiceException e) {
                    break;
                }

                Path parentPath = (Path) key.watchable();

                for (WatchEvent<?> e : key.pollEvents()) {
                    WatchEvent.Kind kind = e.kind();

                    if (kind == OVERFLOW) {
                        executeCallback(key, AlterationListener::onOverflow);
                        break;
                    }

                    final Path path = parentPath.resolve((Path) e.context());

                    if (!mFilter.test(path))
                        continue;

                    if (kind == ENTRY_CREATE)
                        executeCallback(key, l -> l.onAdded(path));
                    else if (kind == ENTRY_DELETE)
                        executeCallback(key, l -> l.onDeleted(path));
                    else
                        executeCallback(key, l -> l.onModified(path));
                }

                if (!key.reset()) {
                    mCallbackExecutor.execute(() -> {
                        mKeys.remove(parentPath, key);
                        mListeners.remove(key);
                    });
                }
            }
        }
    }

    private void executeCallback(WatchKey key, Consumer<AlterationListener> callbackExecutor) {
        mCallbackExecutor.execute(() -> {
            AlterationListener l = mListeners.get(key);

            if (l != null)
                callbackExecutor.accept(l);
        });
    }

    public interface AlterationListener {
        void onAdded(Path path);
        void onDeleted(Path path);
        void onModified(Path path);
        void onOverflow();
    }
}
