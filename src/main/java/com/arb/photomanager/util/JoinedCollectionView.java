package com.arb.photomanager.util;

import com.google.common.collect.Iterables;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;

/**
 * A live unmodifiable aggregated collection view of the collections passed in.
 * @author Thomas Altenbach
 */
public final class JoinedCollectionView<E> implements Collection<E> {
    private final Collection<? extends Collection<E>> mMultiCollection;
    private int mSize;

    public JoinedCollectionView(Collection<? extends Collection<E>> multiCollection) {
        mMultiCollection = multiCollection;
        mSize = -1;
    }

    @Override
    public int size() {
        if (mSize == -1) {
            mSize = 0;

            for (Collection<E> c : mMultiCollection)
                mSize += c.size();
        }

        return mSize;
    }

    @Override
    public boolean isEmpty() {
        return !iterator().hasNext();
    }

    @Override
    public boolean contains(Object o) {
        for (Collection<E> c : mMultiCollection) {
            if (c.contains(o))
                return true;
        }

        return false;
    }

    @NotNull
    @Override
    public Iterator<E> iterator() {
        return Iterables.unmodifiableIterable(Iterables.concat(mMultiCollection))
                .iterator();
    }

    @NotNull
    @Override
    public Object[] toArray() {
        Object[] arr = new Object[size()];
        int i = 0;

        for (Collection<E> c : mMultiCollection) {
            for (E e : c) {
                arr[i] = e;
                ++i;
            }
        }

        return arr;
    }

    @SuppressWarnings("unchecked")
    @NotNull
    @Override
    public <T> T[] toArray(@NotNull T[] a) {
        T[] arr = a.length >= size() ? a :
                (T[]) Array.newInstance(a.getClass().getComponentType(), size());

        int i = 0;

        for (Collection<E> c : mMultiCollection) {
            for (E e : c) {
                arr[i] = (T) e;
                ++i;
            }
        }

        return arr;
    }

    @Override
    public boolean add(E e) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean containsAll(@NotNull Collection<?> c) {
        for (Object e : c) {
            if (!contains(e))
                return false;
        }

        return true;
    }

    @Override
    public boolean addAll(@NotNull Collection<? extends E> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(@NotNull Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(@NotNull Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }
}
