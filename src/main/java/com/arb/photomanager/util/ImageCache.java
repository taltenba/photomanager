package com.arb.photomanager.util;

import javafx.scene.image.Image;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * LRU cache of images.
 * @author Thomas Altenbach
 */
public class ImageCache {
    private final long mMaxPixelCount;
    private final Map<Long, Image> mImages;

    private long mPixelCount;
    private long mNextKey;

    public ImageCache(long maxPixelCount) {
        if (maxPixelCount <= 0)
            throw new IllegalArgumentException("Maximum pixel count must be strictly positive.");

        mMaxPixelCount = maxPixelCount;
        mImages = new LinkedHashMap<>(0, 0.75f, true); // Access-order
    }

    public long getUniqueKey() {
        return mNextKey++;
    }

    @Nullable
    public Image put(long key, Image img) {
        mPixelCount += getPixelCount(img);

        Image old = mImages.put(key, img);

        if (old != null)
            mPixelCount -= getPixelCount(img);

        while (mPixelCount > mMaxPixelCount) {
            Map.Entry<Long, Image> removed = mImages.entrySet().iterator().next();
            mImages.remove(removed.getKey());
            mPixelCount -= getPixelCount(removed.getValue());
        }

        return old;
    }

    @Nullable
    public Image get(long key) {
        return mImages.get(key);
    }

    @Nullable
    public Image remove(long key) {
        Image removed = mImages.remove(key);

        if (removed == null)
            return null;

        mPixelCount -= getPixelCount(removed);
        return removed;
    }

    private long getPixelCount(Image img) {
        return (long) img.getHeight() * (long) img.getWidth();
    }
}
