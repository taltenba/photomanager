package com.arb.photomanager.util;

import com.google.common.collect.Iterators;
import org.jetbrains.annotations.NotNull;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.lang.reflect.Array;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author Thomas Altenbach
 */
public class WeakListenerCollection<E> implements Collection<E>, RandomAccess {
    private final List<WeakReference<E>> mList;
	private final ReferenceQueue<E> mStaleQueue;

    public WeakListenerCollection() {
        mList = new CopyOnWriteArrayList<>();
        mStaleQueue = new ReferenceQueue<>();
    }

    @SuppressWarnings("unchecked")
    private void expungeStaleValues() {
        List<WeakReference<E>> staleValues = new ArrayList<>();
        WeakReference<E> staleValue;

        while ((staleValue = (WeakReference<E>) mStaleQueue.poll()) != null)
            staleValues.add(staleValue);

        mList.removeAll(staleValues);
    }

    @Override
    public int size() {
        expungeStaleValues();
        return mList.size();
    }

    @Override
    public boolean isEmpty() {
        expungeStaleValues();
        return mList.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        for (E e : this) {
            if (e.equals(o))
                return true;
        }

        return false;
    }

    @NotNull
    @Override
    public Iterator<E> iterator() {
        expungeStaleValues();
        return Iterators.filter(
                Iterators.transform(mList.iterator(), WeakReference::get),
                Objects::nonNull);
    }

    @NotNull
    @Override
    public Object[] toArray() {
        int size = size();
        Object[] arr = new Object[size];

        for (int i = 0; i < size; ++i)
            arr[i] = mList.get(i).get();

        return arr;
    }

    @SuppressWarnings("unchecked")
    @NotNull
    @Override
    public <T> T[] toArray(@NotNull T[] a) {
        int size = size();
        T[] arr = a.length >= size ? a :
                (T[]) Array.newInstance(a.getClass().getComponentType(), size);

        for (int i = 0; i < size; ++i)
            arr[i] = (T) mList.get(i).get();

        return arr;
    }

    @Override
    public boolean add(E e) {
		Objects.requireNonNull(e);
        return mList.add(new WeakReference<>(e, mStaleQueue));
    }

    @Override
    public boolean remove(Object o) {
        if (o == null)
            return false;

        expungeStaleValues();

        int i = 0;

        for (WeakReference<E> weakValue : mList) {
            if (o.equals(weakValue.get())) {
                mList.remove(i);
                return true;
            }

            ++i;
        }

        return false;
    }

    @Override
    public boolean containsAll(@NotNull Collection<?> c) {
        for (Object e : c) {
            if (!contains(e))
                return false;
        }

        return true;
    }

    private List<WeakReference<E>> toWeakValues(Collection<? extends E> c) {
        List<WeakReference<E>> weakValues = new ArrayList<>(c.size());

        for (E e : c)
            weakValues.add(new WeakReference<>(e));

        return weakValues;
    }

    @Override
    public boolean addAll(@NotNull Collection<? extends E> c) {
        return mList.addAll(toWeakValues(c));
    }

    @Override
    public boolean removeAll(@NotNull Collection<?> c) {
        expungeStaleValues();

        List<WeakReference<E>> toRemove = new ArrayList<>();

        for (WeakReference<E> weakValue : mList) {
            E value = weakValue.get();

            if (value != null && c.contains(value))
                toRemove.add(weakValue);
        }

        return mList.removeAll(toRemove);
    }

    @Override
    public boolean retainAll(@NotNull Collection<?> c) {
        expungeStaleValues();

        List<WeakReference<E>> toRetain = new ArrayList<>();

        for (WeakReference<E> weakValue : mList) {
            E value = weakValue.get();

            if (value != null && c.contains(value))
                toRetain.add(weakValue);
        }

        return mList.retainAll(toRetain);
    }

    @Override
    public void clear() {
        mList.clear();
    }
}
