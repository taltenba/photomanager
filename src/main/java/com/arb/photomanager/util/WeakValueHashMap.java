package com.arb.photomanager.util;

import org.jetbrains.annotations.NotNull;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.*;

public final class WeakValueHashMap<K, V> implements Map<K, V> {
    private final HashMap<K, WeakValue<K, V>> mMap;
    private final ReferenceQueue<Object> mStaleQueue;

    public WeakValueHashMap() {
        mMap = new HashMap<>();
        mStaleQueue = new ReferenceQueue<>();
    }

    public WeakValueHashMap(int initialCapacity) {
        mMap = new HashMap<>(initialCapacity);
        mStaleQueue = new ReferenceQueue<>();
    }

    public WeakValueHashMap(int initialCapacity, float loadFactor) {
        mMap = new HashMap<>(initialCapacity, loadFactor);
        mStaleQueue = new ReferenceQueue<>();
    }

    public WeakValueHashMap(Map<? extends K, ? extends V> map) {
        mMap = new HashMap<>(map.size());
        mStaleQueue = new ReferenceQueue<>();

        putAll(map);
    }

    @SuppressWarnings("unchecked")
    private void expungeStaleEntries() {
        WeakValue<K, V> staleValue;

        while ((staleValue = (WeakValue<K, V>) mStaleQueue.poll()) != null)
            mMap.remove(staleValue.mKey);
    }

    @Override
    public int size() {
        expungeStaleEntries();
        return mMap.size();
    }

    @Override
    public boolean isEmpty() {
        expungeStaleEntries();
        return mMap.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        expungeStaleEntries();
        return mMap.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        expungeStaleEntries();
        return mMap.containsValue(value);
    }

    @Override
    public V get(Object key) {
        /*
         * Calling expungeStaleEntries is useless here since in the case the value associated with the key
         * has been garbage collected, getReferencedValue will return null.
         * Moreover, if we would call expungeStaleEntries, we would not be able to call this method during
         * iteration since it would potentially modify the map.
         */

        return getReferencedValue(mMap.get(key));
    }

    @Override
    public V put(K key, @NotNull V value) {
        /*
         * Calling expungeStaleEntries is not required here but enable a better distribution
         * of expungeStaleEntries calls on the map operations.
         */
        expungeStaleEntries();

        WeakValue<K, V> oldValue = mMap.put(key, new WeakValue<>(key, value, mStaleQueue));
        return getReferencedValue(oldValue);
    }

    @Override
    public V remove(Object key) {
        /*
         * Calling expungeStaleEntries is not required here but enable a better distribution
         * of expungeStaleEntries calls on the map operations.
         */
        expungeStaleEntries();

        WeakValue<K, V> removed = mMap.remove(key);
        return getReferencedValue(removed);
    }

    @Override
    public void putAll(@NotNull Map<? extends K, ? extends V> m) {
        expungeStaleEntries();

        for (Entry<? extends K, ? extends V> entry : m.entrySet()) {
            K key = entry.getKey();
            mMap.put(key, new WeakValue<>(key, entry.getValue(), mStaleQueue));
        }
    }

    @Override
    public void clear() {
        mMap.clear();

        //noinspection StatementWithEmptyBody
        while (mStaleQueue.poll() != null) {
            // Empty
        }
    }

    @NotNull
    @Override
    public Set<K> keySet() {
        expungeStaleEntries();
        return mMap.keySet();
    }

    /*private class KeySet extends AbstractSet<K> {
        @NotNull
        @Override
        public Iterator<K> iterator() {
            return new KeyIterator();
        }

        @Override
        public int size() {
            return WeakValueHashMap.this.size();
        }

        @Override
        public boolean contains(Object o) {
            return containsKey(o);
        }

        @Override
        public boolean remove(Object o) {
            return WeakValueHashMap.this.remove(o) != null;
        }

        @Override
        public void clear() {
            WeakValueHashMap.this.clear();
        }
    }*/

    @NotNull
    @Override
    public Collection<V> values() {
        expungeStaleEntries();
        return new Values();
    }

    private class Values extends AbstractCollection<V> {
        @NotNull
        @Override
        public Iterator<V> iterator() {
            return new ValueIterator();
        }

        @Override
        public int size() {
            return WeakValueHashMap.this.size();
        }

        @Override
        public boolean contains(Object o) {
            return containsValue(o);
        }

        @Override
        public void clear() {
            WeakValueHashMap.this.clear();
        }
    }

    @NotNull
    @Override
    public Set<Entry<K, V>> entrySet() {
        expungeStaleEntries();
        return new EntrySet();
    }

    private class EntrySet extends AbstractSet<Entry<K, V>> {
        @NotNull
        @Override
        public Iterator<Entry<K, V>> iterator() {
            return new EntryIterator();
        }

        @Override
        public int size() {
            return WeakValueHashMap.this.size();
        }

        @Override
        public boolean contains(Object o) {
            if (!(o instanceof Entry))
                return false;

            Entry<?, ?> entry = (Entry<?, ?>) o;
            V value = get(entry.getKey());

            return value != null && value.equals(entry.getValue());
        }

        @Override
        public boolean remove(Object o) {
            if (!contains(o))
                return false;

            Entry<?, ?> entry = (Entry<?, ?>) o;
            WeakValueHashMap.this.remove(entry.getKey());

            return true;
        }

        @Override
        public void clear() {
            WeakValueHashMap.this.clear();
        }

        private List<Entry<K, V>> deepCopy() {
            List<Entry<K, V>> list = new ArrayList<>(size());

            for (Entry<K, V> entry : this)
                list.add(new AbstractMap.SimpleEntry<>(entry));

            return list;
        }

        @NotNull
        @Override
        public Object[] toArray() {
            return deepCopy().toArray();
        }

        @SuppressWarnings("SuspiciousToArrayCall")
        @NotNull
        @Override
        public <T> T[] toArray(@NotNull T[] a) {
            return deepCopy().toArray(a);
        }
    }

    private V getReferencedValue(WeakValue<K, V> value) {
        return value == null ? null : value.get();
    }

    private class ValueIterator extends HashIterator<V> {
        @Override
        public V next() {
            return nextEntry().getValue();
        }
    }

    /*private class KeyIterator extends HashIterator<K> {
        @Override
        public K next() {
            return nextEntry().getKey();
        }
    }*/

    private class EntryIterator extends HashIterator<Entry<K, V>> {
        @Override
        public Entry<K, V> next() {
            return nextEntry();
        }
    }

    private abstract class HashIterator<T> implements Iterator<T> {
        private final Iterator<Entry<K, WeakValue<K, V>>> mIterator;

        /**
         * Hold a strong reference of the next value, avoiding the
         * disappearance of the value between hasNext and next.
         */
        private StrongEntry mNextEntry;

        HashIterator() {
            mIterator = mMap.entrySet().iterator();
        }

        @Override
        public boolean hasNext() {
            return prepareNextEntry() != null;
        }

        @Override
        public void remove() {
            mIterator.remove();
        }

        Entry<K, V> nextEntry() {
            StrongEntry entry = mNextEntry;
            mNextEntry = null;

            return entry;
        }

        private Entry<K, V> prepareNextEntry() {
            if (mNextEntry != null)
                return mNextEntry;

            Entry<K, WeakValue<K, V>> nextWeakEntry = null;
            V nextValue = null;

            while (mIterator.hasNext() && nextValue == null) {
                nextWeakEntry = mIterator.next();
                nextValue = nextWeakEntry.getValue().get();
            }

            if (nextValue == null)
                mNextEntry = null;
            else
                mNextEntry = new StrongEntry(nextWeakEntry);

            return mNextEntry;
        }
    }

    private final class StrongEntry implements Map.Entry<K, V> {
        private final Entry<K, WeakValue<K, V>> mEntry;
        private V mValue;

        StrongEntry(@NotNull Map.Entry<K, WeakValue<K, V>> entry) {
            mEntry = entry;
            mValue = entry.getValue().get();
        }

        @Override
        public K getKey() {
            return mEntry.getKey();
        }

        @Override
        public V getValue() {
            return mValue;
        }

        @Override
        public V setValue(V value) {
            V oldValue = mValue;
            mValue = value;

            mEntry.setValue(new WeakValue<>(mEntry.getKey(), value, mStaleQueue));

            return oldValue;
        }
    }

    private static final class WeakValue<K, V> extends WeakReference<V> {
        private final K mKey;

        WeakValue(K key, V value, ReferenceQueue<? super V> staleQueue) {
            super(value, staleQueue);

            mKey = key;
        }
    }
}
