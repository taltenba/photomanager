package com.arb.photomanager.util;

import com.arb.photomanager.model.setting.Settings;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashSet;

/**
 * @author Aymeric Robitaille
 */
final public class AlbumPathsParser {

    private AlbumPathsParser() { /* EMPTY */}

    static public String toString(Collection<Path> paths) {
        StringBuffer stringBuff = new StringBuffer(Settings.MAX_STRING_LENGTH);

        for (Path path : paths) {
            String pathStr = path.toString();
            if (stringBuff.length() + pathStr.length() + 2 > stringBuff.capacity())
                break;

            stringBuff
                    .append("\"")
                    .append(pathStr)
                    .append("\"")
                    .append(",");
        }
        stringBuff.deleteCharAt(stringBuff.length() - 1);

        return stringBuff.toString();
    }

    static public Collection<Path> fromString(String parsedString) {
        Collection<Path> paths = new HashSet<>();

        String[] parsedSubStrings = parsedString.split(",");
        for (String str : parsedSubStrings) {
            if (str.length() < 2)
                continue;

            paths.add(
                    Paths.get(
                            str.substring(1, str.length() - 1)
                    )
            );
        }

        return paths;
    }
}
