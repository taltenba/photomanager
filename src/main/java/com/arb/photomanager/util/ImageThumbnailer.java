package com.arb.photomanager.util;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.nio.file.Path;
import java.util.concurrent.ExecutionException;

/**
 * @author Thomas Altenbach
 */
public class ImageThumbnailer {
    private final ImageCache mImageCache;
    private final int mThumbnailHeight;
    private final int mThumbnailWidth;

    public ImageThumbnailer(int thumbnailWidth, int thumbnailHeight, long maxCachedPixelCount) {
        if (thumbnailHeight <= 0 || thumbnailWidth <= 0)
            throw new IllegalArgumentException("Thumbnail width and height must be strictly positive");

        mImageCache = new ImageCache(maxCachedPixelCount);
        mThumbnailHeight = thumbnailHeight;
        mThumbnailWidth = thumbnailWidth;
    }

    public long getUniqueKey() {
        return mImageCache.getUniqueKey();
    }

    private Image createThumbnail(Image fullResImage) throws ExecutionException, InterruptedException {
        ImageView imageView = new ImageView();
        imageView.setSmooth(true);
        imageView.setPreserveRatio(true);
        imageView.setFitWidth(mThumbnailWidth);
        imageView.setFitHeight(mThumbnailHeight);
        imageView.setImage(fullResImage);

        return BackgroundNodeSnapshooter.snapshot(imageView, null, null);
    }

    public Image getThumbnail(long key, ImageLoader loader) throws Exception {
        Image thumbnail = mImageCache.get(key);

        if (thumbnail != null)
            return thumbnail;

        Image fullResImage = loader.loadImage();

        if (fullResImage.getWidth() <= mThumbnailWidth && fullResImage.getHeight() <= mThumbnailHeight)
            thumbnail = fullResImage;
        else
            thumbnail = createThumbnail(fullResImage);

        mImageCache.put(key, thumbnail);

        return thumbnail;
    }

    public Image getThumbnail(long key, Path imagePath) throws Exception {
        Image thumbnail = mImageCache.get(key);

        if (thumbnail != null)
            return thumbnail;

        thumbnail = new Image(imagePath.toUri().toString(), mThumbnailWidth, mThumbnailHeight,
                                true, true);

        if (thumbnail.getException() != null)
            throw thumbnail.getException();

        mImageCache.put(key, thumbnail);

        return thumbnail;
    }

    public void invalidateThumbnail(long key) {
        mImageCache.remove(key);
    }

    @FunctionalInterface
    public interface ImageLoader {
        Image loadImage() throws Exception;
    }
}
