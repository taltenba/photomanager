package com.arb.photomanager.util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

/**
 * Utility class for files unity conversion
 * @author Aymeric Robitaille
 */
final public class FileUtil {

    private FileUtil() { /* Empty */ }

    /**
     * Converts size in bites to a human readable string
     * @param size size in bites
     * @return human readable result
     */
    public static String readableFileSize(long size) {
        if(size <= 0) return "0";
        final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    /**
     * Converts millis to human readable date in format 'MM/dd/yyyy h:m:s'
     * @param d millis from epoch
     * @return human readable date
     */
    public static  String readableFileDate(long d) {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy kk:mm:ss");
        return df.format(d);
    }
}
