package com.arb.photomanager.util;

import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Aymeric Robitaille
 */
final public class SortingParser {
    private SortingParser() { /* Empty */}

    static public String tagsToString(Set<String> tags) {
        StringBuilder stringBuilder = new StringBuilder();

        for (String s : tags) {
            stringBuilder
                    .append(s)
                    .append(" ");
        }

        if (stringBuilder.toString().length() == 0)
            return "";

        return stringBuilder.substring(0, stringBuilder.lastIndexOf(" ")).toUpperCase();
    }

    @Nullable
    static public Set<String> stringToTags(String str) {
        String[] strTags = str.toUpperCase().split(" ");

        return new HashSet<>(Arrays.asList(strTags));
    }

    static public String datesToString(long startDate, long endDate) {
        return "\""
                + FileUtil.readableFileDate(startDate)
                + "\",\""
                + FileUtil.readableFileDate(endDate)
                + "\""
                ;
    }

    @Nullable
    static public Date stringToStartDate(String str) {

        if (str.isBlank())
            return null;

        String first = str.split(",")[0];
        first = first.substring(1, first.length() - 1);

        return stringToDate(first);
    }

    @Nullable
    static public Date stringToEndDate(String str) {

        String[] datesStr = str.split(",");

        if (datesStr.length != 2)
            return null;

        String end = str.split(",")[1];
        end = end.substring(1, end.length() - 1);

        return stringToDate(end);
    }

    @Nullable
    static private Date stringToDate(String str) {
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy kk:mm:ss");

        try {
            return formatter.parse(str);
        } catch (ParseException e) {
            return null;
        }
    }
}
