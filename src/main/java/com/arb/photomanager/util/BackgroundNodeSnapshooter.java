package com.arb.photomanager.util;

import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @author Thomas Altenbach
 */
public final class BackgroundNodeSnapshooter {
    public static WritableImage snapshot(Node node, @Nullable SnapshotParameters params, @Nullable WritableImage image) throws ExecutionException, InterruptedException {
        Objects.requireNonNull(node);

        if (Platform.isFxApplicationThread())
            return node.snapshot(params, image);

        /*CompletableFuture<WritableImage> future = new CompletableFuture<>();

        Platform.runLater(() -> node.snapshot(
                param -> {
                    future.complete(param.getImage());
                    return null;
                }, params, image));*/

        FutureTask<WritableImage> future = new FutureTask<>(() -> node.snapshot(params, image));
        Platform.runLater(future);

        return future.get();
    }
}
