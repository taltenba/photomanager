# PhotoManager
PhotoManager is a simple JavaFX-based application aiming at managing a collection of photos.

This project has been carried out as part of our studies. The goal was to develop a Java application making in particular use of the object-oriented programming concepts learned during the semester.

Photos can be gathered in albums and sub-albums, corresponding to filesystem directories, and tags can be added to the pictures enabling to easily find related photos through the application search engine. Moreover, picture metadata can be viewed and some basic image transformations such as resizing or filtering can be applied.

![Imgur Image](https://i.imgur.com/4A2Z9my.png)

## Technologies
The main technologies used in this project are:
* Java 11
* JavaFX
* ControlsFX
* Guava
* Gson
* SQLite
* Apache Commons Imaging, Math and IO

## Getting started
### Prerequisites
* Windows is recommended. The application should also run on Linux and macOS but has not been tested on those platforms.
* Java 11 or above.

### Running
The application can be directly launched using the following commands from the project directory.

Windows:
```cmd
gradlew.bat run
```

Linux & macOS:
```sh
chmod +x gradlew
./gradlew run
```

### Creating a runtime image
A runtime image can also be created using `jlink`, containing only the necessary dependencies.

Windows:
```cmd
gradlew.bat jlink
```

Linux & macOS:
```sh
chmod +x gradlew
./gradlew jlink
```

The generated image can then be found in `build/image` and can be run using the executable in `build/image/bin`.

Windows:
```cmd
cd /build/image/bin
photomanager.bat
```

Linux & macOS:
```sh
cd /build/image/bin
./photomanager
```

## Gallery
<img src="https://i.imgur.com/4A2Z9my.png" width="33%"/>
<img src="https://i.imgur.com/XbTVmrh.png" width="33%"/>
<img src="https://i.imgur.com/DOP8Sif.png" width="33%"/>

<img src="https://i.imgur.com/CRvoDpE.png" width="33%"/>
<img src="https://i.imgur.com/wfLDUKW.png" width="33%"/>
<img src="https://i.imgur.com/JFxxr4g.png" width="33%"/> 

## Model architecture
The UML diagram below describes the architecture of the application data model.

![Imgur Image](https://i.imgur.com/KV3aGzw.png)

## Authors
* ALTENBACH Thomas - @taltenba
* ROBITAILLE Aymeric - @AyRobi
* BIRLING Lucas - @LucasBirling
